package fr.xdata.ga.core.exceptions;

/**
 *
 */
public class IllegalGranularityException extends Throwable {
    public static class Message {
        private String type = IllegalGranularityException.class.getSimpleName();
        private String message;

        public String getType() {
            return type;
        }

        public String getMessage() {
            return message;
        }
    }

    public IllegalGranularityException() {
    }

    public IllegalGranularityException(String message) {
        super(message);
    }

    public IllegalGranularityException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalGranularityException(Throwable cause) {
        super(cause);
    }

    public IllegalGranularityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public Message getStructuredMessage() {
        Message m = new Message();
        m.message = this.getMessage();
        return m;
    }
}
