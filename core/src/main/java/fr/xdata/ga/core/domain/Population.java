package fr.xdata.ga.core.domain;

import javax.persistence.Embeddable;

/**
 *
 */
@Embeddable
public class Population {
    public PopulationCategory category;

    public Population() {
    }

    public Population(PopulationCategory populationCategory) {
        category = populationCategory;
    }

    public PopulationCategory getCategory() {
        return category;
    }

    public void setCategory(PopulationCategory category){
        this.category = category;
    }

    @Override
    public String toString() {
        return category == null ? "null" : category.name();
    }
}