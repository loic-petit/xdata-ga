/*
 * Copyright (C) by Data Publica, All Rights Reserved.
 */
package fr.xdata.ga.core.security;

import fr.xdata.ga.core.domain.User;
import fr.xdata.ga.core.domain.UserRole;
import fr.xdata.ga.core.repositories.UserRepository;
import fr.xdata.ga.core.repositories.ZoneRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class UserDetailsServiceImpl implements CustomUserDetailsService {
    protected final Log log = LogFactory.getLog(getClass());

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ZoneRepository zoneRepository;

    @PostConstruct
    private void init() {
        // create an admin user if necessary
        User adminUser = userRepository.findByEmail("admin@data-publica.com");
        if (adminUser == null) {
            adminUser = new User("admin@data-publica.com", "SimilarPoints", "Publica", UserRole.Admin);
            adminUser.encryptAndSetPassword("kitesurf");
            userRepository.save(adminUser);
        }
        /*
        User userRegionRhoneAlpes = userRepository.findByEmail("cr@rhonealpes.fr");
        if (userRegionRhoneAlpes == null) {
            userRegionRhoneAlpes = new User("cr@rhonealpes.fr", "Région", "Rhône-Alpes", UserRole.Customer);
            userRegionRhoneAlpes.encryptAndSetPassword("rhonealpes");
            HashSet<Zone> zones = Sets.newHashSet(zoneRepository.findAllDepByReg(82));
            zones.add(zoneRepository.findByCog(82, null, null, null));
            userRegionRhoneAlpes.setRestrictedZones(zones);
            userRepository.save(userRegionRhoneAlpes);
        } else {
            userRegionRhoneAlpes.getRestrictedZones().add(zoneRepository.findByCog(82, null, null, null));
            userRepository.save(userRegionRhoneAlpes);
        }

        User userAquitaine = userRepository.findByEmail("cr@aquitaine.fr");
        if (userAquitaine == null) {
            userAquitaine = new User("cr@aquitaine.fr", "Région", "Aquitaine", UserRole.Customer);
            userAquitaine.encryptAndSetPassword("aquitaine");
            HashSet<Zone> zones = Sets.newHashSet(zoneRepository.findAllDepByReg(72));
            zones.add(zoneRepository.findByCog(72, null, null, null));
            userAquitaine.setRestrictedZones(zones);
            userRepository.save(userAquitaine);
        } else {
            userAquitaine.getRestrictedZones().add(zoneRepository.findByCog(72, null, null, null));
            userRepository.save(userAquitaine);
        }*/
    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        User user = userRepository.findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("user not found: " + username);
        }
        return new SecureUser(user);
    }

    public SecureUser getAuthenticatedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && !(auth instanceof AnonymousAuthenticationToken)) {
            return ((SecureUser) auth.getPrincipal());
        } else {
            return null;

        }
    }

}
