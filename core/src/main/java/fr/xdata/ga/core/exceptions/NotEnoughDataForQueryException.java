package fr.xdata.ga.core.exceptions;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.xdata.ga.core.selection.dimensions.DimensionType;
import fr.xdata.ga.core.selection.internal.ComplexSelection;
import fr.xdata.ga.core.selection.internal.GlobalSelection;

import java.util.EnumMap;
import java.util.Map;
import java.util.Set;

/**
 *
 */
public class NotEnoughDataForQueryException extends Exception {
    private ComplexSelection missing;

    public static class Message {
        private String type = NotEnoughDataForQueryException.class.getSimpleName();
        private Set<Map<DimensionType, String>> detail;

        public String getType() {
            return type;
        }

        public Set<Map<DimensionType, String>> getDetail() {
            return detail;
        }
    }

    public NotEnoughDataForQueryException(ComplexSelection missing) {
        this.missing = missing;
    }

    public Message getStructuredMessage() {
        Message message = new Message();
        message.detail = Sets.newHashSet();
        for (GlobalSelection dsms : missing) {
            EnumMap<DimensionType,String> dim = Maps.newEnumMap(DimensionType.class);
            for (DimensionType type : DimensionType.values()) {
                dim.put(type, dsms.S(type).toString());
            }
            message.detail.add(dim);
        }

        return message;
    }
}
