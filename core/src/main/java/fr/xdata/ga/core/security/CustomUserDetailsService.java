/*
 * Copyright (C) by Data Publica, All Rights Reserved.
 */
package fr.xdata.ga.core.security;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface CustomUserDetailsService extends UserDetailsService {
    public SecureUser getAuthenticatedUser();
}
