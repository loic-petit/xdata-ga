package fr.xdata.ga.core.selection.internal;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.xdata.ga.core.domain.DataSet;
import fr.xdata.ga.core.repositories.DataSetRepository;
import fr.xdata.ga.core.selection.Select;
import fr.xdata.ga.core.selection.dimensions.DimensionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * DataSelection module! It's a bit tricky and needs some algebraic details before reading it. See my PDF for further
 * explanations
 */
@Service
public class DataSetSelection {
    private static final Logger log = LoggerFactory.getLogger(DataSetSelection.class);

    private Map<GlobalSelection, DataSet> dataSets;
    private ComplexSelection missing;

    // protected DataSetSelection(List<DataSet> ds) {
    // }

    @Autowired
    private DataSetRepository dsr;

    public DataSetSelection() {
    }

    @PostConstruct
    public void ensureCachedDataSets() {
        if(dsr != null)
            fill(dsr.findAll());
    }

    protected void fill(List<DataSet> ds) {
        dataSets = Maps.newHashMap();
        for (DataSet d : ds) {
            dataSets.put(new GlobalSelection(d), d);
        }
    }

    public Map<GlobalSelection, DataSet> getDataSets() {
        return dataSets;
    }

    /**
     * Select the best strategy to answer the given query
     * 
     * @param query The query
     * @return The best strategy, null if no possible strategy can be found
     */
    public Strategy selectSource(Select query) {
        ensureCachedDataSets();
        GlobalSelection queryMetaData = new GlobalSelection(query);
        Strategy primary = primaryStrategy(queryMetaData);
        // this could actually work without this special case but it's cleaner
        if(primary.isEmpty())
            return primary;

        List<Strategy> strategies = Lists.newArrayList();
        buildSelections(primary, primary.keySet(), strategies);

        Strategy max = Collections.max(strategies);
        max.setMissing(primary.getMissing());
        return max;
    }

    public Strategy primaryStrategy(GlobalSelection queryMetaData) {
        Set<GlobalSelection> primary = primarySelection(queryMetaData);
        if(primary.isEmpty()) {
            Strategy s = new Strategy();
            s.setMissing(new ComplexSelection(queryMetaData));
            return s;
        }

        ComplexSelection missing = queryMetaData.coverageIsContainedIn(primary);
        if (!missing.isEmpty()) {
            if (log.isTraceEnabled()) {
                log.trace("[Select][R] The set of dataset\n\n{}\ndoes not cover the query\n{}\nmissing: {}\n", primary,
                        queryMetaData, missing);
            }
        }
        // ok we're good we can select strategies
        Strategy strat = new Strategy();
        for (GlobalSelection ds : primary) {
            ComplexSelection queryDS = new ComplexSelection(new GlobalSelection(ds, queryMetaData));
            strat.put(ds, new ComplexSelection(queryDS, missing));
        }
        List<Strategy> strategies = Lists.newArrayList();
        buildSelections(strat, primary, strategies);

        Strategy max = Collections.max(strategies);
        max.setMissing(missing);
        return max;
    }

    /**
     * In case of an error in selectSource, this method gives the missing parts to recover from
     * 
     * @return a disjunctive list of elements
     */
    public ComplexSelection getMissing() {
        return missing;
    }

    public Set<GlobalSelection> primarySelectionByDimension(DimensionType type, Integer granularity,
                    DimensionRange range) {
        Set<GlobalSelection> res = Sets.newHashSet();
        for (GlobalSelection ds : dataSets.keySet()) {
            if (!(ds.G(type) > granularity || ds.S(type).duplicate().restrict(range).isEmpty()))
                res.add(ds);
        }
        return res;
    }

    /**
     * Compute the primary selection, i.e. select all the datasets that can contribute at least once in the query
     * 
     * @param query the query meta data
     * @return the set
     */
    public Set<GlobalSelection> primarySelection(GlobalSelection query) {
        Set<GlobalSelection> res = Sets.newHashSet();
        for (GlobalSelection ds : dataSets.keySet()) {
            boolean valid = true;
            for (DimensionType type : DimensionType.values()) {
                if (ds.G(type) > query.G(type) || ds.S(type).duplicate().restrict(query.S(type)).isEmpty()) {
                    // if (log.isTraceEnabled()) {
                    // log.trace("\n[Primary] REJECTED\n\n{}\nhas been rejected for the query\n{}\nC: {}, G: {}, S: {}\n\n",
                    // ds, query, type, ds.G(type) <= query.G(type),
                    // !ds.S(type).duplicate().restrict(query.S(type)).isEmpty());
                    // }
                    valid = false;
                    break;
                }
            }
            if (valid) {
                // if (log.isTraceEnabled()) {
                // log.trace("\n[Primary] ACCEPTED\n\n{}\nhas been accepted for the query\n{}\n\n", ds,
                // query);
                // }
                res.add(ds);
            }
        }
        return res;
    }

    /**
     * Compute the collection of non-overlapping strategies
     * 
     * @param current Current strategy
     * @param dataSets Set of sources that needs to be cleaned to not overlap
     * @param strategy The list to fill
     */
    private void buildSelections(Strategy current, Set<GlobalSelection> dataSets, List<Strategy> strategy) {
        // if no dataSets needs to be cleaned add the current strategy and finish
        if (dataSets.isEmpty()) {
            strategy.add(current);
            return;
        }

        Set<GlobalSelection> notUsedDataSets = Sets.newHashSet(dataSets);
        // for each DSMD
        for (GlobalSelection selectedDS : dataSets) {
            notUsedDataSets.remove(selectedDS);
            HashSet<GlobalSelection> otherSources = Sets.newHashSet(notUsedDataSets);

            // compute the new restriction
            // otherSources could be modified to remove DSMD that are not relevant
            Strategy r = computeRestrictedResult(current, selectedDS, otherSources);
            // throw the recursion!
            if(!otherSources.isEmpty()) {
                buildSelections(r, otherSources, strategy);
            } else {
                strategy.add(r);
            }

            current = new Strategy(current);
            current.remove(selectedDS);
        }
    }

    /**
     * Let current be a result, restrict all the datasets to all that selectedDS does not handle on otherSources.
     * 
     * @param current the current strategy
     * @param selectedDS the dataset to remove
     * @param otherSources list of datasets to be restricted. this set can be modified if a dataset is not necessary
     *            anymore
     * @return The new strategy
     */
    private Strategy computeRestrictedResult(Strategy current, GlobalSelection selectedDS,
                    Set<GlobalSelection> otherSources) {
        Strategy r = new Strategy(current);
        for (GlobalSelection ds : Sets.newHashSet(otherSources)) {
            // iterate on duplicate to be able to remove
            ComplexSelection restricted = new ComplexSelection(current.get(ds), selectedDS);
            if (restricted.isEmpty()) {
                r.remove(ds);
                otherSources.remove(ds);
                // ensure r(d \in otherSources)
                continue;
            }
            r.put(ds, restricted);
        }
        return r;
    }

    /**
     * If possible, recover a DataSet object from a DSMD object
     * 
     * @param meta the DSMD object
     * @return The DataSet object if found, null else
     */
    public DataSet asDataSet(GlobalSelection meta) {
        return dataSets.get(meta);
    }
}
