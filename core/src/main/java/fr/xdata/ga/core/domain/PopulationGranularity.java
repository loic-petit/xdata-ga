package fr.xdata.ga.core.domain;

/**
 *
 */
public enum PopulationGranularity {
    SELECTED, ALL
}
