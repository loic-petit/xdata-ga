package fr.xdata.ga.core.importers;

import fr.xdata.ga.core.domain.*;
import fr.xdata.ga.core.repositories.ZoneRepository;
import fr.xdata.ga.core.util.SecurityUtils;
import org.slf4j.Logger;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Describe the dataset and how to read the data file.
 *
 * <p>
 * DataSetDescriptor are created from a dataset descriptor file (*.dataset) using the class constructor, and then
 * the 'parseProperty' method. After call to each of those, the method 'isValid' tell if error were found in the
 * description files. These can be retrieve using 'getErrors'.
 *
 * <p>
 * The property files can contain the properties (* are required):
 * <ul>
 *     <li> <b>name*</b>:     the name of the dataset
 *     <li> <b>content*</b>:  the file containing the dataset data, as a relative path
 *     <li> <b>provider*</b>: the name of this dataset provider
 *     <li> <b>label*</b>:    a label attached to the dataset
 *     <li> <b>url</b>:        an url where the dataset can be obtained
 *
 *     <li> <b>origin</b>: the name of the zone the dataset movements are from     (1)
 *     <li> <b>target</b>: the name of the zone the dataset movements are going to (1)
 *     <li> <b>zoneGranularity*</b>: the granularity of movement zone (See {@link ZoneGranularity})
 *
 *     <li> <b>from*</b>: The first date (in yyyy-MM-dd format) covered by the dataset   (1)
 *     <li> <b>to*</b>: The first date (in yyyy-MM-dd format) not covered by the dataset (1)
 *     <li> <b>periodGranularity*</b>: the granularity of movement period (See {@link PeriodGranularity})
 *
 *     <li> <b>PopulationGranularity*</b>: the granularity of movement population (See {@link PopulationGranularity})
 *
 *     <li> <b>moveType*</b>: the type of movement records given by this dataset (See {@link MoveType})
 *     <li> <b>dataType</b>:  Type of movement data. Can be "default" or "immigration" (2)
 * </ul>
 * <br>
 * Then, for standard data type, it should give a description of the dataset column values for parsing of individual
 * movement.
 * <ul>
 *     <li> <b>countColumn*</b>: the index of the dataset column containing the movement count (3)
 *
 *     <li> <b>originColumn</b>:     the index of the dataset column containing the movement origin code (A,3)
 *     <li> <b>originNameColumn</b>: the index of the dataset column containing the movement origin name (B,3,4)
 *     <li> <b>originFormat</b>:     the format of origin code: DDCCC (default) or POSTAL                (4)
 *
 *     <li> <b>targetColumn</b>:     the index of the dataset column containing the movement target code (A,3)
 *     <li> <b>targetNameColumn</b>: the index of the dataset column containing the movement target name (B,3,4)
 *     <li> <b>targetFormat</b>:     the format of target code: DDCCC (default) or POSTAL                (4)
 *
 *     <li> <b>populationColumn</b> (required if populationGranularity is SELECTED) See {@link PopulationGranularity}
 *
 *     <li> <b>fromColumn</b>:  the index of the dataset column containing the movement 'from' date (A,3)
 *     <li> <b>fromPattern</b>: pattern of the data in the 'from' column.                          (C)
 *     <li> <b>toColumn</b>:    the index of the dataset column containing the movement 'to' date   (A,3)
 *     <li> <b>toPattern</b>:   pattern of the data in the 'to' column.                              (C)
 * </ul>
 * <p>
 * (1) It is used as default value of individual movement if none is provided (i.e. no dedicated column). <br>
 * (2) Immigration data type has a specific format.<br>
 * (3) column indices start at 0 <br>
 * (4) if zone are postal code (type is POSTAL) then a lookup in data-publica DB is done to match it on an standard
 *     INSEE code (DDCCC: Departement in 2 digits, then Commune in 3 digits)
 *     In this case, the zone names are also used in order to have a more precise matching.
 * <p>
 * (A) if not provided, use the respective dataset attribute <br>
 * (B) if not provided, it is not used <br>
 * (C) default to "yyyy-MM-dd". See {@link SimpleDateFormat} for possible values <br>
 */                                         // TODO finish doc on immigration file format
class DataSetDescriptor{
    // use for descriptor file parsing
    private Properties descriptor;
    private List<String> errors =  new LinkedList<>();
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");


    // file stuff
    private File descriptorFile;
    private File datasetFile;
    private String md5;

    // general properties
    private String provider;
    private String label;
    private String name;
    private String url;

    // zone properties
    private Zone origin;
    private Zone target;
    private ZoneGranularity zoneGranularity;

    // time properties
    private Date from, to;
    private PeriodGranularity periodGranularity;

    // population properties
    private PopulationGranularity populationGranularity;

    // movements properties
    private MoveType moveType;
    private String dataType;
    private String originFormat;
    private String targetFormat;
    private int originColumn;
    private int targetColumn;
    private int countColumn;
    private int populationColumn;
    private int originNameColumn, targetNameColumn;
    private int fromColumn, toColumn;
    private SimpleDateFormat fromPattern, toPattern;

    public DataSetDescriptor(File file){
        readDescriptorFile(file);
    }

    private void readDescriptorFile(File file){
        descriptorFile = file;
        descriptor = new Properties();
        errors.clear();

        // get file input stream
        InputStreamReader stream;
        try{
            stream = new InputStreamReader(new FileInputStream(descriptorFile),"UTF8");
        }catch (FileNotFoundException e){
            errors.add("File not found");
            return;
        }catch (UnsupportedEncodingException e){
            errors.add("Unsupported encoding: .dataset file should be UTF-8");
            return;
        }

        // read properties
        try{
            descriptor.load(stream);
        }catch(IOException e){
            errors.add("Invalid file structure: "+e.getMessage());
            return;
        }

        // get content file
        String content  = descriptor.getProperty("content", null);
        if(content==null){
            errors.add("missing content: the dataset content file name");
        }else{
            datasetFile = new File(descriptorFile.getParent(), content);
            if(!datasetFile.exists()){
                errors.add("dataset content file not found: "+datasetFile.getAbsolutePath());
            }else
                md5 = SecurityUtils.getMD5(datasetFile.getAbsolutePath());
        }
    }

    // properties
    // ----------
    public void parseProperties(ZoneRepository zoneRepository, final Logger log){
        // general properties
        name     = getStringProperty("name",    null);   //log.debug("  name="+name+";");
        provider = getStringProperty("provider",null);   //log.debug("  provider="+provider+";");
        label    = getStringProperty("label",   null);   //log.debug("  label="+name+";");
        url      = getStringProperty("url",     "");

        // zone properties
        origin = getZoneProperty("origin", zoneRepository);   //log.debug("  origin="+origin.getLib()+";");
        target = getZoneProperty("target", zoneRepository);   //log.debug("  target="+target.getLib()+";");
        zoneGranularity = getGranularityProperty("zoneGranularity",ZoneGranularity.class);

        // time properties
        from = getDateProperty("from");
        to   = getDateProperty("to");
        periodGranularity = getGranularityProperty("periodGranularity",PeriodGranularity.class);

        // population properties
        populationGranularity = getGranularityProperty("populationGranularity",PopulationGranularity.class,"ALL");

        // movements properties
        dataType = getStringProperty("dataType", "default").toLowerCase();   //log.debug("  dataType="+dataType+";");
        moveType = getGranularityProperty("moveType",MoveType.class);

        if(!isImmigrationData()){
            countColumn  = getColumnProperty("countColumn", true);

            originColumn = getColumnProperty("originColumn", true);
            originFormat = getStringProperty("originFormat", "DDCCC").toUpperCase();
            originNameColumn = getColumnProperty("originNameColumn", false); // required if originFormat==POSTAL ?

            targetColumn = getColumnProperty("targetColumn", true);
            targetFormat = getStringProperty("targetFormat", "DDCCC").toUpperCase();
            targetNameColumn = getColumnProperty("targetNameColumn", false); // required if targetFormat==POSTAL ?

            populationColumn = getColumnProperty("populationColumn",populationGranularity==PopulationGranularity.SELECTED);

            fromColumn  = getColumnProperty("fromColumn",false);
            toColumn    = getColumnProperty("toColumn", false);
            fromPattern = getDatePatternProperty("fromPattern");
            toPattern   = getDatePatternProperty("toPattern");
        }
    }

    private String getStringProperty(String name, String defaultValue){
        String prop = descriptor.getProperty(name, defaultValue);
        if(defaultValue!=null && prop==null) errors.add("missing '"+name+"'");
        return prop;
    }
    private Zone getZoneProperty(String name, ZoneRepository zoneRepository){
        String zone = descriptor.getProperty(name,null);
        if(zone!=null) return zoneRepository.findByLib(zone);
        else           return null;
    }
    private <E extends Enum<E>> E  getGranularityProperty(String name, Class<E> granularity){
        return getGranularityProperty(name, granularity, null);
    }
    private <E extends Enum<E>> E getGranularityProperty(String name, Class<E> granularity, String defaultValue){
        String prop = descriptor.getProperty(name, defaultValue);

        if(prop==null){
            errors.add("missing '"+name+"'");
            return null;
        }
        try{
            return Enum.valueOf(granularity,prop);
        }catch(IllegalArgumentException e){
            errors.add("Unrecognized '"+name+"': "+ prop);
            return null;
        }
    }
    private Date getDateProperty(String name){
        String date = descriptor.getProperty(name,null);
        if(date==null)
            errors.add("missing '"+name+"'");
        else{
            try{
                return DATE_FORMAT.parse(date);
            }catch(ParseException e){
                errors.add("Invalid format of '"+name+"' property ("+date+"): should be YYYY/MM/DD");
            }
        }
        return null;
    }
    private SimpleDateFormat getDatePatternProperty(String name){
        String pattern = descriptor.getProperty(name,null);
        if(pattern==null) return DATE_FORMAT;
        else              return new SimpleDateFormat(pattern);
    }
    private int getColumnProperty(String name, boolean required){
        int prop = Integer.parseInt(descriptor.getProperty(name, "-1"));
        if(required && prop<0) errors.add("Invalid or missing '"+name+"' ("+prop+")");
        return prop;
    }

    // data type and validity
    // ----------------------
    public boolean isValid(){
        return errors.size()==0;
    }
    public boolean isImmigrationData(){
        return (dataType.equals("immigration"));
    }

    // getter
    // ------
    public File getDatasetFile() {
        return datasetFile;
    }
    public List<String> getErrors() {
        return errors;
    }

    public String getMd5() {
        return md5;
    }
    public String getName() {
        return name;
    }
    public String getLabel(){
        return label;
    }

    public Date getFrom() {
        return from;
    }
    public Date getTo() {
        return to;
    }
    public String getProvider() {
        return provider;
    }
    public Zone getOrigin() {
        return origin;
    }
    public Zone getTarget() {
        return target;
    }

    public PeriodGranularity getPeriodGranularity() {
        return periodGranularity;
    }
    public PopulationGranularity getPopulationGranularity() {
        return populationGranularity;
    }
    public ZoneGranularity getZoneGranularity() {
        return zoneGranularity;
    }

    public MoveType getMoveType() {
        return moveType;
    }

    // read movement property
    // ----------------------
    public String readOriginValue(String[] tuple){
        return tuple[originColumn];
    }
    public Zone readOrigin(String[] tuple, ZoneCache zoneCache){
        return readZone(tuple, originColumn, originFormat, originNameColumn, zoneCache);
    }
    public Zone readTarget(String[] tuple, ZoneCache zoneCache){
        return readZone(tuple, targetColumn, targetFormat, targetNameColumn, zoneCache);
    }
    private Zone readZone(String[] tuple, int codeColumn, String format, int nameColumn, ZoneCache zoneCache) {
        String code = tuple[codeColumn];
        String inseeCode;

        // if necessary, convert to INSEE "DDCCC" format code
        if(format!=null && format.equals("POSTAL")){
            String txt;
            if(nameColumn>=0) txt = tuple[nameColumn];
            else              txt = "";

            DataPublicaCommune com = DataPublicaCommune.request(code,txt);
            if(com==null) return null;  // not found
            else          inseeCode = com.getCodeINSEE();
        }else{
            inseeCode = code;
        }

        // dirty trick, for Paris, Lyon & Marseille change region+arr -> region+com
        if (inseeCode.compareTo("75101") >= 0 && inseeCode.compareTo("75120") <= 0) {
            inseeCode = "75056";
        } else if (inseeCode.compareTo("69381") >= 0 && inseeCode.compareTo("69389") <= 0) {
            inseeCode = "69123";
        } else if (inseeCode.compareTo("13201") >= 0 && inseeCode.compareTo("13216") <= 0) {
            inseeCode = "13055";
        }
        return zoneCache.get(inseeCode);
    }

    public Date readFrom(String[] tuple){
        return readDate(tuple, fromColumn, fromPattern);
    }
    public Date readTo(String[] tuple, Date from){
        Date to = readDate(tuple, toColumn, toPattern);

        if(to==null && from!=null){
            Calendar toc = Calendar.getInstance();
            toc.setTime(from);
            switch(periodGranularity){
                case SECOND: toc.add(Calendar.SECOND, 1); break;
                case MINUTE: toc.add(Calendar.MINUTE, 1); break;
                case HOUR:   toc.add(Calendar.HOUR,   1); break;
                case DAY:    toc.add(Calendar.DATE,   1); break;
                case MONTH:  toc.add(Calendar.MONTH,  1); break;
                case YEAR:   toc.add(Calendar.YEAR,   1); break;
                // no other case possible
            }
            to = toc.getTime();
        }
        return to;
    }
    private Date readDate(String[] tuple, int dateColumn, SimpleDateFormat pattern){
        if(dateColumn<0) return null;

        try{
            return pattern.parse(tuple[dateColumn]);
        }catch(ParseException e){
            return null;           // TODO: should print an error?
        }

    }

    public Double readCount(String[] tuple) {
        String c =  tuple[countColumn];
        return new Double(c.replace(',', '.'));
    }
    public Population readPopulation(String[] tuple) {
        Population pop = new Population();

        String p = tuple[populationColumn];
        switch (p.trim()){            // TODO add mapping in descriptor file instead of hard-coded?
            case "Z": pop.setCategory(PopulationCategory.ETUDIANT_INDEPENDANT); break;
            case "1": pop.setCategory(PopulationCategory.AGRICULTURE);          break;
            case "2": pop.setCategory(PopulationCategory.ARTISANT);             break;
            case "3": pop.setCategory(PopulationCategory.CADRES);               break;
            case "4": pop.setCategory(PopulationCategory.INTERMEDIAIRE);        break;
            case "5": pop.setCategory(PopulationCategory.EMPLOYES);             break;
            case "6": pop.setCategory(PopulationCategory.OUVRIERS);             break;
            case "7": pop.setCategory(PopulationCategory.RETRAITES);            break;
            case "8": pop.setCategory(PopulationCategory.SANS_ACTIVITE);        break;
        }

        return pop;
    }
}
