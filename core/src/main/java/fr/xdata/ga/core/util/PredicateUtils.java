package fr.xdata.ga.core.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 */
public class PredicateUtils {

    private static final SimpleDateFormat SQL_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static String buildPredicate(String with, boolean lessThan, boolean orEquals,
                                           Comparable value) {
        String res;
        if (lessThan && !orEquals)
            res = with +" < "+object2HQL(value);
        else if (lessThan)
            res = with +" <= "+object2HQL(value);
        else if (!orEquals)
            res = with +" > "+object2HQL(value);
        else
            res = with +" >= "+object2HQL(value);
        return res;
    }
    public static String buildEqualsPredicate(String with, Comparable value) {
        return with += " = "+ object2HQL(value);
    }

    public static String object2HQL(Object o) {
        if(o instanceof Number) {
            return o.toString();
        } else if(o instanceof String) {
            return "'"+((String) o).replace("'", "\\'")+"'";
        } else if(o instanceof Date) {
            return "'"+SQL_DATE_FORMAT.format((Date) o)+"'";
        }
        throw new IllegalArgumentException("Illegal object to serialize "+o.getClass().getName()+" / "+o.toString());
    }
}
