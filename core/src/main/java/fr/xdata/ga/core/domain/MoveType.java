package fr.xdata.ga.core.domain;

/**
 *
 */
public enum MoveType {
    HOUSING, TOURISM, WORK, SCHOLAR;
}
