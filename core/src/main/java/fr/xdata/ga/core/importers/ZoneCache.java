package fr.xdata.ga.core.importers;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.xdata.ga.core.domain.Zone;
import fr.xdata.ga.core.repositories.ZoneRepository;

import java.util.List;
import java.util.Map;
import java.util.Set;

class ZoneCache{
    private Map<String, Zone> byCode = Maps.newHashMap();
    private Map<String, Zone> byName = Maps.newHashMap();
    private Set<String> missing = Sets.newHashSet();

    public ZoneCache(ZoneRepository zoneRepository){
        List<Zone> all = zoneRepository.findAll();

        for (Zone zone : all) {

            if (zone.getAr() == null && zone.getDep() == null && zone.getCom() == null && zone.getReg() != null) {
                byName.put(zone.getLib(), zone);
            }

            if (zone.getCom() != null) {
                String id = zone.getDep();
                Integer com = zone.getCom();
                String comString = com.toString();

                if (comString.length() == 1) {
                    comString = "00" + comString;
                } else if (comString.length() == 2) {
                    comString = "0" + comString;
                }
                byCode.put(id + comString, zone);
            }
        }
    }

    public Set<String> getMissing(){
        return missing;
    }
    public Zone getWithName(String name){
        return assertZone(byName.get(name), name);
    }
    public Zone getWithCode(String code){
        return assertZone(byCode.get(code), code);
    }
    public Zone get(String codeOrName){
        Zone zone = getWithCode(codeOrName);
        if(zone==null) zone = getWithName(codeOrName);
        return zone;
    }
    private Zone assertZone(Zone zone, String name){
        if (zone == null) {
            //if (codeGeo.compareTo("95") <= 0)
            missing.add(name);
        }
        return zone;
    }
}
