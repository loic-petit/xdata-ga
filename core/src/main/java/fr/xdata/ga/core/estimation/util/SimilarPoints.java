package fr.xdata.ga.core.estimation.util;

import fr.xdata.ga.core.domain.Movement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.List;


public class SimilarPoints {
    private double[] data;
    private double[] timeValues;

    private static final Logger log = LoggerFactory.getLogger(SimilarPoints.class);

    public SimilarPoints() {
    }

    public SimilarPoints(double[] data, double[] timeValues) {
        this.data = data;
        this.timeValues = timeValues;
    }

    public SimilarPoints(List<Movement> movements) {
        data = new double[movements.size()];
        timeValues = new double[movements.size()];
    }

    public double[] getData() {
        return data;
    }

    public void setData(double[] data) {
        this.data = data;
    }

    public double[] getTimeValues() {
        return timeValues;
    }

    public void setTimeValues(List<Movement> movements) {

        for (int i = 0; i < movements.size(); i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(movements.get(i).getFrom());
            timeValues[i] = calendar.get(Calendar.YEAR);
        }
    }

    public void setData(List<Movement> movements) {
        for (int i = 0; i < movements.size(); i++) {
            data[i] = movements.get(i).getCount();
        }
    }

}
