package fr.xdata.ga.core.estimation.imputation;

import fr.xdata.ga.core.estimation.EstimationResult;
import fr.xdata.ga.core.estimation.util.Missing;

/**
 * Estimation de la donnée dans le cas où l'échantillon du passé n'est pas suffisant
 * 1° méthode imputation de la moyenne (pour l'instant)
 * 2° KNN (plus proche voisin)
 * 3° Markov Chain ??
 */
public class NotEnoughDataHandlerTechnique implements Imputation {

    public EstimationResult calculateEstimation(Missing missing) {
        MeanImputationTechnique meanImputationTechnique = new MeanImputationTechnique();
        return meanImputationTechnique.calculateEstimation(missing);

    }

}
