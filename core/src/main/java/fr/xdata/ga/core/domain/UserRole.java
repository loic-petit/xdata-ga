/*
 * Copyright (C) by Data Publica, All Rights Reserved.
 */
package fr.xdata.ga.core.domain;

public enum UserRole {
    Admin, Customer;
}
