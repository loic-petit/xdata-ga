package fr.xdata.ga.core.selection.internal;

/**
 *
 */

import com.google.common.collect.Maps;
import fr.xdata.ga.core.selection.dimensions.DimensionType;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A strategy is a partial function that associates a DSMD with a disjunction of filters. The keys of this function are
 * the datasets to use, are their values contains the filter to apply on each one to count only once the dataset.
 *
 * This concept is comparable in order to give the best (asc order) strategy
 */
public class Strategy extends HashMap<GlobalSelection, ComplexSelection> implements Comparable<Strategy> {
    public Strategy() {
    }

    public Strategy(Map<? extends GlobalSelection, ? extends ComplexSelection> m) {
        super(m);
    }

    private Map<DimensionType, Integer> globalGranularity = Maps.newEnumMap(DimensionType.class);

    private void ensureGlobalGranularity() {
        if(!globalGranularity.isEmpty())
            return;
        Set<GlobalSelection> dataSets = keySet();
        for (DimensionType type : DimensionType.values()) {
            int c = 0;
            for (GlobalSelection ds : dataSets)
                c += ds.G(type);
            globalGranularity.put(type, c);
        }
    }

    @Override
    public int compareTo(Strategy o) {
        // first try to compare the size
        int c = size() - o.size();
        if(c != 0) return c;
        // then try to compare the global granularity of the datasets
        ensureGlobalGranularity();
        o.ensureGlobalGranularity();
        for (DimensionType type : DimensionType.values()) {
            c += globalGranularity.get(type).compareTo(o.globalGranularity.get(type));
        }
        return c;
    }

    private ComplexSelection missing;

    public ComplexSelection getMissing() {
        return missing;
    }

    public void setMissing(ComplexSelection missing) {
        this.missing = missing;
    }
}
