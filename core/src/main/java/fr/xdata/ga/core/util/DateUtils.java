package fr.xdata.ga.core.util;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 */
public class DateUtils {

    public static Date d(Integer year, Integer month, Integer day) {
        return new GregorianCalendar(year, month == null ? 0 : month, day == null ? 1 : day).getTime();
    }

    public static Date d(Integer year, Integer month) {
        return d(year, month, null);
    }

    public static Date d(Integer year) {
        return d(year, null, null);
    }

}
