package fr.xdata.ga.core.selection;

import fr.xdata.ga.core.domain.*;
import fr.xdata.ga.core.exceptions.IllegalGranularityException;

/**
 *
 */
public class Select {
    private Zone origin;
    private ZoneGranularity originGranularity;
    private PopulationGranularity populationGranularity;
    private MoveType type;
    private PopulationCategory population;
    private Period period;
    private PeriodGranularity granularity;
    private Zone target;

    public Select() {
    }

    public Select(MoveType type, PopulationCategory population, Period period, PeriodGranularity granularity,
                    Zone target) throws IllegalGranularityException {
        this(type, population, period, granularity, target, target.computeUpperGranularity(), target.getGranularity());
    }

    public Select(MoveType type, PopulationCategory population, Period period, PeriodGranularity granularity,
                    Zone target, Zone origin) throws IllegalGranularityException {
        this(type, population, period, granularity, target, origin, target.getGranularity());
    }

    public Select(MoveType type, PopulationCategory population, Period period, PeriodGranularity granularity,
                    Zone target, Zone origin, ZoneGranularity originGranularity) throws IllegalGranularityException {
        this(type, population, period, granularity, target, origin, originGranularity,
                        population != null ? PopulationGranularity.SELECTED : PopulationGranularity.ALL);
    }

    public Select(MoveType type, PopulationCategory population, Period period, PeriodGranularity granularity,
                    Zone target, Zone origin, ZoneGranularity originGranularity,
                    PopulationGranularity populationGranularity) throws IllegalGranularityException {
        this.type = type;
        this.population = population;
        this.period = period;
        this.granularity = granularity;
        this.target = target;
        this.origin = origin;
        this.originGranularity = originGranularity;
        this.populationGranularity = populationGranularity;
        if (target.getGranularity().compareTo(originGranularity) > 0)
            throw new IllegalGranularityException(
                            "Trying to create a selection with a granularity more precise than the target");
    }

    public MoveType getType() {
        return type;
    }

    public PopulationCategory getPopulation() {
        return population;
    }

    public Period getPeriod() {
        return period;
    }

    public PeriodGranularity getPeriodGranularity() {
        return granularity;
    }

    public Zone getTarget() {
        return target;
    }

    public ZoneGranularity getOriginGranularity() {
        return originGranularity;
    }

    public void setType(MoveType type) {
        this.type = type;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public void setTarget(Zone target) {
        this.target = target;
    }

    public String toString() {
        return "(" + type + ", " + population + ", " + period + ", " + granularity + ", " + target + ")";
    }

    public Zone getOrigin() {
        return origin;
    }

    public PopulationGranularity getPopulationGranularity() {
        return populationGranularity;
    }
}
