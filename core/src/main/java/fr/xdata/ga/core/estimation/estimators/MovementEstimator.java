package fr.xdata.ga.core.estimation.estimators;

import fr.xdata.ga.core.domain.Movement;
import fr.xdata.ga.core.estimation.EstimationResult;
import fr.xdata.ga.core.estimation.contexts.Context;
import fr.xdata.ga.core.estimation.imputation.Imputation;
import fr.xdata.ga.core.estimation.imputation.SimpleRegressionTechnique;
import fr.xdata.ga.core.estimation.util.Missing;
import fr.xdata.ga.core.estimation.util.SimilarPoints;
import fr.xdata.ga.core.exceptions.IllegalGranularityException;
import fr.xdata.ga.core.exceptions.NotEnoughDataForQueryException;
import fr.xdata.ga.core.selection.QueryResult;
import fr.xdata.ga.core.selection.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;


public class MovementEstimator implements Estimator {

    private Context context;
    //the primary imputation technique
    private Imputation primaryEstimation;
    //a set of alternative imputation techniques
    private Set<Imputation> suitableAlternativeImputations;
    //minimum number of points to judge the reliability of a linear relationship
    public static final int MIN_DATA_SIZE = 3;

    private final SimpleRegressionTechnique simpleRegressionTechnique = new SimpleRegressionTechnique();
    private static final Logger log = LoggerFactory.getLogger(MovementEstimator.class);


    public MovementEstimator(Imputation primaryEstimation, Set<Imputation> imputationSet) {
        this.primaryEstimation = primaryEstimation;
        this.suitableAlternativeImputations = imputationSet;
    }

    @Override
    public List<EstimationResult> strategy(Select givenSelect, QueryResult queryResult) throws NotEnoughDataForQueryException, IllegalGranularityException {
        List<EstimationResult> estimationResultList = new ArrayList<>();
        Set<Missing> missingSet = this.context.contextBuilder(givenSelect, queryResult);
        for (Missing missing : missingSet) {
            EstimationResult estimationResult = this.estimationStrategy(missing, missing.getMovementObjectMissing().getFrom());
            estimationResult.setMovement(missing.getMovementObjectMissing());
            if (Double.isInfinite(estimationResult.getEstimatedValue())) {
                estimationResult.getMovement().setCount(Double.NaN);
            } else {
                estimationResult.getMovement().setCount(estimationResult.getEstimatedValue());
                //we don't add a wrong result to the result lists (in case the estimatedValue is equal to infinity
                estimationResultList.add(estimationResult);
            }
        }
        return estimationResultList;
    }

    @Override
    public boolean judgeReliability(List<Movement> movements) {
        boolean isOk = false;
        SimilarPoints dataSelection = new SimilarPoints(movements);
        dataSelection.setData(movements);
        dataSelection.setTimeValues(movements);
        double[] data = dataSelection.getData();
        double[] timeValues = dataSelection.getTimeValues();
        if (data.length == timeValues.length) {
            if (data.length >= MIN_DATA_SIZE) {
                if (Math.abs(simpleRegressionTechnique.getRSquared(data, timeValues)) >= 0.65) {
                    isOk = true;
                }
            }
            return isOk;
        } else throw new IllegalArgumentException("input data-sets don't have the same size");
    }

    @Override
    public EstimationResult estimationStrategy(Missing missingObject, Object missingPoint) {
        EstimationResult estimatedValue;
        List<Movement> movements = missingObject.getSimilarMovements();
        SimilarPoints selection = new SimilarPoints(movements);
        selection.setData(movements);
        selection.setTimeValues(movements);
        Calendar calendar = Calendar.getInstance();
        double[] data = selection.getData();
        double[] timeValues = selection.getTimeValues();

        for (int i = 0; i < data.length; i++) {
            log.info("time " + i + " = " + timeValues[i]);
            log.info("data " + i + " = " + data[i]);
        }
        calendar.setTime((Date) missingPoint);
        log.info("getting missing date : " + missingObject);
        if (this.judgeReliability(movements)) {
            log.info("judgeReliability OK ");
            //estimatedValue = simpleRegressionTechnique.calculateRegressionEstimate(timeValues, data, missingDate);
            estimatedValue = primaryEstimation.calculateEstimation(missingObject);
            //estimatedValue = simpleRegressionTechnique.calculateEstimation(missingObject);
        } else {
            estimatedValue = this.calculateAlternativeEstimate(missingObject);
        }
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime((Date) missingPoint);
        estimatedValue.setMissing(calendar1.get(Calendar.YEAR));
        estimatedValue.setError(estimatedValue.getError() / estimatedValue.getEstimatedValue() * 100);
        log.info("error set in % : " + estimatedValue.getError());
        if (estimatedValue.getEstimatedValue() < 0) {
            log.info("inadequate value , an error might be negative");
            estimatedValue.setEstimatedValue(Double.POSITIVE_INFINITY);
        }
        return estimatedValue;
    }

    /**
     * as long as judgeReliability returns false, this method is an alternative way to estimate missing data
     *
     * @param missingObject : the missing object & list of movements we got to compute the estimation
     * @return the estimated result
     */
    public EstimationResult calculateAlternativeEstimate(Missing missingObject) {
        EstimationResult estimatedValue = new EstimationResult();

        double[] data = new double[missingObject.getSimilarMovements().size()];
        int i = 0;
        if (missingObject.getSimilarMovements().size() <= 1) {
            estimatedValue.setEstimatedValue(Double.POSITIVE_INFINITY);
            estimatedValue.setError(Double.POSITIVE_INFINITY);
            //c'est là qu'il faut appeler une méthode alternative - not enough data handler, qui actuellement ne fait qu'une imputation de moyenne, donc non fiable lorsqu'il y a qu'une valeur ou moins
        } else {
            for (Movement m : missingObject.getSimilarMovements()) {
                data[i] = m.getCount();
                i++;
            }
            for (Imputation imputation : suitableAlternativeImputations) {
                estimatedValue = imputation.calculateEstimation(missingObject);
                if (!Double.isInfinite(estimatedValue.getEstimatedValue()))
                    break;
            }

        }
        return estimatedValue;
    }

    public Imputation getPrimaryImputation() {
        return primaryEstimation;
    }

    public Set<Imputation> getSuitableAlternativeImputations() {
        return suitableAlternativeImputations;
    }

    public void setPrimaryEstimation(Imputation primaryEstimation) {
        this.primaryEstimation = primaryEstimation;
    }

    public void setSuitableAlternativeImputations(Set<Imputation> suitableImputations) {
        this.suitableAlternativeImputations = suitableImputations;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}