package fr.xdata.ga.core.estimation.imputation;

import fr.xdata.ga.core.estimation.EstimationResult;
import fr.xdata.ga.core.estimation.util.Missing;
import fr.xdata.ga.core.estimation.util.SimilarPoints;
import org.apache.commons.math.stat.regression.SimpleRegression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;


public class SimpleRegressionTechnique implements Imputation {

    private static final Logger log = LoggerFactory.getLogger(SimpleRegressionTechnique.class);

    public SimpleRegressionTechnique() {
    }

    /**
     * returns the value of the R-squared to judge how linear is the relation
     *
     * @param data       the similar points (values)
     * @param timeValues the similar points time values
     * @return the r-square value
     */
    public double getRSquared(double[] data, double[] timeValues) {
        SimpleRegression myRegression = new SimpleRegression();
        for (int i = 0; i < data.length; i++) {
            myRegression.addData(timeValues[i], data[i]);
        }
        log.info("r square " + myRegression.getRSquare());
        return myRegression.getRSquare();
    }
/*

    public EstimationResult calculateRegressionEstimate(double[] xvalues, double[] targetValues, double missingPoint) {
        EstimationResult result = new EstimationResult();
        SimpleRegression myRegression = new SimpleRegression();
        for (int i = 0; i < targetValues.length; i++) {
            myRegression.addData(xvalues[i], targetValues[i]);
        }
        result.setEstimatedValue(myRegression.predict(missingPoint));
        result.setError(Math.pow(myRegression.getMeanSquareError(),0.5));
        double distance = this.evaluateDistance(xvalues,missingPoint);
        if(distance>1) {
            result.setError(result.getError()*(1+distance*0.20));
        }
        //TODO : cas partition mensuelle / journalière, utilisation timestamp?
        log.info(" estimated value " + result.getEstimatedValue() );
        log.info(" error " + result.getError());
        //several ways of generating the error within this class : getTotalSuSquare , getMeanSumSquare ,...
        return result;
    }
*/

    public EstimationResult calculateEstimation(Missing missing) {
        EstimationResult estimationResult = new EstimationResult();
        SimpleRegression myRegression = new SimpleRegression();
        SimilarPoints similarPoints = new SimilarPoints(missing.getSimilarMovements());
        similarPoints.setData(missing.getSimilarMovements());
        similarPoints.setTimeValues(missing.getSimilarMovements());
        for (int i = 0; i < similarPoints.getTimeValues().length; i++) {
            myRegression.addData(similarPoints.getTimeValues()[i], similarPoints.getData()[i]);
        }
        estimationResult.setEstimatedValue(myRegression.predict(this.transformMissingDate(missing)));
        estimationResult.setError(Math.pow(myRegression.getMeanSquareError(), 0.5));
        double distance = this.evaluateDistance(similarPoints.getTimeValues(), this.transformMissingDate(missing));
        if (distance > 1) {
            estimationResult.setError(estimationResult.getError() * (1 + distance * 0.20));
        }
        log.info(" estimated value " + estimationResult.getEstimatedValue());
        log.info(" error " + estimationResult.getError());
        //several ways of generating the error within this class : getTotalSuSquare , getMeanSumSquare ,...
        return estimationResult;
    }

    /**
     * evaluates how far the missing point is from the existing points
     *
     * @param missingCoverage all the similar points x-values (ie time values)
     * @param missingPoint    the x-value  that is missing (ie time value)
     * @return a distance between the closest similar point and the missing point (in terms of date)
     */
    public double evaluateDistance(double[] missingCoverage, double missingPoint) {
        double distance = Math.abs(missingCoverage[0] - missingPoint);
        for (int i = 1; i < missingCoverage.length; i++) {
            if (Math.abs(missingCoverage[i] - missingPoint) < distance) {
                distance = Math.abs(missingCoverage[i] - missingPoint);
            }
        }
        return distance;
    }

    public double transformMissingDate(Missing missing) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(missing.getMovementObjectMissing().getFrom());
        return calendar.get(Calendar.YEAR);
    }
}