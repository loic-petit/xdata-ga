package fr.xdata.ga.core.estimation.imputation;

import fr.xdata.ga.core.estimation.EstimationResult;
import fr.xdata.ga.core.estimation.util.Missing;


public interface Imputation {

    public EstimationResult calculateEstimation(Missing missing);

    //public double calculateError()... ?
}
