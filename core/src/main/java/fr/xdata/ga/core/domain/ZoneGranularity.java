package fr.xdata.ga.core.domain;

/**
 *
 */
public enum ZoneGranularity {
    COMMUNE("com", 0), ARRONDISSEMENT("ar", 10), DEPARTEMENT("dep", 14), REGION("reg", 26), NATION(null, 32);
    private String field;
    private int binaryPosition;

    ZoneGranularity(String field, int binaryPosition) {
        this.field = field;
        this.binaryPosition = binaryPosition;
    }

    public String getField() {
        return field;
    }

    public int getBinaryPosition() {
        return binaryPosition;
    }
}
