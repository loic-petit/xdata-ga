package fr.xdata.ga.core.repositories.Customized;

import fr.xdata.ga.core.domain.Movement;

import java.util.List;

/**
 * Created by Documents on 17/02/14.
 */

public interface MovementRepositoryCustom {

    public Object findTotalNbPeople(Long cogId_origin, Long cogId_target, Integer datasetId);
    public Object findTotalNbPeopleTarget(Long cogId_origin, Long cogId_target, Integer datasetId);
    public Object getStddevOrigin(Integer datasetId, Long origin_cog);
    public Object getStddevTarget(Integer datasetId, Long origin_cog);
    public List<Movement> getSimilarCogId(Integer datasetId, Long originId, Long targetId);
}
