package fr.xdata.ga.core.api;

import com.google.common.collect.Lists;
import fr.xdata.ga.core.importers.DataSetImporter;
import fr.xdata.ga.core.importers.DataSetUriConfig;
import fr.xdata.ga.core.repositories.DataProviderRepository;
import fr.xdata.ga.core.repositories.DataSetRepository;
import fr.xdata.ga.core.repositories.MovementRepository;
import fr.xdata.ga.core.repositories.ZoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 */
@Controller
@RequestMapping
public class ImportDataSets {
    @Autowired
    private ZoneRepository zoneRepository;

    @Autowired
    private MovementRepository movementRepository;

    @Autowired
    private DataSetRepository dataSetRepository;

    @Autowired
    private DataProviderRepository dataProviderRepository;

    @Autowired
    private DataSetUriConfig conf;

    @RequestMapping(method = RequestMethod.GET, value = "/import/datasets", produces = "text/plain; charset=utf-8")
    public @ResponseBody
    String importDataSets2(@RequestParam(required=false, defaultValue="")    String update,
                           @RequestParam(required=false, defaultValue="false") boolean reset) {

        DataSetImporter imp = new DataSetImporter(dataSetRepository, dataProviderRepository, movementRepository, zoneRepository, conf);

        String[] update_list;
        if(update.length()>0) update_list = update.split(",");
        else                  update_list = new String[0];
        Boolean res = imp.importFiles(update_list, reset);

        return res.toString();
    }
}
