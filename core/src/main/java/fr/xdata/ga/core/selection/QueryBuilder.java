package fr.xdata.ga.core.selection;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.xdata.ga.core.domain.*;
import fr.xdata.ga.core.exceptions.NotEnoughDataForQueryException;
import fr.xdata.ga.core.selection.dimensions.DimensionType;
import fr.xdata.ga.core.selection.internal.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.*;

/**
 *
 */
@Service
public class QueryBuilder {
    private static final Logger log = LoggerFactory.getLogger(QueryBuilder.class);

    @Autowired
    private DataSetSelection selection;

    @PersistenceContext
    private EntityManager em;

    public QueryResult query(Select query) throws NotEnoughDataForQueryException {
        Strategy strat = selection.selectSource(query);
        String originSelect = createZoneSelect("m.origin_id", query.getOriginGranularity());
        String hql = "SELECT "+originSelect+" as origin_id, m.moveType, min(m.fromDate) as fromDate, max(m.toDate) as toDate, sum(m.count) as count";
        String fromStatement = "Movement m";

        Map<DimensionType, String> queryFrom = Maps.newEnumMap(DimensionType.class);
        queryFrom.put(DimensionType.CSP, "m");
        queryFrom.put(DimensionType.GEO, "m");
        queryFrom.put(DimensionType.GEO_ORIGIN, "m");
        queryFrom.put(DimensionType.MOVETYPE, "m");
        queryFrom.put(DimensionType.TEMPORAL, "m");

        String finalWhere = null;
        Set<DataSet> dataSets = Sets.newHashSet();
        for (Map.Entry<GlobalSelection, ComplexSelection> entry : strat.entrySet()) {
            GlobalSelection key = entry.getKey();
            DataSet ds = selection.asDataSet(key);
            dataSets.add(ds);
            String pred = entry.getValue().asPredicate(queryFrom);
            if (finalWhere == null)
                finalWhere = "(m.dataSet_id = " + ds.getId() + " AND " + pred + ")";
            else
                finalWhere += " OR (m.dataSet_id = " + ds.getId() + " AND " + pred + ")";
        }

        String groupBy = "m.moveType, " + dateGroupByExpression("m.fromDate", query.getPeriodGranularity().ordinal());
        String origin = createZoneGroupBy("origin_id", query.getOriginGranularity());
        if (origin != null)
            groupBy += ", " + origin;
        //TODO CHEKC NEXT LINE PUT INTO A COMMENT BLOCK
        //groupBy += ", m.category";

        if (!query.getPopulationGranularity().equals(PopulationGranularity.ALL)) {
            //TODO TO BE CHECKED
            groupBy +=", m.category";
            hql += ", m.category";
        } else {
            hql += ", NULL as category";
        }

        hql += " FROM " + fromStatement + " WHERE " + finalWhere + " GROUP BY " + groupBy;

        String join = "origin.id = q.origin_id";
        String target = "target.id = " + query.getTarget().getId();
        String finalQuery = "SELECT "
                        + "origin.id as originId, origin.reg as originReg, origin.dep as originDep, origin.ar as originAr, origin.com as originCom, origin.lib as originLib, "
                        + "target.id as targetId, target.reg as targetReg, target.dep as targetDep, target.ar as targetAr, target.com as targetCom, target.lib as targetLib, "
                        + "q.fromDate, q.toDate, q.count, q.category, q.moveType " + "FROM Zone origin, Zone target, ("
                        + hql + ") q WHERE " + join + " AND " + target;

        log.trace(finalQuery);
        Query sql = em.createNativeQuery(finalQuery);
        log.info("query builder : " + finalQuery);

        List<Movement> movements = constructMovements(sql);

        return new QueryResult(movements, dataSets, strat.getMissing());
    }

    private List<Movement> constructMovements(Query sql) {
        List<Movement> results = Lists.newArrayList();
        List resultList = sql.getResultList();
        for (Object o : resultList) {
            Object[] tuple = (Object[]) o;
            Zone originZone = new Zone((Integer) tuple[1], (String) tuple[2], (Integer) tuple[3], (Integer) tuple[4],
                            (String) tuple[5]);
            originZone.setId(((BigInteger) tuple[0]).longValue());
            Zone targetZone = new Zone((Integer) tuple[7], (String) tuple[8], (Integer) tuple[9], (Integer) tuple[10],
                            (String) tuple[11]);
            targetZone.setId(((BigInteger) tuple[6]).longValue());
            Movement move = new Movement();
            move.setOrigin(originZone);
            move.setTarget(targetZone);
            move.setFrom((Date) tuple[12]);
            move.setTo((Date) tuple[13]);
            move.setCount((Double) tuple[14]);
            if (tuple[15] != null)
                move.setCategory(new Population(PopulationCategory.values()[((int) tuple[15])]));
            move.setMoveType(MoveType.values()[(int) tuple[16]]);
            results.add(move);
        }
        return results;
    }

    private String dateGroupByExpression(String fromPath, Integer granularity) {
        PeriodGranularity[] values = PeriodGranularity.values();
        int l = values.length;
        String result = null;
        for (int i = l - 1; i >= granularity; i--) {
            String exp = values[i].name() + "(" + fromPath + ")";
            if (i < l - 1) {
                if (i > granularity)
                    result = "CONCAT(" + result + ", CONCAT(" + exp + ", '-'))";
                else
                    result = "CONCAT(" + result + ", " + exp + ")";
            } else {
                result = exp;
            }
        }
        return result;
    }

    private String createZoneGroupBy(String originBase, ZoneGranularity granularity) {
        if (granularity == ZoneGranularity.NATION)
            return null;
        if (granularity == ZoneGranularity.COMMUNE)
            return originBase;
        return originBase + " DIV " + (1 << granularity.getBinaryPosition());
    }

    private String createZoneSelect(String originBase, ZoneGranularity granularity) {
        if (granularity == ZoneGranularity.NATION)
            return "0";
        if (granularity == ZoneGranularity.COMMUNE)
            return originBase;
        return "("+originBase + " - "+originBase +" MOD "+ (1 << granularity.getBinaryPosition())+")";
    }

    private String joinZone(String originBase, String originResult, int granularity) {
        String finalQuery = null;
        for (ZoneGranularity z : ZoneGranularity.values()) {
            if (z.compareTo(ZoneGranularity.NATION) >= 0)
                break;
            String q;
            if (z.ordinal() >= granularity) {
                q = originBase + "." + (z.getField()) + " = " + originResult + "." + (z.getField());
            } else {
                q = originResult + "." + (z.getField()) + " IS NULL";
            }
            if (finalQuery == null)
                finalQuery = q;
            else
                finalQuery += " AND " + q;
        }
        return finalQuery;
    }

    public List<DataSet> getDataSetsForZone(Zone z) {

        List<DataSet> sets = Lists.newArrayList();
        Integer zoneGranularityOrdinal = z.getGranularity().ordinal();

        Set<GlobalSelection> list = selection.primarySelectionByDimension(DimensionType.GEO, zoneGranularityOrdinal,
                        DimensionRangeFactory.buildFrom(DimensionType.GEO, z));

        for (GlobalSelection tmp : list) {
            DataSet ds = selection.asDataSet(tmp);
            if (!sets.contains(ds))
                sets.add(ds);
        }

        return sets;
    }

    public Map<DimensionType, Set<Integer>> getConstraints(Integer csp) {

        Map<GlobalSelection, DataSet> sets = selection.getDataSets();
        Map<DimensionType, Set<Integer>> dims = Maps.newHashMap();

        // init the lists
        dims.put(DimensionType.GEO, Sets.<Integer> newHashSet());
        dims.put(DimensionType.MOVETYPE, Sets.<Integer> newHashSet());

        for (GlobalSelection dsmd : sets.keySet()) {
            Integer tmpCsp = dsmd.G(DimensionType.CSP);
            if (csp.equals(tmpCsp)) {
                Integer geo = dsmd.G(DimensionType.GEO);
                Integer mov = dsmd.G(DimensionType.MOVETYPE);
                dims.get(DimensionType.GEO).add(geo);
                dims.get(DimensionType.MOVETYPE).add(mov);
            }
        }

        return dims;
    }

    public List<DataSet> getDataSets() {
        return new ArrayList<>(selection.getDataSets().values());
    }

    public Map<GlobalSelection, DataSet> getDataSetsAndMetaData() {
        return selection.getDataSets();
    }
}
