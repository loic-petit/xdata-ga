package fr.xdata.ga.core.selection.internal;

import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.Range;
import fr.xdata.ga.core.domain.MoveType;
import fr.xdata.ga.core.domain.Period;
import fr.xdata.ga.core.domain.PopulationCategory;
import fr.xdata.ga.core.domain.Zone;
import fr.xdata.ga.core.selection.dimensions.*;

/**
 *
 */
public class DimensionRangeFactory {
    private static Range<Dimension> createRangeFrom(DimensionType type, Object o) {
        // if the value is null, then that means that all values are valid!
        if(o == null && type != DimensionType.MOVETYPE || type == null)
            return Range.all();
        switch (type) {
            case TEMPORAL:
                return DateDimension.createFrom((Period) o);
            case GEO:
            case GEO_ORIGIN:
                return COGDimension.createFrom((Zone) o);
            case MOVETYPE:
                return EnumerateDimension.createFrom((MoveType) o, MoveType.class);
            case CSP:
                return EnumerateDimension.createFrom((PopulationCategory) o, PopulationCategory.class);
        }
        return Range.all();
    }

    @SuppressWarnings("unchecked")
    private static DiscreteDomain<Dimension> getDomainIfPossible(DimensionType type) {
        if(type == null)
            return null;
        switch (type) {
            case MOVETYPE:
                return new EnumerateDomain(MoveType.class);
            case CSP:
                return new EnumerateDomain(MoveType.class);
        }
        return null;
    }

    public static DimensionRange buildFrom(DimensionType type, Object o) {
        return new DimensionRange(createRangeFrom(type, o), getDomainIfPossible(type));
    }
}
