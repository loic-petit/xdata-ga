package fr.xdata.ga.core.estimation.contexts.temporal;


import fr.xdata.ga.core.api.ZoneDetail;
import fr.xdata.ga.core.domain.Movement;
import fr.xdata.ga.core.domain.Period;
import fr.xdata.ga.core.domain.Zone;
import fr.xdata.ga.core.estimation.util.Missing;
import fr.xdata.ga.core.exceptions.IllegalGranularityException;
import fr.xdata.ga.core.exceptions.NotEnoughDataForQueryException;
import fr.xdata.ga.core.repositories.ZoneGisRepository;
import fr.xdata.ga.core.repositories.ZoneRepository;
import fr.xdata.ga.core.selection.QueryBuilder;
import fr.xdata.ga.core.selection.QueryResult;
import fr.xdata.ga.core.selection.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MonthTemporalContextBuilder extends TemporalContextBuilder {

    @Autowired
    private QueryBuilder queryBuilder;
    @Autowired
    private ZoneRepository zoneRepository;
    @Autowired
    private ZoneGisRepository zoneGisRepository;

    private static final Logger log = LoggerFactory.getLogger(MonthTemporalContextBuilder.class);


    //two ways of generating selects : look at the previous and post month, and look at the same months years before
    @Override
    public Period buildNewPeriodForSelect(Date dateFrom) {
        Calendar a = Calendar.getInstance();
        Calendar b = Calendar.getInstance();
        a.setTime(dateFrom);
        b.setTime(dateFrom);
        a.add(Calendar.MONTH, periodBefore);
        b.add(Calendar.MONTH, periodAfter);
        log.info("period built : " + new Period(a.getTime(), b.getTime()));
        return new Period(a.getTime(), b.getTime());
    }


    public Set<Missing> contextBuilder(Select select, QueryResult queryResult) throws NotEnoughDataForQueryException, IllegalGranularityException {
        return getMovementsFromMissingDate(select, getMissingDates(queryResult));
    }

    private Set<Missing> getMovementsFromMissingDate(Select givenSelect, Set<Date> datesMissing) throws IllegalGranularityException, NotEnoughDataForQueryException {
        Set<Missing> missingSet = new HashSet<>();
        for (Date date : datesMissing) {
            ZoneDetail zoneDetail = new ZoneDetail(givenSelect.getOrigin().getId(), zoneRepository, zoneGisRepository);
            List<Zone> zonesChildren = zoneDetail.getChildren();
            for (Zone z : zonesChildren) {
                if (z.getLib().equals("Guadeloupe") || z.getLib().equals("Mayotte") || z.getLib().equals("Martinique") || z.getLib().equals("Guyane") || z.getLib().equals("La Réunion")) {
                    //dom tom non pris en compte
                    continue;
                }
                log.info("zone children " + z.getLib());
                List<Movement> mov = this.getMovements(givenSelect, date, z);

                if (!mov.isEmpty()) {
                    Movement estimatedMovement = buildEstimatedMovement(givenSelect, date, z, mov);
                    missingSet.add(new Missing(estimatedMovement, mov));
                }
            }
        }
        return missingSet;
    }

    private Movement buildEstimatedMovement(Select givenSelect, Date date, Zone z, List<Movement> mov) {
        Movement estimatedMovement = new Movement();
        estimatedMovement.setFrom(date);
        estimatedMovement.setCategory(mov.get(0).getCategory());
        estimatedMovement.setOrigin(z);
        estimatedMovement.setTarget(givenSelect.getTarget());
        estimatedMovement.setMoveType(givenSelect.getType());
        Calendar monthCalendar = Calendar.getInstance();
        monthCalendar.setTime(date);
        monthCalendar.add(Calendar.MONTH, 1);
        estimatedMovement.setTo(monthCalendar.getTime());
        return estimatedMovement;
    }

    public List<Movement> getMovements(Select givenSelect, Date date, Zone z) throws NotEnoughDataForQueryException, IllegalGranularityException {
        Select context = new Select(
                givenSelect.getType(),
                givenSelect.getPopulation(),
                null,
                givenSelect.getPeriodGranularity(),
                givenSelect.getTarget(),
                z,
                givenSelect.getOriginGranularity(),
                givenSelect.getPopulationGranularity()
        );
        context.setPeriod(this.buildNewPeriodForSelect(date));
        return queryBuilder.query(context).getPoints();
    }

    public Set<Date> getMissingDates(QueryResult queryResult) throws NotEnoughDataForQueryException {
        Set<QueryResult.InternalRange> internalRangeMissing = this.getTemporalInternalRangeMissing(queryResult);
        Set<Date> datesMissing = new HashSet<>();
        //iteration on all the internal range of the temporal dimension to get all the missing points
        for (QueryResult.InternalRange internalRange : internalRangeMissing) {
            datesMissing.addAll(this.getAllMissingDates((Date) internalRange.from, (Date) internalRange.to, internalRange.fromExcluded, internalRange.toExcluded));
        }
        return datesMissing;
    }

    public Set<Date> getAllMissingDates(Date dateFrom, Date dateTo, boolean fromExcluded, boolean toExcluded) {
        Set<Date> dateSet = new HashSet<>();
        Calendar a = Calendar.getInstance();
        Calendar b = Calendar.getInstance();
        a.setTime(dateFrom);
        b.setTime(dateTo);
        dateSet.add(dateFrom);
        int diff = 0;
        if (toExcluded) {
            diff = 1;
        }
        getInternalMissingDates(dateSet, a, b, diff, fromExcluded);
        return dateSet;
    }

    private void getInternalMissingDates(Set<Date> dateSet, Calendar a, Calendar b, int diff, boolean fromExcluded) {
        if (fromExcluded)
            a.add(Calendar.MONTH, 1);
        int diff_month = b.get(Calendar.MONTH) - a.get(Calendar.MONTH);
        while (diff_month > diff) {
            a.add(Calendar.MONTH, 1);
            diff_month = b.get(Calendar.MONTH) - a.get(Calendar.MONTH);
            dateSet.add(a.getTime());
        }
    }

    //build a second way of generating similar movements


/*
    public Set<Date> buildSimilarMonths(Date dateFrom) {
        Set<Date> dateSet = new HashSet<>();
        dateSet.addAll(buildBeforeSimilarMonth(dateFrom));
        dateSet.addAll(buildAfterSimilarMonth(dateFrom));
        for(Date d : dateSet) {
            log.info("date : " + d);
        }
        return dateSet;
    }

    private Set<Date> buildAfterSimilarMonth(Date dateFrom) {

        Set<Date> preDateSet = new HashSet<>();
        int j = 0;
        while(j < 5) {
            Calendar monthAfter = Calendar.getInstance();
            monthAfter.setTime(dateFrom);
            monthAfter.add(Calendar.YEAR,-1);
            preDateSet.add(monthAfter.getTime());
            j++;
        }
        return preDateSet;
    }

    private Set<Date> buildBeforeSimilarMonth(Date dateFrom) {
        Set<Date> postDateSet = new HashSet<>();
        for (int i = 1; i < periodBefore+1 ; i++) {
            Calendar monthBefore = Calendar.getInstance();
            monthBefore.setTime(dateFrom);
            monthBefore.add(Calendar.YEAR,-i);
            postDateSet.add(monthBefore.getTime());
        }
        return postDateSet;
    }

*/

}
