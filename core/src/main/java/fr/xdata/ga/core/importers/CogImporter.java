package fr.xdata.ga.core.importers;

import au.com.bytecode.opencsv.CSVReader;
import com.google.common.collect.Lists;
import fr.xdata.ga.core.domain.Zone;
import fr.xdata.ga.core.repositories.ZoneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 *
 */
public class CogImporter {
    private static final Logger log = LoggerFactory.getLogger(CogImporter.class);

    private CogUriConfig conf;

    private ZoneRepository repository;

    public CogImporter(ZoneRepository repository, CogUriConfig conf) {
        this.conf = conf;
        this.repository = repository;
    }

    private enum CogType {
        REG, DEP, ARR, COM
    }

    public void cleanCog(CogType t){
        log.info("clean cog "+t.toString());
        switch(t){
            case REG : repository.deleteRegions(); break;
            case DEP : repository.deleteDepartements(); break;
            case ARR : repository.deleteArrondissements(); break;
            case COM : repository.deleteCommunes(); break;
            default : repository.deleteAll();
        }
    }

    public int importFiles() {
        // list files
        File dir = new File(conf.getCogImporterUri());
        if (!dir.isDirectory())
            return 1;
        File[] files = dir.listFiles();
        if (files == null)
            return 2;

        for (File f : files) {
            if (f.isDirectory())
                continue;
            CogType t;
            if (f.getName().contains("reg"))
                t = CogType.REG;
            else if (f.getName().contains("depts"))
                t = CogType.DEP;
            else if (f.getName().contains("arrond"))
                t = CogType.ARR;
            else if (f.getName().contains("com"))
                t = CogType.COM;
            else
                continue;

            // clean
            cleanCog(t);
            // import
            importFile(f, t);
        }

        return 0;
    }

    private void importFile(File f, CogType t) {
        try {
            log.info("Importing file "+f.getName() +" of type "+t.name());
            CSVReader reader = new CSVReader(new FileReader(f.getAbsolutePath()), '\t');

            // ignore header
            reader.readNext();

            // ok now iterate through the tuples
            String[] tuple;
            List<Zone> zones = Lists.newArrayList();
            while ((tuple = reader.readNext()) != null) {
                Zone z = parseZone(t, tuple);
                if (z != null)
                    zones.add(z);
            }
            log.info("File parsed... saving now");
            repository.save(zones);
            log.info("... done");

        } catch (IOException e) {
            log.warn("Could not import file "+f.getName()+" due to IOException... ignoring", e);
        }
    }

    private Zone parseZone(CogType t, String[] tuple) {
        switch (t) {
            case REG:
                return new Zone(Integer.parseInt(tuple[0]), null, null, null, tuple[4]);
            case DEP:
                prefixDep(tuple, 1);
                return new Zone(Integer.parseInt(tuple[0]), tuple[1], null, null, tuple[5]);
            case ARR:
                prefixDep(tuple, 1);
                return new Zone(Integer.parseInt(tuple[0]), tuple[1], Integer.parseInt(tuple[2]), null,
                                getArticle(tuple[7]) + tuple[8]);
            case COM:
                prefixDep(tuple, 3);
                try {
                    return new Zone(Integer.parseInt(tuple[2]), tuple[3], Integer.parseInt(tuple[5]),
                                    Integer.parseInt(tuple[4]), getArticle(tuple[10]) + tuple[11]);
                } catch (NumberFormatException nfe) {
                    log.warn("Can't insert tuple : " + tuple[2] + ";" + tuple[3] + ";" + tuple[5] + ";" + tuple[4]
                                    + ";" + tuple[11] + "... ignoring");
                }
        }
        return null;
    }

    private void prefixDep(String[] tuple, int i) {
        if (tuple[i].length() == 1)
            tuple[i] = "0" + tuple[i];
    }

    // construct the name with prefix art
    private String getArticle(String s) {
        String art = "";
        if (s != null && !s.equals("")) {
            art = s.substring(1, s.length() - 1);
            if (art.indexOf('\'') == -1)
                art += " ";
        }
        return art;
    }

}
