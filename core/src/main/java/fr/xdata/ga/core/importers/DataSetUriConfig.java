package fr.xdata.ga.core.importers;

import org.springframework.beans.factory.annotation.Required;

/**
 *
 **/
public class DataSetUriConfig {

    private String dataSetImporterUri;

    public DataSetUriConfig(){

    }

    public String getDataSetImporterUri() {
        return dataSetImporterUri;
    }

    @Required
    public void setDataSetImporterUri(String dataSetImporterUri) {
        this.dataSetImporterUri = dataSetImporterUri;
    }

}
