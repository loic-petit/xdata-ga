package fr.xdata.ga.core.repositories;

import fr.xdata.ga.core.domain.ZoneGis;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 */
public interface ZoneGisRepository extends JpaRepository<ZoneGis, Long> {

}
