package fr.xdata.ga.core.selection.internal;

import fr.xdata.ga.core.selection.dimensions.DimensionType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * Consider two sets AxB and CxD, both subsets of MxN. The substraction of AxB by CxD may not be possible to be
 * represented in a cartesian product form.
 * 
 * Therefore, this class formalise the concept of union of cartesian product form. We can now easily represents the fact
 * that AxB - CxD = (A-C)xB U Cx(B-D) [and øxZ = ø obviously]
 */
public class ComplexSelection extends ArrayList<GlobalSelection> {
    /**
     * Initialize a disjunctive form with one atom. It is put inside iff it's empty
     * 
     * @param original the atom
     */
    public ComplexSelection(GlobalSelection original) {
        if (!original.isEmpty())
            add(original);
    }

    /**
     * Construct a new disjunctive form by performing the operation similar to AxB - CxD. Emptysets won't be inserted
     * inside the form
     * 
     * @param original The left side
     * @param remove The right side
     */
    public ComplexSelection(GlobalSelection original, GlobalSelection remove) {
        if (original.isEmpty())
            // nothing to do here
            return;
        for (DimensionType type : DimensionType.values()) {
            DimensionRange duplicate = original.selections.get(type).duplicate()
                            .remove(remove.selections.get(type));
            if (duplicate.isEmpty())
                continue;

            GlobalSelection ds = new GlobalSelection(original);
            ds.selections.put(type, duplicate);
            add(ds);
        }
    }

    /**
     * Perform the same operation as the other constructor on already well-formed expression. As the set minus operation
     * is distributive over the union, then the operation is applied on each atom
     * 
     * @param original The left side
     * @param remove The right side
     */
    public ComplexSelection(ComplexSelection original, GlobalSelection remove) {
        // it is distributive for disjunction
        for (GlobalSelection ds : original) {
            for (GlobalSelection meta : new ComplexSelection(ds, remove)) {
                add(meta);
            }
        }
    }

    public ComplexSelection(ComplexSelection original, ComplexSelection missing) {
        // C \ ø = C
        if(missing.isEmpty()) {
            addAll(original);
        } else {
            // C \ (A U B)  = (C \ A) \ B
            for (GlobalSelection remove : missing) {
                original = new ComplexSelection(original, remove);
            }
            addAll(original);
        }
    }

    /**
     * Add a dsmd to the disjunction. This implementation tries to do a proper union with the current list of dsmd.
     *
     * For instance, AxBxC U AxBxD = AxBx(C U D). So this implementation optimize the representation with this trick
     *
     * @param dsmd The dsmd to add
     * @return true
     */
    @Override
    public boolean add(GlobalSelection dsmd) {
        // try to add
        boolean found = false;
        for (GlobalSelection ds : this) {
            for (DimensionType tryType : DimensionType.values()) {
                if(!areDSMDEquals(dsmd, ds, tryType))
                    continue;
                ds.S(tryType).union(dsmd.S(tryType));
                found = true;
                break;
            }
            if(found)
                break;
        }
        if(!found)
            super.add(dsmd);
        return true;
    }

    private boolean areDSMDEquals(GlobalSelection dsmd, GlobalSelection ds, DimensionType except) {
        boolean equals = true;
        for (DimensionType type : DimensionType.values()) {
            if(type == except)
                continue;
            if(!dsmd.S(type).rangeEquals(ds.S(type))) {
                equals = false;
                break;
            }
        }
        return equals;
    }

    @Override
    public boolean addAll(Collection<? extends GlobalSelection> c) {
        for (GlobalSelection ds : c) {
            add(ds);
        }
        return true;
    }

    public void clean() {

    }

    public String asPredicate(Map<DimensionType, String> query) {
        String finalQuery = null;
        for (GlobalSelection dsmd : this) {
            if(finalQuery == null)
                finalQuery = dsmd.asPredicate(query);
            else
                finalQuery += " OR "+dsmd.asPredicate(query);
        }
        return "("+finalQuery+")";
    }
}
