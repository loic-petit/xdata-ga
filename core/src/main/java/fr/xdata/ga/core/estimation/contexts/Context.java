package fr.xdata.ga.core.estimation.contexts;

import fr.xdata.ga.core.estimation.util.Missing;
import fr.xdata.ga.core.exceptions.IllegalGranularityException;
import fr.xdata.ga.core.exceptions.NotEnoughDataForQueryException;
import fr.xdata.ga.core.selection.QueryResult;
import fr.xdata.ga.core.selection.Select;

import java.util.Set;


public interface Context {

    Set<Missing> contextBuilder(Select givenSelect, QueryResult queryResult) throws NotEnoughDataForQueryException, IllegalGranularityException;

    //given a dimension, dynamically chooses a context ???


}
