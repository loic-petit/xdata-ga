package fr.xdata.ga.core.estimation;

import fr.xdata.ga.core.domain.MoveType;
import fr.xdata.ga.core.estimation.contexts.spatial.SpatialContextBuilder;
import fr.xdata.ga.core.estimation.contexts.temporal.DayTemporalContextBuilder;
import fr.xdata.ga.core.estimation.contexts.temporal.MonthTemporalContextBuilder;
import fr.xdata.ga.core.estimation.contexts.temporal.YearTemporalContextBuilder;
import fr.xdata.ga.core.estimation.estimators.DensityEstimator;
import fr.xdata.ga.core.estimation.estimators.Estimator;
import fr.xdata.ga.core.estimation.estimators.MovementEstimator;
import fr.xdata.ga.core.estimation.imputation.Imputation;
import fr.xdata.ga.core.estimation.imputation.MeanImputationTechnique;
import fr.xdata.ga.core.estimation.imputation.NotEnoughDataHandlerTechnique;
import fr.xdata.ga.core.estimation.imputation.SimpleRegressionTechnique;
import fr.xdata.ga.core.selection.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class EstimationStrategy {
    private static final Logger log = LoggerFactory.getLogger(EstimationStrategy.class);

    @Autowired
    private YearTemporalContextBuilder yearTemporalContextBuilder;
    @Autowired
    private MonthTemporalContextBuilder monthTemporalContextBuilder;
    @Autowired
    private DayTemporalContextBuilder dayTemporalContextBuilder;
    @Autowired
    private SpatialContextBuilder spatialContextBuilder;

    public EstimationStrategy() {
    }

    public Estimator selectSuitableEstimator(Select givenSelect) {

        if (givenSelect.getType() == MoveType.WORK || givenSelect.getType() == MoveType.HOUSING || givenSelect.getType() == MoveType.SCHOLAR || givenSelect.getType() == MoveType.TOURISM) {
            log.info("return movement estimator" + "primaryEstimation : " + this.primaryImputation(givenSelect) + "set of alternative estimations : " + this.selectAlternativeImputations(givenSelect));
            MovementEstimator movementEstimator = new MovementEstimator(this.primaryImputation(givenSelect), this.selectAlternativeImputations(givenSelect));
            switch (givenSelect.getPeriodGranularity()) {
                case YEAR:
                    movementEstimator.setContext(yearTemporalContextBuilder);
                    break;
                case MONTH:
                    movementEstimator.setContext(monthTemporalContextBuilder);
                    break;
                case DAY:
                    movementEstimator.setContext(dayTemporalContextBuilder);
                    break;
            }
            return movementEstimator; //MovementEstimator(context,this.primaryImputation(givenSelect),this.selectAlternativeImputations(givenSelect));
        } else
            return new DensityEstimator(spatialContextBuilder, this.primaryImputation(givenSelect), this.selectAlternativeImputations(givenSelect));
    }

    public Imputation primaryImputation(Select givenSelect) {
        if (givenSelect.getType() == MoveType.WORK || givenSelect.getType() == MoveType.HOUSING || givenSelect.getType() == MoveType.SCHOLAR || givenSelect.getType() == MoveType.TOURISM)
            return new SimpleRegressionTechnique();
        else
            return new MeanImputationTechnique();
    }

    public Set<Imputation> selectAlternativeImputations(Select givenSelect) {
        Set<Imputation> alternativeSuitableImputations = new HashSet<>();
        if (givenSelect.getType() == MoveType.WORK || givenSelect.getType() == MoveType.HOUSING || givenSelect.getType() == MoveType.SCHOLAR || givenSelect.getType() == MoveType.TOURISM) {
            alternativeSuitableImputations.clear();
            alternativeSuitableImputations.add(new MeanImputationTechnique());
            alternativeSuitableImputations.add(new NotEnoughDataHandlerTechnique());
        } else
            alternativeSuitableImputations.clear();

        return alternativeSuitableImputations;
    }

}
