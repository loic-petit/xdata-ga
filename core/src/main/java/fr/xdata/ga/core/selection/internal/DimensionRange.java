package fr.xdata.ga.core.selection.internal;

import com.google.common.collect.*;
import fr.xdata.ga.core.selection.dimensions.Dimension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;

/**
 * Implementation of the set of possibilities for a specific dimension This class implements numerous methods to
 * restrict or expand the set.
 * 
 * It also can provide the predicate to use in a JPA criteria builder
 */
public class DimensionRange {
    private RangeSet<Dimension> set;

    /**
     * Internal clone constructor
     * 
     * @param set the content
     */
    protected DimensionRange(RangeSet<Dimension> set, DiscreteDomain<Dimension> domain) {
        this.set = set;
        this.domain = domain;
    }

    /**
     * Create an empty set
     */
    public DimensionRange() {
        set = TreeRangeSet.create();
    }

    /**
     * Create a set initialized with the given range
     * 
     * @param base the base range
     */
    public DimensionRange(Range<Dimension> base, DiscreteDomain<Dimension> domain) {
        this();
        this.domain = domain;
        if(domain != null)
            base.canonical(domain);
        set.add(base);
    }

    /**
     * Is the set empty?
     * 
     * @return true if empty
     */
    public boolean isEmpty() {
        return set.isEmpty();
    }

    private DiscreteDomain<Dimension> domain;
    private void canonical() {
        if(domain == null)
            return;
        TreeRangeSet<Dimension> newRangeSet = TreeRangeSet.create();
        for (Range<Dimension> range : set.asRanges()) {
            if(!range.hasLowerBound() || !range.hasUpperBound()) {
                range = range.intersection(Range.closed(domain.minValue(), domain.maxValue()));
            }
            if(range.isEmpty())
                continue;
            Range<Dimension> canonicalRange = range.canonical(domain);
            if(!canonicalRange.isEmpty())
                newRangeSet.add(canonicalRange);
        }
        set = newRangeSet;
    }

    /**
     * Is the set included in source
     *
     * @return true if this ⊆ source
     */
    public boolean isSubsetOf(DimensionRange source) {
        return source.set.enclosesAll(set);
    }

    /**
     * Perform the set operation this <- this \ source.
     * 
     * @param source the set to remove
     * @return this instance for further operations
     */
    public DimensionRange remove(DimensionRange source) {
        set.removeAll(source.set);
        canonical();
        return this;
    }

    /**
     * Perform the set operation this <- this ∩ source.
     * 
     * @param source the set to intersect
     * @return this instance for further operations
     */
    public DimensionRange restrict(DimensionRange source) {
        set.removeAll(source.set.complement());
        canonical();
        return this;
    }

    /**
     * Perform the set operation this <- this ∪ source.
     * 
     * @param source the set to union
     * @return this instance for further operations
     */
    public DimensionRange union(DimensionRange source) {
        set.addAll(source.set);
        return this;
    }

    /**
     * Clone this set to avoid unwanted interactions
     * 
     * @return a new instance of the same set
     */
    public DimensionRange duplicate() {
        return new DimensionRange(TreeRangeSet.create(set), domain);
    }

    private static final Logger log = LoggerFactory.getLogger(DimensionRange.class);

    /**
     * Create a JPA Predicate of this set ready to be inserted in a criteria query
     * 
     *
     * @param query
     * @param from The expression (typically a Path) that evaluate to the lower bound of the criteria
     * @param end The expression (typically a Path) that evaluate to the lower bound of the criteria. Can be null, if so
     * @param upperBoundAlwaysIncluded
     * */
    public String asPredicate(String query, String from, String end, boolean upperBoundAlwaysIncluded) {
        if (end == null)
            end = from;
        Set<Range<Dimension>> ranges = set.asRanges();
        List<String> or = Lists.newArrayList();
        if(isEmpty())
            return "1=0";
        for (Range<Dimension> range : ranges) {
            if (!(range.hasLowerBound() && range.hasUpperBound()))
                continue;
            String lower = null, upper = null;
            if (range.hasLowerBound()) {
                lower = range.lowerEndpoint().compareWith(query, from, false,
                        range.lowerBoundType() == BoundType.CLOSED);
            }

            if (range.hasUpperBound()) {
                upper = range.upperEndpoint().compareWith(query, end, true,
                                range.upperBoundType() == BoundType.CLOSED || upperBoundAlwaysIncluded);
            }
            if (lower != null && upper != null)
                or.add("("+lower +" AND "+ upper+")");
            else if (lower != null)
                or.add(lower);
            else if (upper != null)
                or.add(upper);
        }
        // case if the set is ]-∞, +∞[
        if (or.isEmpty())
            return "1=1";
        else if (or.size() == 1)
            return or.get(0);
        String finalQuery = null;
        for (String s : or) {
            if(finalQuery == null)
                finalQuery = s;
            else
                finalQuery += " OR "+s;
        }

        return "("+finalQuery+")";
    }

    @Override
    public String toString() {
        return set.toString();
    }

    public Set<Range<Dimension>> getRangeSet() {
        return set.asRanges();
    }

    public boolean rangeEquals(DimensionRange s) {
        return set.equals(s.set);
    }
}
