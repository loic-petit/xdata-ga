package fr.xdata.ga.core.domain;

import javax.persistence.*;

/**
 * Represents a (persistent) dataset
 */
@Entity
public class DataSet {
    @Id
    @GeneratedValue
    private int id;

    private String label;

    @ManyToOne(optional = false)
    private DataProvider provider;

    @Column(nullable = false)
    private String md5sum;

    private MoveType moveType;

    // time capacity
    @Embedded
    private Period timeFilter;
    @Column(nullable = false)
    private PeriodGranularity timeGranularity;

    // geo capacity
    @ManyToOne
    private Zone originGeoFilter;
    @ManyToOne
    private Zone targetGeoFilter;
    @Column(nullable = false)
    private ZoneGranularity geoGranularity;

    // population capacity
    @Embedded
    private Population populationFilter;
    @Column(nullable = false)
    private PopulationGranularity populationGranularity;

    public DataSet() {
    }

    public DataSet(int id, String label, DataProvider provider, String md5sum, MoveType moveType, Period timeFilter,
                    PeriodGranularity timeGranularity, Zone originGeoFilter, Zone targetGeoFilter, ZoneGranularity geoGranularity,
                    Population populationFilter, PopulationGranularity populationGranularity) {
        this.id = id;
        this.label = label;
        this.provider = provider;
        this.md5sum = md5sum;
        this.moveType = moveType;
        this.timeFilter = timeFilter;
        this.timeGranularity = timeGranularity;
        this.originGeoFilter = originGeoFilter;
        this.targetGeoFilter = targetGeoFilter;
        this.geoGranularity = geoGranularity;
        this.populationFilter = populationFilter;
        this.populationGranularity = populationGranularity;
    }

    // getter-setter

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DataProvider getProvider() {
        return provider;
    }

    public void setProvider(DataProvider provider) {
        this.provider = provider;
    }

    public Period getTimeFilter() {
        return timeFilter;
    }

    public void setTimeFilter(Period timeFilter) {
        this.timeFilter = timeFilter;
    }

    public PeriodGranularity getTimeGranularity() {
        return timeGranularity;
    }

    public void setTimeGranularity(PeriodGranularity timeGranularity) {
        this.timeGranularity = timeGranularity;
    }

    public Zone getOriginGeoFilter() {
        return originGeoFilter;
    }

    public void setOriginGeoFilter(Zone originGeoFilter){
        this.originGeoFilter = originGeoFilter;
    }

    public Zone getTargetGeoFilter(){
        return targetGeoFilter;
    }

    public void setTargetGeoFilter(Zone targetGeoFilter) {
        this.targetGeoFilter = targetGeoFilter;
    }

    public ZoneGranularity getGeoGranularity() {
        return geoGranularity;
    }

    public void setGeoGranularity(ZoneGranularity geoGranularity) {
        this.geoGranularity = geoGranularity;
    }

    public Population getPopulationFilter() {
        return populationFilter;
    }

    public void setPopulationFilter(Population populationFilter) {
        this.populationFilter = populationFilter;
    }

    public PopulationGranularity getPopulationGranularity() {
        return populationGranularity;
    }

    public void setPopulationGranularity(PopulationGranularity populationGranularity) {
        this.populationGranularity = populationGranularity;
    }

    public MoveType getMoveType() {
        return moveType;
    }

    public void setMoveType(MoveType moveType) {
        this.moveType = moveType;
    }

    public String getMd5sum() {
        return md5sum;
    }

    public void setMd5sum(String md5sum) {
        this.md5sum = md5sum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataSet dataSet = (DataSet) o;

        if (id != dataSet.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
