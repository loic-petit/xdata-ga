package fr.xdata.ga.core.repositories;

import fr.xdata.ga.core.domain.DataSet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
public interface DataSetRepository extends JpaRepository<DataSet, Integer> {

    public DataSet findDataSetByMd5sum(String md5sum);

    @Modifying
    @Transactional
    @Query("delete from DataSet d where d.id = ?1")
    public void deleteDataSetById(Integer id);

}
