/*
 * Copyright (C) by Data Publica, All Rights Reserved.
 */
package fr.xdata.ga.core.repositories;

import fr.xdata.ga.core.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    public User findByEmail(String email);

}
