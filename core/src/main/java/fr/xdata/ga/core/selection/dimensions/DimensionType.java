package fr.xdata.ga.core.selection.dimensions;

/**
 *
 */
public enum DimensionType {
    TEMPORAL("fromDate", "toDate"), GEO("target_id"), GEO_ORIGIN("origin_id"), MOVETYPE("moveType"), CSP("category");

    private String from;
    private String end;
    private DimensionType(String from, String end) {
        this.from = from;
        this.end = end;
    }

    private DimensionType(String from) {
        this.from = from;
        this.end = from;
    }

    public String getUpperBoundField() {
        return end;
    }

    public String getLowerBoundField() {
        return from;
    }

    public boolean isUpperBoundAlwaysIncluded() {
        return !end.equals(from);
    }
}
