package fr.xdata.ga.core.repositories;

import fr.xdata.ga.core.domain.DataProvider;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 */
public interface DataProviderRepository extends JpaRepository<DataProvider, Integer> {
    DataProvider findByProviderName(String insee);
}
