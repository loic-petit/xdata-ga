package fr.xdata.ga.core.selection.dimensions;

import com.google.common.collect.DiscreteDomain;

/**
 *
 */
public class EnumerateDomain extends DiscreteDomain<Dimension> {
    private Class<? extends Enum> c;

    public EnumerateDomain(Class<? extends Enum> c) {
        this.c = c;
    }

    @Override
    public EnumerateDimension next(Dimension dim_) {
        EnumerateDimension dim = (EnumerateDimension) dim_;
        if(dim.value == c.getEnumConstants().length-1)
            return null;
        return new EnumerateDimension(dim.value+1, c);
    }

    @Override
    public EnumerateDimension previous(Dimension dim_) {
        EnumerateDimension dim = (EnumerateDimension) dim_;
        if(dim.value == 0)
            return null;
        return new EnumerateDimension(dim.value-1, c);
    }

    @Override
    public EnumerateDimension minValue() {
        return new EnumerateDimension(0, c);
    }

    @Override
    public EnumerateDimension maxValue() {
        return new EnumerateDimension(c.getEnumConstants().length-1, c);
    }

    @Override
    public long distance(Dimension dim1_, Dimension dim2_) {
        return ((EnumerateDimension)dim2_).value - ((EnumerateDimension)dim1_).value;
    }


}
