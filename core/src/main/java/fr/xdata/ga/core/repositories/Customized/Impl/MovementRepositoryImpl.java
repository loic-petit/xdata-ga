package fr.xdata.ga.core.repositories.Customized.Impl;

import fr.xdata.ga.core.domain.Movement;
import fr.xdata.ga.core.repositories.Customized.MovementRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;


/**
 * Created by Documents on 17/02/14.
 */
public class MovementRepositoryImpl implements MovementRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    public MovementRepositoryImpl() {
    }

    @Override
    public Object findTotalNbPeople(Long cogId_origin, Long cogId_target, Integer datasetId) {
        TypedQuery<Double> total= (TypedQuery<Double>) em.createNativeQuery("select sum(NB_PEOPLE_MOVING) AS TOTAL from " +
                   "(select m.origin_id AS ID_COMMUNE_ORIGIN, sum(m.count) AS NB_PEOPLE_MOVING, z.lib AS NAME_COMMUNE_ORIGIN " +
                   "from Movement m, Zone z " +
                   "where m.target_id=:2  and " +
                   "z.id=:1 and " +
                   "m.dataset_id=:3 and " +
                   "m.origin_id not in (select m.origin_id from Movement m where m.target_id=:2 and m.origin_id=:1)" +
                   "group by ID_COMMUNE_ORIGIN) C");
                   total.setParameter("1", cogId_origin);
                   total.setParameter("2", cogId_target);
                   total.setParameter("3", datasetId);



                //checker set paramaters avec query
        return (Double) total.getSingleResult();

    }

    @Override
    public Double findTotalNbPeopleTarget(Long cogId_origin, Long cogId_target, Integer datasetId) {
            TypedQuery<Double> nbe = (TypedQuery<Double>) em.createNativeQuery("select sum(NB_PEOPLE_MOVING) AS TOTAL from " +
                    "(select m.target_id AS ID_COMMUNE_TARGET, sum(m.count) AS NB_PEOPLE_MOVING, z.lib AS NAME_COMMUNE_TARGET " +
                    "from Movement m, Zone z " +
                    "where m.origin_id=:cogId_origin and " +
                    "z.id=m.target_id and " +
                    "m.dataset_id=:datasetId and " +
                    "m.target_id not in (select m.target_id from Movement m where m.target_id=:cogId_target and m.origin_id=:cogId_origin) " +
                    "group by ID_COMMUNE_TARGET) C");
            nbe.setParameter("cogId_origin", cogId_origin) ;
            nbe.setParameter("cogId_target", cogId_target);
            nbe.setParameter("datasetId", datasetId);
            return (Double) nbe.getSingleResult();
        }

    @Override
    public Double getStddevOrigin(Integer datasetId, Long origin_cog) {
        TypedQuery<Double> stdo= (TypedQuery<Double>) em.createNativeQuery("select std(m.count) as std from Movement m where m.dataset_id=:datasetId and m.origin_id=:origin_cog");
        stdo.setParameter("datasetId", datasetId);
        stdo.setParameter("target_cog", origin_cog);
        return (Double) stdo.getSingleResult();
    }

    @Override
    public Double getStddevTarget(Integer datasetId, Long target_cog) {


            TypedQuery<Double> stdo= (TypedQuery<Double>) em.createNativeQuery("select std(m.count) as std from Movement m where m.dataset_id=:datasetId and m.target_id=:target_cog");
            stdo.setParameter("datasetId", datasetId);
            stdo.setParameter("target_cog", target_cog);
            return (Double) stdo.getSingleResult();
    }

    @Override
    public List<Movement> getSimilarCogId(Integer datasetId, Long originId, Long targetId) {
        TypedQuery<Movement> listMovement = (TypedQuery<Movement>) em.createNativeQuery(" select * from Movement m where m.dataset_id=:datasetId and " +
                "m.origin_id=:originId and m.target_id = :targetId");
                listMovement.setParameter("datasetId", datasetId);
                listMovement.setParameter("originId", originId + "%");
                listMovement.setParameter("targetId", targetId);
        return  listMovement.getResultList();
    }

}


