package fr.xdata.ga.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 */
@Entity
public class ZoneGis {
    @Id
    private Long id;
    @Column(columnDefinition = "varchar(4095)")
    private String path;
    private double centerX;
    private double centerY;

    public ZoneGis() {
    }

    public ZoneGis(Long id, String path, double centerX, double centerY) {
        this.id = id;
        this.path = path;
        this.centerX = centerX;
        this.centerY = centerY;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public double getCenterX() {
        return centerX;
    }

    public void setCenterX(double centerX) {
        this.centerX = centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    public void setCenterY(double centerY) {
        this.centerY = centerY;
    }
}
