package fr.xdata.ga.core.importers;

import org.springframework.beans.factory.annotation.Required;

/**
 *
 */
public class CogUriConfig {

    private String cogImporterUri;

       public CogUriConfig(){

       }

    public String getCogImporterUri() {
        return cogImporterUri;
    }

    @Required
    public void setCogImporterUri(String cogImporterUri) {
        this.cogImporterUri = cogImporterUri;
    }
}
