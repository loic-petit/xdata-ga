package fr.xdata.ga.core.domain;

import java.util.Date;

import javax.persistence.*;  // Entity, GeneratedValue, Column, Embedded, ManyToOne

import org.hibernate.annotations.Index;

/**
 * Persistent class to represent movement entities: the main DB data type
 *
 * A Movement is described by, and contains:
 * <p><ul>
 *  <li> dataset: the {@link DataSet} it is part of
 *  <li> count:  **???**
 *  <li> category: the {@link Population} category it applies to
 *  <li> from: the {@link Date} it started at
 *  <li> to: the {@link Date} it ended
 *  <li> moveType: the {@link MoveType} of this movement
 *  <li> origin: the {@link Zone} it started from
 *  <li> target: the {@link Zone} it ended at
 * </ul></p>
 * @author Emmanuel Castanier
 * @author Loic Petit
 */
@Entity
@org.hibernate.annotations.Table(appliesTo = "Movement", indexes = { @Index(name = "main", columnNames = {
                "dataSet_id", "fromDate", "toDate", "category", "moveType", "target_id", "origin_id", "count" }) })
public class Movement {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    private DataSet dataSet;

    private double count;

    @Column(name="fromDate")
    private Date from;
    @Column(name="toDate")
    private Date to;
    private MoveType moveType;

    @Embedded
    private Population category;

    @ManyToOne
    private Zone origin;
    @ManyToOne
    private Zone target;

    public Movement(){

    }

    public DataSet getDataSet() {
        return dataSet;
    }

    public void setDataSet(DataSet dataSet) {
        this.dataSet = dataSet;
    }

    public Zone getTarget() {
        return target;
    }

    public void setTarget(Zone target) {
        this.target = target;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public MoveType getMoveType() {
        return moveType;
    }

    public void setMoveType(MoveType moveType) {
        this.moveType = moveType;
    }

    public Zone getOrigin() {
        return origin;
    }

    public void setOrigin(Zone origin) {
        this.origin = origin;
    }

    public Population getCategory() {
        return category;
    }

    public void setCategory(Population category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Movement{" +
                "id=" + id +
                ", dataSet=" + dataSet +
                ", count=" + count +
                ", from=" + from +
                ", to=" + to +
                ", moveType=" + moveType +
                ", category=" + category +
                ", origin=" + origin +
                ", target=" + target +
                '}';
    }
}
