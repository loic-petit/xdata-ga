package fr.xdata.ga.core.selection.dimensions;

import com.google.common.collect.Range;
import fr.xdata.ga.core.util.PredicateUtils;

/**
 *
 */
public class EnumerateDimension implements Dimension {
    protected int value;
    private Class<? extends Enum> c;

    public EnumerateDimension(int value, Class<? extends Enum> c) {
        this.value = value;
        this.c = c;
    }

    public static Range<Dimension> createFrom(Enum o, Class<? extends Enum> c) {
        if(o == null) {
            Enum[] v = c.getEnumConstants();
            return Range.closed((Dimension) new EnumerateDimension(0, c), new EnumerateDimension(v.length-1, c));
        }
        return Range.closed((Dimension)new EnumerateDimension(o.ordinal(),c), new EnumerateDimension(o.ordinal(),c));
    }

    @Override
    public String compareWith(String query, String with, boolean lessThan, boolean orEquals) {
        return PredicateUtils.buildPredicate(with, lessThan, orEquals, value);
    }

    @Override
    public Object asOriginal() {
        return c.getEnumConstants()[value];
    }

    @Override
    public int compareTo(Dimension r) {
        return value - ((EnumerateDimension) r).value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }


}
