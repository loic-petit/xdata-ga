package fr.xdata.ga.core.api;

import fr.xdata.ga.core.domain.*;
import fr.xdata.ga.core.estimation.EstimationResult;
import fr.xdata.ga.core.estimation.EstimationStrategy;
import fr.xdata.ga.core.exceptions.IllegalGranularityException;
import fr.xdata.ga.core.exceptions.NotEnoughDataForQueryException;
import fr.xdata.ga.core.repositories.MovementRepository;
import fr.xdata.ga.core.repositories.ZoneGisRepository;
import fr.xdata.ga.core.repositories.ZoneRepository;
import fr.xdata.ga.core.selection.QueryBuilder;
import fr.xdata.ga.core.selection.QueryResult;
import fr.xdata.ga.core.selection.Select;
import fr.xdata.ga.core.selection.dimensions.DimensionType;
import fr.xdata.ga.core.selection.internal.GlobalSelection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 *
 */
@Controller
@RequestMapping
public class API {
    @Autowired
    private MovementRepository movementRepository;
    @Autowired
    private ZoneRepository zoneRepository;
    @Autowired
    private ZoneGisRepository zoneGisRepository;
    @Autowired
    private EstimationStrategy estimationStrategy;


    /*
     * @RequestMapping(method = RequestMethod.GET, value = "/test/{param}", produces = "text/plain; charset=utf-8")
     * public @ResponseBody String test(@PathVariable String param) { return param; }
     */
    @Autowired
    private QueryBuilder queryBuilder;

    /**
     * Call this method to get the list of all datasets wich contains at least one instance using the Zone
     * passed in param.
     *
     * @return a JSON list of datasets
     */
    @RequestMapping(method = RequestMethod.GET, value = "/datasets", produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    List<DataSet> getDataSets(@RequestParam(required = false) Long cogId) {
        if (cogId == null) {
            return queryBuilder.getDataSets();
        } else {
            return queryBuilder.getDataSetsForZone(zoneRepository.findOne(cogId));
        }
    }


    /**
     * Call this method to get the list of all datasets wich contains at least one instance using the Zone
     * passed in param.
     *
     * @return a JSON list of datasets
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{cogId}/datasets", produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    List<DataSet> getDataSetsFromZone(@PathVariable Long cogId) {
        return getDataSets(cogId);
    }

    /**
     * Call this method to get the list of all datasets wich contains at least one instance using the Zone
     * passed in param.
     *
     * @return a JSON list of datasets
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{cogId}/zone", produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    ZoneDetail zoneDetail(@PathVariable Long cogId) {
        return new ZoneDetail(cogId, zoneRepository, zoneGisRepository);
    }


    /**
     * This method use the CSP constraint to get usable other constraints (geo and move)
     *
     * @param csp
     * @return A simple map of dimension types
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{cogId}/constraints", produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    Map<DimensionType, Set<Integer>> constraints(@PathVariable Long cogId, @RequestParam(required = true) Integer csp) {
        Zone z = zoneRepository.findOne(cogId);
        return queryBuilder.getConstraints(csp);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/getDataSetsAndMetaData", produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    Map<GlobalSelection, DataSet> getDataSetsAndMetaData() {
        return queryBuilder.getDataSetsAndMetaData();
    }

    /**
     * This method use the CSP constraint to get usable other constraints (geo and move)
     *
     * @param csp
     * @return A simple map of dimension types
     */
    @RequestMapping(method = RequestMethod.GET, value = "/getConstraints", produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    Map<DimensionType, Set<Integer>> getConstraints(@RequestParam(required = true) Integer csp) {
        return queryBuilder.getConstraints(csp);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/test/{param}", produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    Movement fetch(@PathVariable
                   String param, @RequestParam(value = "bidule")
                   String fields) {
        System.out.println(param);
        System.out.println(fields);
        return new Movement();
    }


    @RequestMapping(method = RequestMethod.GET, value = "/{cogId}/data/{zoneGranularity}", produces = "application/json; charset=utf-8")
    @ResponseBody
    public QueryResult selectParam(@RequestParam(required = false) MoveType type,
                                   @RequestParam(required = false) PopulationCategory category,
                                   @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date from,
                                   @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date end,
                                   @RequestParam PeriodGranularity granularity,
                                   @RequestParam(required = false) Long originCogId,
                                   @RequestParam(defaultValue = "ALL", required = false) PopulationGranularity populationGranularity,
                                   @PathVariable Long cogId,
                                   @PathVariable String zoneGranularity, @RequestParam(defaultValue = "false", required = false) boolean estimation)
            throws IllegalGranularityException, NotEnoughDataForQueryException {

        Zone target = zoneRepository.findOne(cogId);
        Zone origin;
        if (originCogId == null)
            origin = target.computeUpperGranularity();
        else
            origin = zoneRepository.findOne(originCogId);

        Select select = new Select(type, category, new Period(from, end), granularity, target, origin,
                zoneGranularity.equals("global") ? origin.getGranularity() : target.getGranularity(),
                populationGranularity);
        QueryResult queryResult = queryBuilder.query(select);
        if (estimation && !queryResult.getMissing().isEmpty()) {
            List<EstimationResult> estimatedPoints = this.estimatedResults(select, queryResult);
            queryResult.setEstimationResults(estimatedPoints);
            List<Movement> movements = new ArrayList<>();
            for (EstimationResult result : estimatedPoints) {
                movements.add(result.getMovement());
            }
            queryResult.setEstimatedMovements(movements);
        }
        return queryResult;
            /*
            for(Map<DimensionType, Set<QueryResult.InternalRange>> map : queryResult.getMissing()) {
                for(DimensionType dim : DimensionType.values()) {
                    System.out.println(dim);
                    if(!map.get(dim).isEmpty() && dim!=DimensionType.CSP){
                    for(QueryResult.InternalRange internalRange : map.get(dim)) {
                        System.out.println(internalRange);
                        System.out.println(internalRange.from);
                        System.out.println(internalRange.to);}
                    }
                }
            }
            */
    }

    public List<Movement> estimatedMovements(Select select, QueryResult queryResult) throws IllegalGranularityException, NotEnoughDataForQueryException {
        List<Movement> estimatedMovements = new ArrayList<>();
        List<EstimationResult> estimationResultList = this.estimatedResults(select, queryResult);
        if (!estimationResultList.isEmpty()) {
            for (EstimationResult r : estimationResultList) {
                if (Double.isNaN(r.getMovement().getCount()) || r.getError() > 1000) {
                    continue;
                }
                estimatedMovements.add(r.getMovement());
            }
            }
        return estimatedMovements;
    }

    public List<EstimationResult> estimatedResults(Select select, QueryResult queryResult) throws NotEnoughDataForQueryException, IllegalGranularityException {
        return estimationStrategy.selectSuitableEstimator(select).strategy(select, queryResult);
    }


    /**
     * this method calculates the size of the dataset
     * @param id of the dataset
     * @return the size
     */
    @RequestMapping(method = RequestMethod.GET, value="/datasets/{id}/count", produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    Long count(@PathVariable Integer id) {
        return movementRepository.getDataSetSizeById(id);
    }

    /**
     * this method calculates the cities which people come from for a given dataset
     * @param origin_id the origin city
     * @param target_id the target city
     * @return a JSON list
     */
    @RequestMapping(method = RequestMethod.GET, value = "/getorigins/{datasetid}/{origin_id}/{target_id}", produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    List<Object[]> getto(@PathVariable Integer datasetid, @PathVariable Long origin_id, @PathVariable Long target_id ) {
        return movementRepository.getRelationOriginTarget(origin_id, target_id, datasetid);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getorigins/{datasetid}/{origin_id}/{target_id}/total", produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    double totalgetto(@PathVariable Integer datasetid, @PathVariable Long origin_id, @PathVariable Long target_id ) {
        return (double) movementRepository.findTotalNbPeople(origin_id,target_id,datasetid);
    }

    /**
     * this method shows where the people are going giving an origin city
     * @param target_id
     * @param origin_id
     * @return a JSON list
     */
    @RequestMapping(method = RequestMethod.GET, value = "/gettargets/{datasetid}/{target_id}/{origin_id}", produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    List<Object[]> getfrom(@PathVariable Integer datasetid, @PathVariable Long target_id, @PathVariable Long origin_id ) {

        return movementRepository.getRelationTargetOrigin(datasetid ,origin_id, target_id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/gettargets/{datasetid}/{origin_id}/{target_id}/total", produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    double totalgetfrom(@PathVariable Integer datasetid, @PathVariable Long origin_id, @PathVariable Long target_id ) {
        return (double) movementRepository.findTotalNbPeopleTarget(origin_id, target_id, datasetid);
    }

    /* Requete simples de stat sur la moyenne et la dispersion des mouvements */

    @RequestMapping(method = RequestMethod.GET, value = "/stats/{datasetId}/origin/{cogId}/avg", produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    String getAvgOrigin(@PathVariable Integer datasetId, @PathVariable Long cogId) {
        return "nb " + movementRepository.getAvgOrigin(datasetId, cogId);
    }

/*
    @RequestMapping(method = RequestMethod.GET, value = "/estimation/{moveType}/{cog}/{from}/{to}", produces = "application/json; charset=utf-8")
    public
    @ResponseBody
    List<EstimationResult> estimate(@RequestParam(required = false) MoveType type,
                    @RequestParam(required = false) PopulationCategory category,
                    @PathVariable Integer moveType,
                    @PathVariable Long cog,
                    @PathVariable Integer from,
                    @PathVariable Integer to)
            throws IllegalGranularityException, NotEnoughDataForQueryException {

        Zone target = zoneRepository.findOne(cog);
        List<EstimationResult> listResult = new ArrayList<>();
        switch(moveType) {
            case 1:
                listResult = estimationService.getEstimatedValue(new Select(MoveType.TOURISM, null, new Period(d(from), d(to)), PeriodGranularity.YEAR, target));
            case 2:
                listResult = estimationService.getEstimatedValue(new Select(MoveType.WORK, null, new Period(d(from), d(to)), PeriodGranularity.YEAR, target));
            case 3:
                listResult = estimationService.getEstimatedValue(new Select(MoveType.HOUSING, null, new Period(d(from), d(to)), PeriodGranularity.YEAR, target));
            case 4:
                listResult = estimationService.getEstimatedValue(new Select(MoveType.SCHOLAR, null, new Period(d(from), d(to)), PeriodGranularity.YEAR, target));
        }

        return listResult;
    }
*/

    @ExceptionHandler(value = {NotEnoughDataForQueryException.class})
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public
    @ResponseBody
    NotEnoughDataForQueryException.Message handleException(NotEnoughDataForQueryException e) {
        return e.getStructuredMessage();
    }

}
