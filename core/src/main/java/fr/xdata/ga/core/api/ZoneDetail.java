package fr.xdata.ga.core.api;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import fr.xdata.ga.core.domain.Zone;
import fr.xdata.ga.core.domain.ZoneGis;
import fr.xdata.ga.core.repositories.ZoneGisRepository;
import fr.xdata.ga.core.repositories.ZoneRepository;

import java.util.List;

/**
 *
 */
public class ZoneDetail {
    private Zone zone;
    private Zone parent;
    private List<Zone> siblings;
    private List<Zone> children;
    public static class ZoneDetailGis extends Zone {
        public String path;
        public double centerX;
        public double centerY;

        public ZoneDetailGis(Zone z, ZoneGis gis) {
            super(z.getReg(), z.getDep(), z.getAr(), z.getCom(), z.getLib());
            this.path = gis.getPath();
            this.centerX = gis.getCenterX();
            this.centerY = -gis.getCenterY();
        }
    }

    public ZoneDetail(Long cogId, final ZoneRepository repository, final ZoneGisRepository gisRepository) {

        Function<Zone, Zone> transformToGis = new Function<Zone, Zone>() {
            @Override
            public Zone apply(Zone z) {
                if(z == null)
                    return z;
                ZoneGis gis = gisRepository.findOne(z.getId());
                if(gis == null)
                    return z;
                return new ZoneDetailGis(z, gis);
            }
        };

        zone = transformToGis.apply(repository.findOne(cogId));
        assert this.zone != null;
        parent = this.zone.computeUpperGranularity();
        parent = repository.findByCog(parent.getReg(), parent.getDep(), parent .getAr(), parent.getCom());
        parent = transformToGis.apply(parent);

        switch (zone.getGranularity()) {
            case NATION:
                siblings = Lists.newArrayList();
                //children = repository.findAllDepByReg(zone.getReg());
                children = repository.findAllReg();
                break;
            case REGION:
                siblings = repository.findAllReg();
                children = repository.findAllDepByReg(zone.getReg());
                break;
            case DEPARTEMENT:
                siblings = repository.findAllDepByReg(parent.getReg());
                children = repository.findAllArByDep(zone.getReg(), zone.getDep());
                break;
            case ARRONDISSEMENT:
                siblings = repository.findAllArByDep(parent.getReg(), parent.getDep());
                children = repository.findAllComByAr(zone.getReg(), zone.getDep(), zone.getAr());
                break;
            case COMMUNE:
                siblings = repository.findAllComByAr(parent.getReg(), parent.getDep(), parent.getAr());
                children = Lists.newArrayList();
                break;
        }

        siblings = Lists.transform(siblings, transformToGis);
        children = Lists.transform(children, transformToGis);
    }

    public Zone getZone() {
        return zone;
    }

    public Zone getParent() {
        return parent;
    }

    public List<Zone> getSiblings() {
        return siblings;
    }

    public List<Zone> getChildren() {
        return children;
    }
}
