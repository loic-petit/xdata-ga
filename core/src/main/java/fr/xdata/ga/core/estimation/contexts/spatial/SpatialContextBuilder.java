package fr.xdata.ga.core.estimation.contexts.spatial;

import com.google.common.collect.Sets;
import fr.xdata.ga.core.domain.Zone;
import fr.xdata.ga.core.estimation.contexts.Context;
import fr.xdata.ga.core.estimation.util.Missing;
import fr.xdata.ga.core.exceptions.IllegalGranularityException;
import fr.xdata.ga.core.exceptions.NotEnoughDataForQueryException;
import fr.xdata.ga.core.selection.QueryBuilder;
import fr.xdata.ga.core.selection.QueryResult;
import fr.xdata.ga.core.selection.Select;
import fr.xdata.ga.core.selection.dimensions.DimensionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Service
public class SpatialContextBuilder implements Context {

    @Autowired
    QueryBuilder queryBuilder;


    public Set<Missing> contextBuilder(Select givenSelect, QueryResult queryResult) throws NotEnoughDataForQueryException, IllegalGranularityException {
        return getMovementsFromMissingPoint(queryResult, givenSelect, getMissingZones(getMissingPoints(queryResult, givenSelect)));

    }

    public Set<Missing> getMovementsFromMissingPoint(QueryResult queryResult, Select givenSelect, Set<Zone> zoneMissing) {
        Set<Missing> missingSet = new HashSet<>();
        Set<QueryResult.InternalRange> internalRangeSet = getMissingPoints(queryResult, givenSelect);
        getMissingZones(internalRangeSet);
        for (Zone z : zoneMissing) {
            //TODO a impléementer : génération de select qui conduisent à des list<Movement>
        }
        return missingSet;
    }

    private Set<Zone> getMissingZones(Set<QueryResult.InternalRange> internalRangeSet) {
        Set<Zone> zoneMissingSet = new HashSet<>();
        for (QueryResult.InternalRange internalRange : internalRangeSet) {
            //TODO
            zoneMissingSet.add((Zone) internalRange.from);

        }
        return zoneMissingSet;
    }

    public Set<QueryResult.InternalRange> getMissingPoints(QueryResult queryResult, Select givenSelect) {
        Set<QueryResult.InternalRange> internalRangeMissing = Sets.newHashSet();
        for (Map<DimensionType, Set<QueryResult.InternalRange>> map : queryResult.getMissing()) {
            internalRangeMissing.addAll(map.get(DimensionType.GEO));
        }
        return internalRangeMissing;
    }

    public Set<Zone> buildSimilarZones(Select givenSelect) {
        //TODO à implémenter
        Set<Zone> similarZoneSet = new HashSet<>();
        return similarZoneSet;
    }


}
