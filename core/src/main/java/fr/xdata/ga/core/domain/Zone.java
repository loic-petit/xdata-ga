package fr.xdata.ga.core.domain;

import org.hibernate.annotations.Index;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 *
 */
@Entity
@Table(name="Zone")
public class Zone implements Serializable{

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name="reg", nullable=true)
    @Index(name="reg_index")
    private Integer reg;                      // region
    @Column(name="dep", nullable=true)
    @Index(name="dep_index")
    private String dep;                       // department
    @Column(name="ar", nullable=true)
    @Index(name="ar_index")
    private Integer ar;                       // arrondissement           error: enums are not supported in -source 1.3
    @Column(name="com", nullable=true)
    @Index(name="com_index")
    private Integer com;                      // commune

    @Column(name="lib", nullable=false)
    public String lib;

    public Zone(){
    }

    public Zone(Integer reg, String dep, Integer ar, Integer com) {
        this.reg = reg;
        this.dep = dep;
        this.ar = ar;
        this.com = com;
        this.id = generateId();
    }

    private Long generateId() {
        long res = 0;
        if(reg != null)
            res |= reg;
        res = res << 12;
        if(dep != null)
            res |= Integer.parseInt(dep, 16);
        res = res << 4;
        if(ar != null)
            res |= ar;
        res = res << 10;
        if(com != null)
            res |= com;
        return res;
    }

    public Zone(Integer reg, String dep, Integer ar, Integer com, String lib){
        this(reg, dep, ar, com);
        this.lib = lib;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getReg() {
        return reg;
    }

    public void setReg(Integer reg) {
        this.reg = reg;
    }

    public String getDep() {
        return dep;
    }

    public void setDep(String dep) {
        this.dep = dep;
    }

    public Integer getAr() {
        return ar;
    }

    public void setAr(Integer ar) {
        this.ar = ar;
    }

    public Integer getCom() {
        return com;
    }

    public void setCom(Integer com) {
        this.com = com;
    }

    public String getLib() {
        return lib;
    }

    public void setLib(String lib) {
        this.lib = lib;
    }

    /**
     * TODO : Should be a find in the zone repository
     * @return the zone of higher granularity
     */
    public Zone computeUpperGranularity() {
        Integer reg = this.reg;
        String dep = this.dep;
        Integer ar = this.ar;
        Integer com = this.com;
        if(com != null)
            com = null;
        else if(ar != null)
            ar = null;
        else if(dep != null)
            dep = null;
        else
            reg = null;
        return new Zone(reg, dep, ar, com);
    }

    public ZoneGranularity getGranularity() {
        if(com != null)
            return ZoneGranularity.COMMUNE;
        if(ar != null)
            return ZoneGranularity.ARRONDISSEMENT;
        if(dep != null)
            return ZoneGranularity.DEPARTEMENT;
        if(reg != null)
            return ZoneGranularity.REGION;
        return ZoneGranularity.NATION;
    }

    public String toString (){
        return "(" + reg + ", " + dep + ", " + ar + ", " + com + ", " + lib + ")";
    }
}
