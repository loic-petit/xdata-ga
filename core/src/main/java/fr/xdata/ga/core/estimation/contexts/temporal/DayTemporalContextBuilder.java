package fr.xdata.ga.core.estimation.contexts.temporal;

import fr.xdata.ga.core.api.ZoneDetail;
import fr.xdata.ga.core.domain.Movement;
import fr.xdata.ga.core.domain.Period;
import fr.xdata.ga.core.domain.Zone;
import fr.xdata.ga.core.estimation.util.Missing;
import fr.xdata.ga.core.exceptions.IllegalGranularityException;
import fr.xdata.ga.core.exceptions.NotEnoughDataForQueryException;
import fr.xdata.ga.core.repositories.ZoneGisRepository;
import fr.xdata.ga.core.repositories.ZoneRepository;
import fr.xdata.ga.core.selection.QueryBuilder;
import fr.xdata.ga.core.selection.QueryResult;
import fr.xdata.ga.core.selection.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DayTemporalContextBuilder extends TemporalContextBuilder {


    @Autowired
    ZoneRepository zoneRepository;
    @Autowired
    ZoneGisRepository zoneGisRepository;
    @Autowired
    QueryBuilder queryBuilder;

    private static final Logger log = LoggerFactory.getLogger(YearTemporalContextBuilder.class);

    @Override
    public Set<Missing> contextBuilder(Select givenSelect, QueryResult queryResult) throws NotEnoughDataForQueryException, IllegalGranularityException {
        return getMovementsFromMissingDate(givenSelect, getMissingDates(queryResult));
    }

    private Set<Missing> getMovementsFromMissingDate(Select givenSelect, Set<Date> datesMissing) throws IllegalGranularityException, NotEnoughDataForQueryException {
        Set<Missing> missingSet = new HashSet<>();
        for (Date date : datesMissing) {
            ZoneDetail zoneDetail = new ZoneDetail(givenSelect.getOrigin().getId(), zoneRepository, zoneGisRepository);
            List<Zone> zonesChildren = zoneDetail.getChildren();
            for (Zone z : zonesChildren) {
                if (z.getLib().equals("Guadeloupe") || z.getLib().equals("Mayotte") || z.getLib().equals("Martinique") || z.getLib().equals("Guyane") || z.getLib().equals("La Réunion")) {
                    //dom tom non pris en compte
                    continue;
                }
                List<Movement> mov = this.getMovements(givenSelect, date, z);

                if (!mov.isEmpty()) {
                    Movement estimatedMovement = buildEstimatedMovement(givenSelect, date, z, mov);
                    missingSet.add(new Missing(estimatedMovement, mov));
                }
            }
        }
        return missingSet;
    }

    public Movement buildEstimatedMovement(Select givenSelect, Date date, Zone z, List<Movement> similarMovements) {
        Movement estimatedMovement = new Movement();
        estimatedMovement.setFrom(date);
        estimatedMovement.setCategory(similarMovements.get(0).getCategory());
        estimatedMovement.setOrigin(z);
        estimatedMovement.setTarget(givenSelect.getTarget());
        estimatedMovement.setMoveType(givenSelect.getType());
        buildDateTo(date, estimatedMovement);
        return estimatedMovement;
    }

    private void buildDateTo(Date date, Movement estimatedMovement) {
        Calendar yearCalendar = Calendar.getInstance();
        yearCalendar.setTime(date);
        yearCalendar.add(Calendar.DAY_OF_MONTH, 1);
        estimatedMovement.setTo(yearCalendar.getTime());
    }

    /**
     * search the movements with the new built select
     *
     * @param z           the zone origin
     * @param date        the missing date
     * @param givenSelect the given select
     * @return list of movements that will be used for the estimation
     * @throws NotEnoughDataForQueryException
     */
    public List<Movement> getMovements(Select givenSelect, Date date, Zone z) throws NotEnoughDataForQueryException, IllegalGranularityException {

        Select context = new Select(
                givenSelect.getType(),
                givenSelect.getPopulation(),
                null,
                givenSelect.getPeriodGranularity(),
                givenSelect.getTarget(),
                z,
                givenSelect.getOriginGranularity(),
                givenSelect.getPopulationGranularity()
        );
        context.setPeriod(this.buildNewPeriodForSelect(date));
        return queryBuilder.query(context).getPoints();
    }

    /**
     * set of all the missing dates that are included (those that are excluded are not added to the set of the select that "fails"
     *
     * @param queryResult the given queryResult
     * @return a set of all missing dates
     * @throws NotEnoughDataForQueryException
     */
    public Set<Date> getMissingDates(QueryResult queryResult) throws NotEnoughDataForQueryException {
        Set<QueryResult.InternalRange> internalRangeMissing = this.getTemporalInternalRangeMissing(queryResult);
        Set<Date> datesMissing = new HashSet<>();
        //iteration on all the internal range of the temporal dimension to get all the missing points
        for (QueryResult.InternalRange internalRange : internalRangeMissing) {
            datesMissing.addAll(this.getAllMissingDates((Date) internalRange.from, (Date) internalRange.to, internalRange.fromExcluded, internalRange.toExcluded));
        }
        return datesMissing;
    }

    public Set<Date> getAllMissingDates(Date dateFrom, Date dateTo, boolean fromExcluded, boolean toExcluded) {
        Set<Date> dateSet = new HashSet<>();
        Calendar a = Calendar.getInstance();
        Calendar b = Calendar.getInstance();
        a.setTime(dateFrom);
        b.setTime(dateTo);
        dateSet.add(dateFrom);
        int diff = 0;
        if (toExcluded) {
            diff = 1;
        }
        getInternalMissingDates(dateSet, a, b, diff, fromExcluded);
        return dateSet;
    }

    private void getInternalMissingDates(Set<Date> dateSet, Calendar a, Calendar b, int diff, boolean fromExcluded) {
        if (fromExcluded)
            a.add(Calendar.DAY_OF_MONTH, 1);
        int diff_year = b.get(Calendar.DAY_OF_MONTH) - a.get(Calendar.DAY_OF_MONTH);
        while (diff_year > diff) {
            a.add(Calendar.DAY_OF_MONTH, 1);
            diff_year = b.get(Calendar.DAY_OF_MONTH) - a.get(Calendar.DAY_OF_MONTH);
            dateSet.add(a.getTime());
        }
    }

    @Override
    public Period buildNewPeriodForSelect(Date dateFrom) {
        Calendar a = Calendar.getInstance();
        Calendar b = Calendar.getInstance();
        a.setTime(dateFrom);
        b.setTime(dateFrom);
        a.add(Calendar.DAY_OF_MONTH, periodBefore);
        b.add(Calendar.DAY_OF_MONTH, periodAfter);
        log.info("period built : " + new Period(a.getTime(), b.getTime()));
        return new Period(a.getTime(), b.getTime());
    }


}
