package fr.xdata.ga.core.util;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 */
public class SecurityUtils {
    public static String getMD5(String filePath){

        String md5 = null;

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(new File(filePath));
            try {
                md5 = DigestUtils.md5Hex(fis);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return md5;
    }
}
