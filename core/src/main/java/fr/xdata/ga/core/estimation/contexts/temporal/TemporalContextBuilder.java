package fr.xdata.ga.core.estimation.contexts.temporal;

import com.google.common.collect.Sets;
import fr.xdata.ga.core.domain.Period;
import fr.xdata.ga.core.estimation.contexts.Context;
import fr.xdata.ga.core.estimation.util.Missing;
import fr.xdata.ga.core.exceptions.IllegalGranularityException;
import fr.xdata.ga.core.exceptions.NotEnoughDataForQueryException;
import fr.xdata.ga.core.selection.QueryResult;
import fr.xdata.ga.core.selection.Select;
import fr.xdata.ga.core.selection.dimensions.DimensionType;

import java.util.Date;
import java.util.Map;
import java.util.Set;


/**
 * this class finds out from a query result all the temporal missing points
 * for each missing points, it builds a context and returns a list of movements that will be used to estimate the missing points
 */
public abstract class TemporalContextBuilder implements Context {

    public static final int periodBefore = -4;
    public static final int periodAfter = 4;

    public abstract Period buildNewPeriodForSelect(Date dateFrom);

    public abstract Set<Missing> contextBuilder(Select select, QueryResult queryResult) throws NotEnoughDataForQueryException, IllegalGranularityException;

    /**
     * searches the internal range that stores the temporal data missing
     *
     * @param queryResult the queryResult object that contains the missing ranges
     * @return the temporal internalRange that is missing
     * @throws NotEnoughDataForQueryException
     */
    public Set<QueryResult.InternalRange> getTemporalInternalRangeMissing(QueryResult queryResult) throws NotEnoughDataForQueryException {
        Set<QueryResult.InternalRange> internalRangeMissing = Sets.newHashSet();
        for (Map<DimensionType, Set<QueryResult.InternalRange>> map : queryResult.getMissing()) {
            internalRangeMissing.addAll(map.get(DimensionType.TEMPORAL));
        }
        return internalRangeMissing;
    }

}
