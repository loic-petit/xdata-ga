package fr.xdata.ga.core.importers;

import com.google.common.collect.Maps;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.core.io.ClassPathResource;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

/**
 * Class to access Data-Publica internal web service "commune"
 *
 * Use the 'request' method to get the INSEE code from postal code and search text
 *
 * This service require to have a 'datapub.properties' file in the resources directory
 * containing the suitable 'datapub.commune.url' property
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class DataPublicaCommune{
    private String codeINSEE;
    private String nom;

    private static RestTemplate restTemplate = new RestTemplate();
    protected static String serviceUrl = null;

    private static Map<String, DataPublicaCommune> resultCache = Maps.newHashMap();


    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getCodeINSEE() {
        return codeINSEE;
    }
    public void setCodeINSEE(String codeINSEE) {
        this.codeINSEE = codeINSEE;
    }

    // service
    /*
     * Call the Commune service to search for a postal code and a commune name
     */
    public static DataPublicaCommune request(String postalCode, String text){
        boolean initialized = init();

        text = text.toLowerCase();
        DataPublicaCommune com;
        if(!initialized){
            com = new DataPublicaCommune();
        }else{
            if (text.startsWith("st ") || text.startsWith("st-"))
                text = text.replaceFirst("st", "saint");
            String cacheCheck = postalCode+text;
            com = resultCache.get(cacheCheck);
            if(com != null) {
                return com;
            }
            String url = serviceUrl+"?txt="+text+"&zipcode="+postalCode;
            try{
                com = restTemplate.getForObject(url, DataPublicaCommune.class);
            }catch (HttpClientErrorException e){
                if(e.getStatusCode()== HttpStatus.NOT_FOUND)
                    return null;
                else
                    throw e;
            }
            resultCache.put(cacheCheck, com);
        }
        return com;
    }

    protected static boolean init(){
        if(serviceUrl!=null) return true;

        ClassPathResource resource = new ClassPathResource( "datapub.properties" );
        Properties p = new Properties();
        try {
            p.load(resource.getInputStream());
        }
        catch (IOException e){
            return false;
        }

        serviceUrl = p.getProperty("datapub.commune.url");
        return true;
    }
}
