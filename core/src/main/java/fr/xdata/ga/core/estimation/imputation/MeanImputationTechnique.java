package fr.xdata.ga.core.estimation.imputation;

import fr.xdata.ga.core.estimation.EstimationResult;
import fr.xdata.ga.core.estimation.util.Missing;
import fr.xdata.ga.core.estimation.util.SimilarPoints;
import org.apache.commons.math.stat.descriptive.DescriptiveStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ensemble de méthodes alternatives dans le cas où la régression n'est pas linéaire :
 * - imputation de la moyenne
 */

public class MeanImputationTechnique implements Imputation {

    public static final double ERROR_MAJORATION = 1.20;
    private static final Logger log = LoggerFactory.getLogger(MeanImputationTechnique.class);

    /**
     * imputation de la moyenne en fonction de la valeur de l'écart type
     *
     * @return a result object
     */
    public EstimationResult calculateEstimation(Missing missing) {

        EstimationResult estimatedValue = new EstimationResult();
        DescriptiveStatistics dataDesc = new DescriptiveStatistics();
        SimilarPoints dataSelection = new SimilarPoints(missing.getSimilarMovements());
        dataSelection.setData(missing.getSimilarMovements());
        for (double d : dataSelection.getData()) {
            dataDesc.addValue(d);
        }
        double std = dataDesc.getStandardDeviation();
        double meanVariationCoefficient = std / dataDesc.getMean();
        System.out.println(" mean variation  " + meanVariationCoefficient);
        double error = std;
        if (meanVariationCoefficient <= 0.15) {
            log.info("fiable");
            estimatedValue.setEstimatedValue(dataDesc.getMean());
            //l'erreur est égale à la variation moyenne
            estimatedValue.setError(error);
        } else if (meanVariationCoefficient <= 0.35) {
            log.info("assez fiable");
            //on estime variation est importante, la moyenne comporte une erreur
            //on majore l'erreur de 20%
            estimatedValue.setEstimatedValue(dataDesc.getMean());
            estimatedValue.setError(error * ERROR_MAJORATION);
        } else {
            log.info("pas fiable");
            //l'imputation de la moyenne serait éronnée, on ne peut se fier aux données des communes adjacentes
            // données passées insuffisantes, données zones similaires défaillantes
            estimatedValue.setEstimatedValue(Double.POSITIVE_INFINITY);
        }
        log.info("estimated value with mean imputation : " + estimatedValue.getEstimatedValue());
        return estimatedValue;
    }

}
