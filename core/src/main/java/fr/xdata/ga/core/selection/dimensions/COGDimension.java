package fr.xdata.ga.core.selection.dimensions;

import com.google.common.collect.Range;
import fr.xdata.ga.core.domain.Zone;
import fr.xdata.ga.core.util.PredicateUtils;

import java.util.List;

/**
 * Represent a comparable object of
 */
public class COGDimension implements Dimension {
    private Integer reg;
    private String dep;
    private Integer ar;
    private Integer com;

    public COGDimension(Integer reg, String dep, Integer ar, Integer com) {
        this.reg = reg;
        this.dep = dep;
        this.ar = ar;
        this.com = com;
    }

    public static Range<Dimension> createFrom(Zone z) {
        return new COGDimension(z.getReg(), z.getDep(), z.getAr(), z.getCom()).asRange();
    }

    public Range<Dimension> asRange() {
        if (com != null)
            return Range.closedOpen((Dimension) this, new COGDimension(reg, dep, ar, com + 1));
        if (ar != null)
            return Range.closedOpen((Dimension) this, new COGDimension(reg, dep, ar + 1, null));
        if (dep != null)
            return Range.closedOpen((Dimension) this, new COGDimension(reg, depInc(dep), null,
                            null));
        if (reg != null)
            return Range.closedOpen((Dimension) this, new COGDimension(reg + 1, null, null, null));
        return Range.all();
    }

    protected static String depInc(String dep) {
        char c = dep.charAt(dep.length() - 1);
        if (c >= 'A' && c < 'Z') {
            // corse !
            return dep.substring(0, dep.length() - 1) + ((char)(c + 1));
        }
        String r = String.valueOf(Integer.parseInt(dep) + 1);
        if (r.length() == 1)
            r = "0" + r;
        return r;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String compareWith(String from, String with, boolean lessThan,
                    boolean orEquals) {
        long id = new Zone(reg, dep, ar, com).getId();
        return PredicateUtils.buildPredicate(with, lessThan, orEquals, id);
    }

    @Override
    public Object asOriginal() {
        return new Zone(reg, dep, ar, com);
    }

    private void addPredicate(Comparable value, String comField, boolean equals, boolean lessThan, boolean orEquals,
                              List<String> list) {
        if (value == null)
            return;
        if(equals)
            list.add(PredicateUtils.buildEqualsPredicate(comField, value));
        else
            list.add(PredicateUtils.buildPredicate(comField, lessThan, orEquals, value));
    }

    @Override
    public int compareTo(Dimension r) {
        COGDimension o = (COGDimension) r;

        int c = 0;
        if (reg == null && o.reg != null)
            return -1;
        else if (reg != null && o.reg == null)
            return 1;
        else if (reg != null)
            c = reg.compareTo(o.reg);
        if (c != 0)
            return c;
        if (dep == null && o.dep != null)
            return -1;
        else if (dep != null && o.dep == null)
            return 1;
        else if (dep != null)
            c = dep.compareTo(o.dep);
        if (c != 0)
            return c;
        if (ar == null && o.ar != null)
            return -1;
        else if (ar != null && o.ar == null)
            return 1;
        else if (ar != null)
            c = ar.compareTo(o.ar);
        if (c != 0)
            return c;
        if (com == null && o.com != null)
            return -1;
        else if (com != null && o.com == null)
            return 1;
        else if (com != null)
            return com.compareTo(o.com);
        return 0;
    }

    @Override
    public String toString() {
        return "cog(" + reg + "," + dep + "," + ar + "," + com + '}';
    }

}

