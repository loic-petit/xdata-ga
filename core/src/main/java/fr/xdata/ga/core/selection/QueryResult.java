package fr.xdata.ga.core.selection;

import com.google.common.collect.BoundType;
import com.google.common.collect.Maps;
import com.google.common.collect.Range;
import com.google.common.collect.Sets;
import fr.xdata.ga.core.domain.DataSet;
import fr.xdata.ga.core.domain.Movement;
import fr.xdata.ga.core.estimation.EstimationResult;
import fr.xdata.ga.core.selection.dimensions.Dimension;
import fr.xdata.ga.core.selection.dimensions.DimensionType;
import fr.xdata.ga.core.selection.internal.ComplexSelection;
import fr.xdata.ga.core.selection.internal.GlobalSelection;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 */
public class QueryResult {
    public static class InternalRange {
        public Object from;
        public boolean fromExcluded = false;
        public Object to;
        public boolean toExcluded = false;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            InternalRange that = (InternalRange) o;

            if (fromExcluded != that.fromExcluded) return false;
            if (toExcluded != that.toExcluded) return false;
            if (from != null ? !from.equals(that.from) : that.from != null) return false;
            if (to != null ? !to.equals(that.to) : that.to != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = from != null ? from.hashCode() : 0;
            result = 31 * result + (fromExcluded ? 1 : 0);
            result = 31 * result + (to != null ? to.hashCode() : 0);
            result = 31 * result + (toExcluded ? 1 : 0);
            return result;
        }
    }

    private List<Movement> points;
    private Set<DataSet> usedDataSets;
    private Set<Map<DimensionType, Set<InternalRange>>> missing = Sets.newHashSet();

    private List<EstimationResult> estimatedPoints;


    private List<Movement> estimatedMovements;
    private Set<Map<DimensionType, Set<InternalRange>>> estimatedMovementMissing = Sets.newHashSet();

    public QueryResult(List<Movement> points, Set<DataSet> usedDataSets, ComplexSelection missing) {
        this.points = points;
        this.usedDataSets = usedDataSets;

        buildDTOFromComplexSelection(missing, this.missing);
    }

    public void buildDTOFromComplexSelection(ComplexSelection complexSelection, Set<Map<DimensionType, Set<InternalRange>>> dto) {
        for (GlobalSelection dsms : complexSelection) {
            EnumMap<DimensionType, Set<InternalRange>> dim = Maps.newEnumMap(DimensionType.class);
            for (DimensionType type : DimensionType.values()) {
                Set<Range<Dimension>> rangeSet = dsms.S(type).getRangeSet();
                Set<InternalRange> result = Sets.newHashSet();
                for (Range<Dimension> range : rangeSet) {
                    InternalRange r = new InternalRange();
                    if (range.hasLowerBound()) {
                        r.from = range.lowerEndpoint().asOriginal();
                        r.fromExcluded = range.lowerBoundType() == BoundType.OPEN;
                    }
                    if (range.hasUpperBound()) {
                        r.to = range.upperEndpoint().asOriginal();
                        r.toExcluded = range.upperBoundType() == BoundType.OPEN;
                    }
                    if (r.to != null || r.from != null)
                        result.add(r);
                }

                if (!result.isEmpty())
                    dim.put(type, result);
            }
            dto.add(dim);
        }
    }

    public List<Movement> getPoints() {
        return points;
    }

    public Set<DataSet> getUsedDataSets() {
        return usedDataSets;
    }

    public Set<Map<DimensionType, Set<InternalRange>>> getMissing() {
        return missing;
    }

    public void setEstimationResults(List<EstimationResult> estimatedMovementsResults) {
        this.estimatedPoints = estimatedMovementsResults;
    }

    public void setEstimatedMovementMissing(Set<Map<DimensionType, Set<InternalRange>>> estimatedMovementMissing) {
        this.estimatedMovementMissing = estimatedMovementMissing;
    }

    public List<EstimationResult> getEstimatedPoints() {
        return estimatedPoints;
    }

    public Set<Map<DimensionType, Set<InternalRange>>> getEstimatedMovementMissing() {
        return estimatedMovementMissing;
    }

    public void setEstimatedMovements(List<Movement> estimatedMovements) {
        this.estimatedMovements = estimatedMovements;
    }

    public List<Movement> getEstimatedMovements() {
        return estimatedMovements;
    }


}
