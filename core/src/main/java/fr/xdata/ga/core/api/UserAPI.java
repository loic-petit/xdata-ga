/*
 * Copyright (C) by Data Publica, All Rights Reserved.
 */
package fr.xdata.ga.core.api;

import com.google.common.base.Strings;
import fr.xdata.ga.core.domain.User;
import fr.xdata.ga.core.repositories.UserRepository;
import fr.xdata.ga.core.security.CustomUserDetailsService;
import fr.xdata.ga.core.security.SecureUser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserAPI {
    private static final Log LOG = LogFactory.getLog(UserAPI.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CustomUserDetailsService userDetailsService;

    @PreAuthorize("hasRole('Admin')")
    @RequestMapping(method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody
    List<User> list() {
        return userRepository.findAll();
    }

    @PreAuthorize("hasRole('Admin')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody
    User get(@PathVariable Long id) {
        return userRepository.findOne(id);
    }

    @PreAuthorize("hasRole('Admin')")
    @RequestMapping(method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public @ResponseBody
    User create(@RequestBody User user) {
        // encode password
        user.encryptAndSetPassword(user.getPassword());
        LOG.info("user " + user.getEmail() + " saved");
        return userRepository.save(user);
    }

    @PreAuthorize("hasRole('Admin')")
    @RequestMapping(method = RequestMethod.POST, value = "/{id}", produces = "application/json;charset=UTF-8")
    public @ResponseBody
    User update(@PathVariable("id") long id, @RequestBody User user) throws IOException {
        if (Strings.isNullOrEmpty(user.getPassword()))
            throw new IllegalArgumentException("Password cannot be empty");
        user.encryptAndSetPassword(user.getPassword());
        if (user.getId() != id)
            throw new IOException("trying to update user id=" + id + " with invalid user " + user.getId());
        LOG.info("update customer id=" + id + " name=" + user.getEmail());
        return userRepository.save(user);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "current/userInfo", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    SecureUser getUserInfo() {
        return userDetailsService.getAuthenticatedUser();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({ IOException.class, JsonParseException.class, JsonMappingException.class })
    public @ResponseBody
    String handleInvalidException(Exception e) {
        LOG.error(e);
        return "Exception " + e.getMessage();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ IllegalArgumentException.class })
    public @ResponseBody
    String handleInvalidRequestException(Exception e) {
        LOG.error(e);
        return e.getMessage();
    }
}
