package fr.xdata.ga.core.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Date;

/**
 *
 */
@Embeddable
public class Period {

    @Column(name="fromDate")
    private Date from;
    @Column(name="toDate")
    private Date to;

    public Period(Date from, Date to) {
        this.from = from;
        this.to = to;
    }

    public Period() {
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public String toString (){
        return "(" + from + ", " + to + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Period period = (Period) o;

        if (from != null ? !from.equals(period.from) : period.from != null) return false;
        if (to != null ? !to.equals(period.to) : period.to != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = from != null ? from.hashCode() : 0;
        result = 31 * result + (to != null ? to.hashCode() : 0);
        return result;
    }
}
