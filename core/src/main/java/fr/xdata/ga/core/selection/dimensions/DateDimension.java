package fr.xdata.ga.core.selection.dimensions;

import com.google.common.collect.Range;
import fr.xdata.ga.core.domain.Period;
import fr.xdata.ga.core.util.PredicateUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 */
public class DateDimension implements Dimension {
    private Date value;

    public DateDimension(Date value) {
        this.value = value;
    }

    public static Range<Dimension> createFrom(Period period) {
        return Range.closedOpen((Dimension) new DateDimension(period.getFrom()),
                        new DateDimension(period.getTo()));
    }

    @Override
    public String compareWith(String query, String with, boolean lessThan,
                    boolean orEquals) {
        return PredicateUtils.buildPredicate(with, lessThan, orEquals, value);
    }

    @Override
    public Object asOriginal() {
        return value;
    }

    @Override
    public int compareTo(Dimension o) {
        return value.compareTo(((DateDimension) o).value);
    }

    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM");

    @Override
    public String toString() {
        return FORMAT.format(value);
    }


}
