package fr.xdata.ga.core.repositories;

import fr.xdata.ga.core.domain.Zone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 */
public interface ZoneRepository extends JpaRepository<Zone,Long>{

    public Zone findByDepAndCom(String dep, int com);

    @Query("select z from Zone z where z.reg != null AND z.dep IS NULL AND z.ar IS NULL AND z.com IS NULL")
    public List<Zone> findAllReg();

    @Query("select z from Zone z where z.reg = ?1 AND z.dep != null AND z.ar IS NULL AND z.com IS NULL")
    public List<Zone> findAllDepByReg(Integer reg);

    @Query("select z from Zone z where z.reg = ?1 AND z.dep = ?2 AND z.ar != null AND z.com IS NULL")
    public List<Zone> findAllArByDep(Integer reg, String dep);

    @Query("select z from Zone z where z.reg = ?1 AND z.dep = ?2 AND z.ar = ?3 AND z.com != null")
    public List<Zone> findAllComByAr(Integer reg, String dep, Integer ar);

    @Query("select z from Zone z where " +
            "(?1 = null and z.reg is null or z.reg = ?1) AND " +
            "(?2 = null and z.dep is null or z.dep = ?2) AND " +
            "(?3 = null and z.ar is null or z.ar = ?3) AND " +
            "(?4 = null and z.com is null or z.com = ?4)")
    public Zone findByCog(Integer reg, String dep, Integer ar, Integer com);

    @Modifying
    @Transactional
    @Query("delete from Zone z where z.reg IS NOT NULL AND z.dep IS NULL AND z.ar IS NULL AND z.com IS NULL")
    public void deleteRegions();

    @Modifying
    @Transactional
    @Query("delete from Zone z where z.reg IS NOT NULL AND z.dep IS NOT NULL AND z.ar IS NULL AND z.com IS NULL")
    public void deleteDepartements();

    @Modifying
    @Transactional
    @Query("delete from Zone z where z.reg IS NOT NULL AND z.dep IS NOT NULL AND z.ar IS NOT NULL AND z.com IS NULL")
    public void deleteArrondissements();

    @Modifying
    @Transactional
    @Query("delete from Zone z where z.reg IS NOT NULL AND z.dep IS NOT NULL AND z.ar IS NOT NULL AND z.com IS NOT NULL")
    public void deleteCommunes();

    public Zone findByLib(String lib);
}
