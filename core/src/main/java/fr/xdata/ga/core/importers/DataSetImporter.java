package fr.xdata.ga.core.importers;

import au.com.bytecode.opencsv.CSVReader;
import com.google.common.collect.Lists;
import fr.xdata.ga.core.domain.*;
import fr.xdata.ga.core.repositories.DataProviderRepository;
import fr.xdata.ga.core.repositories.DataSetRepository;
import fr.xdata.ga.core.repositories.MovementRepository;
import fr.xdata.ga.core.repositories.ZoneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 */
public class DataSetImporter {
    private static final Logger log = LoggerFactory.getLogger(DataSetImporter.class);

    private DataSetUriConfig conf;

    private DataSetRepository dataSetRepository;

    private MovementRepository movementRepository;

    private DataProviderRepository dataProviderRepository;

    private DataProvider provider;

    private ZoneRepository zoneRepository;

    private static final SimpleDateFormat YEAR_FORMAT = new SimpleDateFormat("yyyy");

    private ZoneCache zoneCache;

    // constructors
    // ------------
    public DataSetImporter() {}

    public DataSetImporter(DataSetRepository dataSetRepository, DataProviderRepository dataProviderRepository,
                           MovementRepository movementRepository, ZoneRepository zoneRepository, DataSetUriConfig conf) {
        this.conf = conf;
        this.dataSetRepository = dataSetRepository;
        this.movementRepository = movementRepository;
        this.zoneRepository = zoneRepository;
        this.dataProviderRepository = dataProviderRepository;
    }

    // repo accessors
    // --------------
    public MovementRepository getMovementRepository() {
        return movementRepository;
    }
    public void setMovementRepository(MovementRepository movementRepository) {
        this.movementRepository = movementRepository;
    }
    public DataSetRepository getDataSetRepository() {
        return dataSetRepository;
    }
    public void setDataSetRepository(DataSetRepository dataSetRepository) {
        this.dataSetRepository = dataSetRepository;
    }
    public ZoneRepository getZoneRepository() {
        return zoneRepository;
    }
    public void setZoneRepository(ZoneRepository zoneRepository) {
        this.zoneRepository = zoneRepository;
    }

    // dataset import
    // --------------
    /**
     * Import all dataset files found in {@link #conf} datasetImporterUri
     *
     * @param update if empty, update all dataset (w.r.t reset value
     *               otherwise, update only dataset which file name (w/out extension) is listed in {@code update}
     * @param reset By default (i.e. false), it reloads only datasets that are missing from {@link #dataSetRepository}.
     *              If true, it reloads the dataset and replace the respective repository content
     */
    public boolean importFiles(String[] update, boolean reset) {
        String log_msg = "";
        for(String up : update) log_msg += up+",";
        log.info("--- import datasets (udpate=" + log_msg +" reset="+reset+ ") ---");

        // reset caches
        // ------------
        log.info("Load zones cache");
        zoneCache = new ZoneCache(zoneRepository);

        // list dataset descriptor files
        // -----------------------------
        File dir = new File(conf.getDataSetImporterUri());
        if (!dir.isDirectory()) {
            log.error("DataSetImporterUri is not a directory");
            return false;
        }
        File[] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".dataset");
            }
        });

        if (files == null || files.length == 0) {
            log.warn("No file found in DataSetImporterUri: " + dir.getAbsolutePath());
            return false;
        }

        // reduce to update list
        List<File> dataset_files;
        if(update.length>0){
            dataset_files = Lists.newArrayList();
            Set<String> update_set= new HashSet<>(Arrays.asList(update));
            for(File file : files){
                String name = file.getName().substring(0,file.getName().length()-8);
                if(update_set.contains(name))
                    dataset_files.add(file);
            }
            reset=true;
        }else{
            dataset_files = Lists.newArrayList(files);
        }
        log.debug("Importing "+ files.length+" files:");
        for(File file : dataset_files)
            log.debug("  - "+file.getAbsolutePath());


        // import each dataset
        // -------------------
        for (File file : dataset_files) {
            log.debug("");
            log.info(" >> processing: " + file.getName());
            if (file.isDirectory())
                continue;

            // get dataset descriptor
            DataSetDescriptor dsDescriptor = new DataSetDescriptor(file);
            if (!dsDescriptor.isValid()) {
                String error_log = "ERROR: Could not load dataset descriptor " + file.getAbsolutePath();
                for (String error : dsDescriptor.getErrors()) {
                    error_log += "\n" + error;
                }
                log.error(error_log);
                continue;
            }

            // read dataset descriptor
            dsDescriptor.parseProperties(zoneRepository, log);
            if (!dsDescriptor.isValid()) {
                String error_log = "ERROR: The content of the dataset descriptor file " + file.getName() + " has errors:";
                for (String error : dsDescriptor.getErrors()) {
                    error_log += "\n\t\t" + error;
                }
                log.error(error_log);
                continue;
            }

            DataSet currentSet = dataSetRepository.findDataSetByMd5sum(dsDescriptor.getMd5());
            if (reset) {
                // reset respective repositories
                log.debug("remove DB content for " + dsDescriptor.getName());
                movementRepository.deleteMovementByDataSet(currentSet);
                dataSetRepository.deleteDataSetById(currentSet.getId());
                currentSet = null;
            }

            if (currentSet == null) {
                importFile(dsDescriptor);
            } else {
                log.debug("File " + file.getName() + " already imported!");
            }
        }
        log.info("--- Import done ---");

        return true;
    }

    /**
     * @param descriptor {@link DataSetDescriptor} of the dataset to import
     */
    private void importFile(DataSetDescriptor descriptor) {
        log.debug("Dataset create: label=" + descriptor.getLabel());
        DataSet dataSet = createDataSet(descriptor);
        dataSet = dataSetRepository.saveAndFlush(dataSet);

        try {
            log.debug("Import dataset content: " + descriptor.getDatasetFile().getAbsolutePath());
            if (descriptor.isImmigrationData())  // Immigration "volume" dataset type
                importImmigrationDataset(descriptor, dataSet);
            else
                importCsvDataset(descriptor, dataSet);

        } catch (IOException e) {
            log.warn("Could not import file " + descriptor.getName() + " due to IOException... ignoring", e);
        }
    }

    protected void importImmigrationDataset(DataSetDescriptor descriptor, DataSet dataSet) throws IOException {
        List<Movement> moves = Lists.newArrayList();
        CSVReader reader = new CSVReader(new FileReader(descriptor.getDatasetFile().getAbsolutePath()), ';');

        String[] tuple;
        String[] years = reader.readNext();                 // get years

        String[] volString = reader.readNext();
        Double[] volumes = new Double[volString.length];    // get volumes
        for (int i = 1; i < volumes.length; i++) {
            volumes[i] = Double.parseDouble(volString[i].replace(',','.'));
        }

        Zone target = descriptor.getTarget();

        // then iterate through the percentages
        while ((tuple = reader.readNext()) != null) {

            Zone origin = descriptor.readOrigin(tuple, zoneCache);
            if (origin == null) log.debug("unrecognized origin: " + descriptor.readOriginValue(tuple));

            // one move by year
            for (int i = 1; i < volumes.length; i++) {
                Movement m = new Movement();
                m.setDataSet(dataSet);

                try {
                    Date year = YEAR_FORMAT.parse(years[i]);
                    m.setFrom(year);
                    m.setTo(year);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                m.setMoveType(dataSet.getMoveType());
                m.setOrigin(origin);
                m.setTarget(target);
                m.setCount(volumes[i] * new Double(tuple[i].replace('%', ' ').replace(',', '.').trim()) * 10000);

                if (m.getTarget() != null && m.getOrigin() != null)
                    moves.add(m);
                else
                    log.error("Invalid movement: " + m.toString());
            }
        }

        log.info("Saving to DB repository");
        movementRepository.save(moves);
    }

    protected void importCsvDataset(DataSetDescriptor descriptor, DataSet dataSet) throws IOException {
        List<Movement> moves = Lists.newArrayList();
        CSVReader reader = new CSVReader(new FileReader(descriptor.getDatasetFile().getAbsolutePath()), ';');

        String[] tuple;
        int i = 0;
        int loaded = 0;
        int failed = 0;
        reader.readNext();     // pass headers
        while ((tuple = reader.readNext()) != null) {
            Movement m = parseMovement(descriptor, dataSet, tuple);
            if (m != null && m.getTarget() != null && m.getOrigin() != null){
                moves.add(m);
                loaded += 1;
            }else{
                failed += 1;
            }

            i++;
            if (i % 10000 == 0) {
                log.debug("  Parsed " + i + " movements - " + failed + " failed");
                movementRepository.save(moves);
                moves.clear();
            }
        }
        log.debug(Integer.toString(loaded) + " loaded on " + i + " movements (" + failed + " failures)");
        Set<String> missing = zoneCache.getMissing();
        if(missing.size()>0){
            List<String> sortedMissing = new ArrayList<>(missing);
            Collections.sort(sortedMissing);
            log.debug("Missing zones:\n" + sortedMissing.toString());
        }

        log.info("Saving to DB repository");
        movementRepository.save(moves);
    }

    private Movement parseMovement(DataSetDescriptor descriptor, DataSet dataSet, String[] tuple) {
        Movement m = new Movement();

        m.setDataSet(dataSet);
        m.setMoveType(dataSet.getMoveType());

        m.setOrigin(descriptor.readOrigin(tuple, zoneCache));
        m.setTarget(descriptor.readTarget(tuple, zoneCache));
        m.setCount(descriptor.readCount(tuple));

        if (descriptor.getPopulationGranularity() == PopulationGranularity.SELECTED)
            m.setCategory(descriptor.readPopulation(tuple));

        Date from = descriptor.readFrom(tuple);
        Date to = descriptor.readTo(tuple, from);   // if from==null, then to=null
        if(from==null) from = dataSet.getTimeFilter().getFrom();
        if(to==null) to = dataSet.getTimeFilter().getTo();
        m.setFrom(from);
        m.setTo(to);

        return m;
    }

    private DataSet createDataSet(DataSetDescriptor descriptor) {
        DataSet dataSet = new DataSet();

        setProvider(descriptor.getProvider(), dataSet);

        dataSet.setLabel(descriptor.getLabel());
        dataSet.setMd5sum(descriptor.getMd5());

        dataSet.setOriginGeoFilter(descriptor.getOrigin());
        dataSet.setTargetGeoFilter(descriptor.getTarget());
        dataSet.setGeoGranularity(descriptor.getZoneGranularity());

        dataSet.setTimeFilter(new Period(descriptor.getFrom(), descriptor.getTo()));
        dataSet.setTimeGranularity(descriptor.getPeriodGranularity());

        dataSet.setPopulationGranularity(descriptor.getPopulationGranularity());

        dataSet.setMoveType(descriptor.getMoveType());

        return dataSet;
    }

    private void setProvider(String name){
        provider = dataProviderRepository.findByProviderName(name);
        if (provider == null) {
            provider = new DataProvider();
            provider.setProviderName(name);
            provider = dataProviderRepository.saveAndFlush(provider);
        }
    }
    private void setProvider(String name, DataSet dataSet){
        setProvider(name);
        dataSet.setProvider(provider);
    }
}


