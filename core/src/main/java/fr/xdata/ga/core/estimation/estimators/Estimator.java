package fr.xdata.ga.core.estimation.estimators;

import fr.xdata.ga.core.domain.Movement;
import fr.xdata.ga.core.estimation.EstimationResult;
import fr.xdata.ga.core.estimation.util.Missing;
import fr.xdata.ga.core.exceptions.IllegalGranularityException;
import fr.xdata.ga.core.exceptions.NotEnoughDataForQueryException;
import fr.xdata.ga.core.selection.QueryResult;
import fr.xdata.ga.core.selection.Select;

import java.util.List;


public interface Estimator {

    /**
     * strategy function that calls the contextBuilder and apply the estimationStrategy to the set of missing points
     *
     * @param givenSelect
     * @param queryResult
     * @return list of results
     * @throws NotEnoughDataForQueryException
     * @throws IllegalGranularityException
     */
    public List<EstimationResult> strategy(Select givenSelect, QueryResult queryResult) throws NotEnoughDataForQueryException, IllegalGranularityException;

    /**
     * evaluates how the input data is reliable to estimate the missing point
     *
     * @param movements
     * @return a boolean value ensuring whether the data point should be trust to conduct the estimation
     */
    public boolean judgeReliability(List<Movement> movements);

    /**
     * strategy of estimating data according to the list of movements as input
     * the estimation is implemented in each class, according to the kind of movement we're analyzing
     *
     * @param missingObject : the missing object with a list of similar movements
     * @param missingPoint
     * @return
     */
    public EstimationResult estimationStrategy(Missing missingObject, Object missingPoint);

}

