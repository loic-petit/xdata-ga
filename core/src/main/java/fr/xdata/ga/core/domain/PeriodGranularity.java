package fr.xdata.ga.core.domain;

/**
 *
 */
public enum PeriodGranularity {
    SECOND, MINUTE, HOUR, DAY, MONTH, YEAR
}
