/*
 * Copyright (C) by Data Publica, All Rights Reserved.
 */
package fr.xdata.ga.core.domain;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonSetter;
import org.hibernate.annotations.Index;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

import javax.persistence.*;
import java.util.Set;

/**
 * Very simple credential model for the company application. No notion of account (only users) Each user has a limited
 * access to the companies: using a tag
 */
@Entity
@Table(name = "User")
public class User {
    private static final PasswordEncoder encoder = new StandardPasswordEncoder();

    @Id
    @GeneratedValue
    private long id;

    @Index(name = "email_idx")
    private String email;
    protected String password;
    private String firstName;
    private String lastName;
    private UserRole role;
    private boolean enabled = true;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Zone> restrictedZones;

    public User() {
    }

    /**
     * @param id
     * @param email
     * @param password
     * @param firstName
     * @param lastName
     * @param role
     */
    public User(String email, String firstName, String lastName, UserRole role) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
    }

    public long getId() {
        return this.id;
    }

    public String getEmail() {
        return this.email;
    }

    @JsonIgnore
    public String getPassword() {
        return this.password;
    }

    @JsonSetter
    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public UserRole getRole() {
        return this.role;
    }

    public void encryptAndSetPassword(String password) {
        this.password = encoder.encode(password);
    }

    public Set<Zone> getRestrictedZones() {
        return restrictedZones;
    }

    public void setRestrictedZones(Set<Zone> restrictedZones) {
        this.restrictedZones = restrictedZones;
    }
}
