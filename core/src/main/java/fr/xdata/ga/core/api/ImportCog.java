package fr.xdata.ga.core.api;

import fr.xdata.ga.core.importers.CogImporter;
import fr.xdata.ga.core.importers.CogUriConfig;
import fr.xdata.ga.core.repositories.ZoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 */
@Controller
@RequestMapping
public class ImportCog {

    @Autowired
    private ZoneRepository zoneRepository;

    @Autowired
    private CogUriConfig conf;

    @RequestMapping(method = RequestMethod.GET, value = "/import/cog", produces = "text/plain; charset=utf-8")
    public @ResponseBody
    String importCog() {

        CogImporter imp = new CogImporter(zoneRepository, conf);
        Integer res = imp.importFiles();

        return res.toString();
    }
}
