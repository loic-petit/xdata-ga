/*
 * Copyright (C) by Data Publica, All Rights Reserved.
 */
package fr.xdata.ga.core.security;

import com.google.common.collect.Sets;
import fr.xdata.ga.core.domain.User;
import fr.xdata.ga.core.domain.UserRole;
import fr.xdata.ga.core.domain.Zone;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SecureUser implements UserDetails, CredentialsContainer {
    private static final long serialVersionUID = -4937189095680912251L;

    private static Map<UserRole, Collection<? extends GrantedAuthority>> Authorities = new HashMap<>();;

    protected final Log log = LogFactory.getLog(getClass());
    private User user;
    private boolean isCredentialsErased = false;

    static {
        Authorities.put(UserRole.Admin, Sets.newHashSet(new SimpleGrantedAuthority(UserRole.Admin.name()),
                        new SimpleGrantedAuthority(UserRole.Customer.name())));
        Authorities.put(UserRole.Customer, Sets.newHashSet(new SimpleGrantedAuthority(UserRole.Customer.name())));
    }

    public SecureUser(User user) {
        this.user = user;
    }

    @Override
    public void eraseCredentials() {
        isCredentialsErased = true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Authorities.get(user.getRole());
    }

    @Override
    public String getUsername() {
        return user.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public String getPassword() {
        return isCredentialsErased ? null : user.getPassword();
    }

    @Override
    public boolean isEnabled() {
        return user.isEnabled();
    }

    // ---- Extra Attributes

    public String getFirstName() {
        return user.getFirstName();
    }

    public String getLastName() {
        return user.getLastName();
    }

    public Set<Zone> getRestrictedZones() {
        return user.getRestrictedZones();
    }

    /**
     * Retrive the underlying user Important: don't serizalize it using Json otherwise the password will be
     * serialized!!!!
     * 
     * @return
     */
    @JsonIgnore
    public User getUnderlyingUser() {
        return user;
    }

}
