package fr.xdata.ga.core.estimation.estimators;


import fr.xdata.ga.core.domain.Movement;
import fr.xdata.ga.core.estimation.EstimationResult;
import fr.xdata.ga.core.estimation.contexts.Context;
import fr.xdata.ga.core.estimation.imputation.Imputation;
import fr.xdata.ga.core.estimation.util.Missing;
import fr.xdata.ga.core.exceptions.IllegalGranularityException;
import fr.xdata.ga.core.exceptions.NotEnoughDataForQueryException;
import fr.xdata.ga.core.selection.QueryResult;
import fr.xdata.ga.core.selection.Select;
import org.apache.commons.math.stat.descriptive.DescriptiveStatistics;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DensityEstimator implements Estimator {


    private Context context;
    private Set<Imputation> suitableAlternativeImputations;
    private Imputation primaryImputation;

    public DensityEstimator() {
    }

    public DensityEstimator(Context context, Imputation primaryImputation, Set<Imputation> imputationSet) {
        this.context = context;
        this.primaryImputation = primaryImputation;
        this.suitableAlternativeImputations = imputationSet;
    }

    @Override
    public List<EstimationResult> strategy(Select givenSelect, QueryResult queryResult) throws NotEnoughDataForQueryException, IllegalGranularityException {
        List<EstimationResult> estimationResultList = new ArrayList<>();


        return estimationResultList;
    }

    @Override
    public boolean judgeReliability(List<Movement> movementList) {
        DescriptiveStatistics dataDescription = new DescriptiveStatistics();
        for (Movement movement : movementList) {
            dataDescription.addValue(movement.getCount());
        }
        double meanVariation = dataDescription.getStandardDeviation() / dataDescription.getMean();
        return (meanVariation <= 0.45);
    }

    @Override
    public EstimationResult estimationStrategy(Missing missing, Object MissingObject) {
        EstimationResult estimatedValue = new EstimationResult();
        if (this.judgeReliability(missing.getSimilarMovements())) {
            estimatedValue = primaryImputation.calculateEstimation(missing);
        } else if (!suitableAlternativeImputations.isEmpty()) {
            for (Imputation alternativeImputations : suitableAlternativeImputations) {
                if (!Double.isInfinite(alternativeImputations.calculateEstimation(missing).getEstimatedValue())) {
                    estimatedValue = alternativeImputations.calculateEstimation(missing);
                    break;
                }
            }
        } else estimatedValue.setEstimatedValue(Double.POSITIVE_INFINITY);
        return estimatedValue;
    }
}
