package fr.xdata.ga.core.repositories;

import fr.xdata.ga.core.domain.DataSet;
import fr.xdata.ga.core.domain.Movement;
import fr.xdata.ga.core.repositories.Customized.MovementRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 */
public interface MovementRepository extends JpaRepository<Movement, Long>, MovementRepositoryCustom {
    /**
     * Get a list of movements that is contained this period of time
     * @param from
     * @param to
     * @return
     */
    //public List<Movement> findByFromGreaterThanAndEndLowerThan(Date from, Date to);

    @Modifying
    @Transactional
    @Query("delete from Movement m where m.dataSet = ?1")
    public void deleteMovementByDataSet(DataSet dataSet);


    @Query("select count(*) from Movement m where m.dataSet.id= ?1")
    public Long getDataSetSizeById(Integer id);
    /*
    @Query("select sum(m.count) from Movement m where m.origin= ?1 and m.dataSet.id= ?2")
    public Long getNbDepartures(Long cogId, Integer id);

    @Query("select sum(m.count) from Movement m where m.target= ?1 and m.dataSet.id= ?2")
    public Long getNbArrivals(Long cogId, Integer id);
    */
    @Query("select m.origin.id AS ID_COMMUNE_ORIGIN, sum(m.count) AS NB_PEOPLE_MOVING, z.lib AS NAME_COMMUNE_ORIGIN from Movement m, Zone z " +
            "where m.target.id=?2 and " +
            "z.id=m.origin.id and " +
            "m.dataSet.id=?3 and " +
            "m.origin.id not in (select m.origin.id from Movement m where m.target.id=?2 and m.origin.id=?1)" +
            "group by m.origin.id order by m.count desc")
    public List<Object[]> getRelationOriginTarget(Long id_origin, Long id_target, Integer dataSet_id);

    @Query("select m.target.id AS ID_COMMUNE_TARGET, sum(m.count) AS NB_PEOPLE_MOVING, z.lib AS NAME_COMMUNE_TARGET from Movement m, Zone z " +
            "where m.origin.id=?2 and " +
            "z.id=m.target.id and " +
            "m.dataSet.id=?1 and " +
            "m.target.id not in (select m.target.id from Movement m where m.target.id=?3 and m.origin.id=?2) " +
            "group by m.target.id order by m.count desc")
    public List<Object[]> getRelationTargetOrigin(Integer dataSet_id, Long id_origin, Long id_target);
/*
    @NamedQuery(name="nativeSQL", query="select sum(NB_PEOPLE_MOVING) AS TOTAL from " +
            "(select m.origin AS ID_COMMUNE_ORIGIN, sum(m.count) AS NB_PEOPLE_MOVING, z.lib AS NAME_COMMUNE_ORIGIN " +
            "from Movement m, Zone z " +
            "where m.target=?2 and " +
            "z.id=m.origin and " +
            "m.dataSet=?3 and " +
            "m.origin not in (select m.origin from Movement m where m.target=?2 and m.origin=?1)" +
            " group by ID_COMMUNE_ORIGIN) F", )
    public double getNbPeopleMovingTo(Long id_origin, Long id_target, Integer dataSet_id);
*/
    @Query(value="select sum(NB_PEOPLE_MOVING) AS TOTAL from "+
            "(select m.target.id AS ID_COMMUNE_TARGET, sum(m.count) AS NB_PEOPLE_MOVING, z.lib AS NAME_COMMUNE_TARGET " +
            "from Movement m, Zone z " +
            "where m.origin.id=?2 and " +
            "z.id=m.target.id and " +
            "m.dataSet.id=?1 and " +
            "m.target.id not in (select m.target.id from Movement m where m.target.id=?3 and m.origin.id=?2) " +
            "group by ID_COMMUNE_TARGET) C", nativeQuery = true)
    public double getNbPeopleMovingFrom(Integer dataSet_id, Long id_origin, Long id_target);

 /*   @Query("select sum(NB_PEOPLE_MOVING) AS TOTAL from " +
            "(select m.origin AS ID_COMMUNE_ORIGIN, sum(m.count) AS NB_PEOPLE_MOVING, z.lib AS NAME_COMMUNE_ORIGIN " +
            "from Movement m, Zone z " +
            "where m.target=?2 and " +
            "z.id=m.origin and " +
            "m.dataSet=?3 and " +
            "m.origin not in (select m.origin from Movement m where m.target=?2 and m.origin=?1)" +
            " group by ID_COMMUNE_ORIGIN) F")
    public double getNbPeopleMovingTo(Long id_origin, Long id_target, Integer dataSet_id); */
    @Query("select avg(m.count) from Movement m where m.dataSet.id=?1 and m.origin.id=?2")
    public double getAvgOrigin(Integer datasetId, Long origin_cog);
    @Query("select avg(m.count) from Movement m where m.dataSet.id=?1 and m.target.id=?2")
    public double getAvgTarget(Integer datasetId, Long target_cog);
 /*
    @Query("select stddev(m.count) from Movement m where m.dataSet.id=?1 and m.origin.id=?2")
    public double getStddevOrigin(Integer datasetId, Long origin_cog);
    @Query("select stddev(m.count) from movement m where m.dataSet.id=?1 and m.target.id=?2")
    public double getStddevTarget(Integer datasetId, Long target_cog);
*/

}
