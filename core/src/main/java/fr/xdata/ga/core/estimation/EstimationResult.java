package fr.xdata.ga.core.estimation;

import fr.xdata.ga.core.domain.Movement;


public class EstimationResult {
    private Object missing;
    private Movement movement;
    private double estimatedValue;
    private double error;

    public EstimationResult() {
    }

    public double getEstimatedValue() {
        return estimatedValue;
    }

    public void setEstimatedValue(double estimatedValue) {
        this.estimatedValue = estimatedValue;
    }

    public double getError() {
        return error;
    }

    public void setError(double error) {
        this.error = Math.round(error);
    }

    public Movement getMovement() {
        return movement;
    }

    public void setMovement(Movement movement) {
        this.movement = movement;
    }

    public void setMissing(Object missing) {
        this.missing = missing;
    }

    public Object getMissing() {
        return missing;
    }

}
