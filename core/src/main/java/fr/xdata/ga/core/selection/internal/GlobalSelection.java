package fr.xdata.ga.core.selection.internal;

import com.google.common.collect.Maps;
import fr.xdata.ga.core.domain.DataSet;
import fr.xdata.ga.core.domain.Population;
import fr.xdata.ga.core.selection.Select;
import fr.xdata.ga.core.selection.dimensions.DimensionType;

import java.util.Collection;
import java.util.Map;

/**
 *
 */
public class GlobalSelection {
    // time capacity
    protected Map<DimensionType, DimensionRange> selections = Maps.newEnumMap(DimensionType.class);
    protected Map<DimensionType, Integer> granularities = Maps.newEnumMap(DimensionType.class);

    /**
     * Create a metadata based on a query selection
     * 
     * @param select the query
     */
    public GlobalSelection(Select select) {
        init(DimensionType.TEMPORAL, select.getPeriodGranularity().ordinal(), select.getPeriod());
        init(DimensionType.GEO_ORIGIN, select.getOriginGranularity().ordinal(), select.getOrigin());
        init(DimensionType.GEO, select.getOriginGranularity().ordinal(), select.getTarget());
        init(DimensionType.CSP, select.getPopulationGranularity().ordinal(), select.getPopulation());
        init(DimensionType.MOVETYPE, 0, select.getType());
    }

    /**
     * Create a duplicate of a GlobalSelection
     * 
     * @param original The original to create a duplicate from
     */
    public GlobalSelection(GlobalSelection original) {
        selections.putAll(original.selections);
        granularities.putAll(original.granularities);
    }

    /**
     * Create a metadata based on a db dataset
     * 
     * @param ds the db dataset
     */
    public GlobalSelection(DataSet ds) {
        init(DimensionType.TEMPORAL, ds.getTimeGranularity().ordinal(), ds.getTimeFilter());
        init(DimensionType.GEO, ds.getGeoGranularity().ordinal(), ds.getTargetGeoFilter());
        init(DimensionType.GEO_ORIGIN, ds.getGeoGranularity().ordinal(), ds.getOriginGeoFilter());
        Population populationFilter = ds.getPopulationFilter();
        init(DimensionType.CSP, ds.getPopulationGranularity().ordinal(), populationFilter == null ? null
                        : populationFilter.getCategory());
        init(DimensionType.MOVETYPE, 0, ds.getMoveType());
    }

    public GlobalSelection(GlobalSelection original, GlobalSelection restrict) {
        for (DimensionType type : DimensionType.values()) {
            DimensionRange selection = original.selections.get(type);
            selections.put(type, selection.duplicate().restrict(restrict.selections.get(type)));
        }
        granularities.putAll(original.granularities);
    }

    private void init(DimensionType dimension, int ordinal, Object content) {
        DimensionRange selection = DimensionRangeFactory.buildFrom(dimension, content);
        selections.put(dimension, selection);
        granularities.put(dimension, ordinal);
    }

    /**
     * 
     * @param dsmd the target to compare to
     * @return return d such that Cov(d) = Cov(this) \ U_{target \in dsmd} Cov(target)
     */
    public ComplexSelection coverageIsContainedIn(Collection<GlobalSelection> dsmd) {
        ComplexSelection current = new ComplexSelection(this);
        // Cov(this) ⊆ U_{d \in dsmd} Cov(d)
        // is strictly equivalent to Cov(this) \ Cov(d_1) \ Cov(d_2)... \ Cov(d_n) = ø
        for (GlobalSelection target : dsmd) {
            current = new ComplexSelection(current, target);
            if (current.isEmpty())
                break;
        }
        return current;
    }

    /**
     * Get the granularity of the dimension
     * 
     * @param type the dimension type
     * @return the ordinal of the granularity
     */
    public Integer G(DimensionType type) {
        return granularities.get(type);
    }

    /**
     * Get the selection of the dimension
     * 
     * @param type the dimension type
     * @return the DimensionRange
     */
    public DimensionRange S(DimensionType type) {
        return selections.get(type);
    }

    @Override
    public String toString() {
        return "{" + "S=" + selections + ", G=" + granularities + '}';
    }

    /**
     * Compute if this set is empty
     * 
     * @return true if ∃c / d.S = ø
     */
    public boolean isEmpty() {
        for (DimensionRange selection : selections.values()) {
            if (selection.isEmpty())
                return true;
        }
        return false;
    }

    public String asPredicate(Map<DimensionType, String> query) {
        String res[] = new String[selections.size()];

        int i = 0;
        String from = query.get(DimensionType.MOVETYPE);
        for (DimensionType dim : DimensionType.values()) {
            DimensionRange selection = selections.get(dim);
            String path = query.get(dim);
            String lower = path+"."+(dim.getLowerBoundField());
            String upper = path+"."+(dim.getUpperBoundField());
            String p = selection.asPredicate(path, lower, upper, dim.isUpperBoundAlwaysIncluded());
            res[i++] = p;
        }
        String finalQuery = null;
        for (String s : res) {
            if(finalQuery == null)
                finalQuery = s;
            else
                finalQuery += " AND "+s;
        }

        return "("+finalQuery+")";
    }
}
