package fr.xdata.ga.core.domain;

/**
 *
 */
public enum PopulationCategory {
    AGRICULTURE("Agriculteurs exploitants"),
    ARTISANT("Artisans, commerçants et chefs d'entreprise"),
    CADRES("Cadres et professions intellectuelles supérieures"),
    INTERMEDIAIRE("Professions Intermédiaires"),
    EMPLOYES("Employés"),
    OUVRIERS("Ouvriers"),
    RETRAITES("Retraités"),
    SANS_ACTIVITE("Autres personnes sans activité professionnelle"),
    ETUDIANT_INDEPENDANT("Etudiants indépendants");

    private String label;

    PopulationCategory(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public String toString() {
        return label;
    }
}
