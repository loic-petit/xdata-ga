package fr.xdata.ga.core.estimation.util;

import fr.xdata.ga.core.domain.Movement;

import java.util.List;


public class Missing {

    private Movement movementObjectMissing;
    private List<Movement> similarMovements;

    public Missing() {
    }


    public Missing(Movement objectMissing, List<Movement> similarMovements) {
        this.movementObjectMissing = objectMissing;
        this.similarMovements = similarMovements;
    }

    public Movement getMovementObjectMissing() {
        return movementObjectMissing;
    }

    public void setObjectMissing(Movement objectMissing) {
        this.movementObjectMissing = objectMissing;
    }

    public List<Movement> getSimilarMovements() {
        return similarMovements;
    }

    public void setSimilarMovements(List<Movement> similarMovements) {
        this.similarMovements = similarMovements;
    }
}
