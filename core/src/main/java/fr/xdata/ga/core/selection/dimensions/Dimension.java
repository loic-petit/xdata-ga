package fr.xdata.ga.core.selection.dimensions;

/**
 *
 */
public interface Dimension extends Comparable<Dimension> {
    String compareWith(String path, String with,
                    boolean lessThan, boolean orEquals);
    Object asOriginal();
    //List<DimensionRange> getPartition(DimensionRange parent, Integer granularity);
}
