package fr.xdata.ga.core.domain;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 */
public class ZoneTest {
    @Test
    public void testGenerateId() {
        Zone z[] = new Zone[] { new Zone(82, "07", null, null), new Zone(82, "07", 3, null), new Zone(82, "07", 3, 1),
                        new Zone(82, "07", 4, 1), new Zone(82, "08", null, null), new Zone(82, "2A", null, null),
                        new Zone(82, "998", null, null),
                        new Zone(82, "999", null, null),
                        new Zone(83, "998", null, null),
                        new Zone(83, "999", null, null),
                        new Zone(84, "2A", null, null),
                        new Zone(84, "2B", null, null),
                        new Zone(85, "01", null, null) };
        for (int i = 0; i < z.length - 1; i++) {
            Assert.assertTrue("Testing "+i, z[i].getId() < z[i + 1].getId());
        }

    }
}
