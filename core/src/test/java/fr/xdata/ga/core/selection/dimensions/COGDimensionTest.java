package fr.xdata.ga.core.selection.dimensions;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 */
public class COGDimensionTest {
    @Test
    public void testDepInc() throws Exception {
        Assert.assertEquals(COGDimension.depInc("05"), "06");
        Assert.assertEquals(COGDimension.depInc("24"), "25");
        Assert.assertEquals(COGDimension.depInc("244"), "245");
        Assert.assertEquals(COGDimension.depInc("2A"), "2B");
        Assert.assertEquals(COGDimension.depInc("2B"), "2C");
    }
}
