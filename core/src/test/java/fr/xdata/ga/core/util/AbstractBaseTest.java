package fr.xdata.ga.core.util;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 */
@ContextConfiguration(locations = { "classpath:/fr/xdata/ga/core/spring/spring-test-bootstrap.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
// to activate autowiring injection dependence
public abstract class AbstractBaseTest {

}
