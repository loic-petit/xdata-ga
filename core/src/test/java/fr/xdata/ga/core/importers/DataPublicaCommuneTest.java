package fr.xdata.ga.core.importers;

import org.junit.Test;
import org.junit.Assert;

//import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * test the interface to the Data-Publica Commune web service
 */
public class DataPublicaCommuneTest {
    @Test
    public void testInit(){
        boolean initialized = DataPublicaCommune.init();
        Assert.assertEquals(initialized,true);
        Assert.assertNotNull(DataPublicaCommune.serviceUrl);
    }
    @Test
    public void testJacksonWrap() throws IOException{
        String jsonString = "{\"codeINSEE\":\"13014\","+
                            "\"nom\":\"Berre-l'Étang\","+
                            "\"foundCodePostal\":13130,"+
                            "\"latitude\":43.47555555555556,"+
                            "\"longitude\":5.168055555555556,"+
                            "\"departmement\":{\"codeINSEE\":\"13\",\"nom\":\"Bouches-du-Rhône\"},"+
                            "\"region\":{\"codeINSEE\":\"93\",\"nom\":\"Provence-Alpes-Côte d'Azur\"}}";
        ObjectMapper objectMapper = new ObjectMapper();
        //objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        DataPublicaCommune com = objectMapper.readValue(jsonString, DataPublicaCommune.class);

        Assert.assertEquals(com.getCodeINSEE(),"13014");
    }
    @Test
    public void testRequest(){
        DataPublicaCommune com = DataPublicaCommune.request("13130","BERRE L ETANG");
        Assert.assertEquals(com.getCodeINSEE(),"13014");
    }

}
