package fr.xdata.ga.core.selection.internal;

import com.google.common.collect.Lists;
import fr.xdata.ga.core.domain.*;
import fr.xdata.ga.core.exceptions.IllegalGranularityException;
import fr.xdata.ga.core.selection.Select;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Set;

/**
 *
 */
public class DataSetSelectionTest {

    private static final DataSet LA_POSTE = new DataSet(0, "La Poste Mensuel", null, "A", MoveType.HOUSING, new Period(
            d(2011), d(2013)), PeriodGranularity.MONTH, null, null, ZoneGranularity.DEPARTEMENT, null,
            PopulationGranularity.ALL);
    private static final DataSet INSEE = new DataSet(1, "INSEE Annuel", null, "B", null, new Period(d(2008), d(2012)),
            PeriodGranularity.YEAR, null, null, ZoneGranularity.COMMUNE, null, PopulationGranularity.SELECTED);
    private static final DataSet INSEE_OLD = new DataSet(2, "INSEE Old", null, "C", null, new Period(d(2006), d(2008)),
            PeriodGranularity.YEAR, null, null, ZoneGranularity.COMMUNE, null, PopulationGranularity.SELECTED);
    private static final DataSet OTHER = new DataSet(3, "Autre", null, "D", MoveType.TOURISM, new Period(d(2008), d(2013)),
            PeriodGranularity.MONTH, null, new Zone(52, null, null, null), ZoneGranularity.COMMUNE, null,
            PopulationGranularity.ALL);

    private static Date d(Integer year, Integer month, Integer day) {
        return new GregorianCalendar(year, month == null ? 0 : month, day == null ? 1 : day).getTime();
    }

    private static Date d(Integer year, Integer month) {
        return d(year, month, null);
    }

    private static Date d(Integer year) {
        return d(year, null, null);
    }

    @Test
    public void testPrimarySelection() throws IllegalGranularityException {
        DataSetSelection select = new DataSetSelection();
        select.fill(Lists.newArrayList(LA_POSTE, INSEE, OTHER));

        final Zone zone = new Zone(
                82, "07", null, null);
        Select r = new Select(MoveType.HOUSING, null, new Period(d(2011), d(2012)), PeriodGranularity.YEAR, zone);
        Set<GlobalSelection> primary = select.primarySelection(new GlobalSelection(r));
        Assert.assertEquals(primary.size(), 2);

        final Zone zone1 = new Zone(82, "07", null, null);
        r = new Select(MoveType.HOUSING, PopulationCategory.CADRES, new Period(d(2011), d(2012)),
                PeriodGranularity.YEAR, zone1);
        primary = select.primarySelection(new GlobalSelection(r));
        Assert.assertEquals(primary.size(), 1);
        Assert.assertEquals(select.asDataSet(primary.iterator().next()), INSEE);

        final Zone zone2 = new Zone(82, "07",
                null, null);
        r = new Select(MoveType.WORK, null, new Period(d(2011), d(2012)), PeriodGranularity.YEAR, zone2);
        primary = select.primarySelection(new GlobalSelection(r));
        Assert.assertEquals(primary.size(), 1);
        Assert.assertEquals(select.asDataSet(primary.iterator().next()), INSEE);

        final Zone zone3 = new Zone(82, "07",
                null, null);
        r = new Select(MoveType.WORK, null, new Period(d(2011), d(2012)), PeriodGranularity.YEAR, zone3);
        primary = select.primarySelection(new GlobalSelection(r));
        Assert.assertEquals(primary.size(), 1);
        Assert.assertEquals(select.asDataSet(primary.iterator().next()), INSEE);

        final Zone zone4 = new Zone(82, "07",
                null, null);
        r = new Select(MoveType.HOUSING, null, new Period(d(2010), d(2011)), PeriodGranularity.YEAR, zone4);
        primary = select.primarySelection(new GlobalSelection(r));
        Assert.assertEquals(primary.size(), 1);
        Assert.assertEquals(select.asDataSet(primary.iterator().next()), INSEE);

        final Zone zone5 = new Zone(82,
                "07", null, null);
        r = new Select(MoveType.HOUSING, null, new Period(d(2011), d(2012)), PeriodGranularity.MONTH, zone5);
        primary = select.primarySelection(new GlobalSelection(r));
        Assert.assertEquals(primary.size(), 1);
        Assert.assertEquals(select.asDataSet(primary.iterator().next()), LA_POSTE);

        final Zone zone6 = new Zone(82,
                "07", 1, null);
        r = new Select(MoveType.HOUSING, null, new Period(d(2011), d(2012)), PeriodGranularity.MONTH, zone6);
        primary = select.primarySelection(new GlobalSelection(r));
        Assert.assertEquals(primary.size(), 0);

        final Zone zone7 = new Zone(52,
                "44", null, null);
        r = new Select(MoveType.TOURISM, null, new Period(d(2011), d(2012)), PeriodGranularity.MONTH, zone7);
        primary = select.primarySelection(new GlobalSelection(r));
        Assert.assertEquals(primary.size(), 1);
        Assert.assertEquals(select.asDataSet(primary.iterator().next()), OTHER);

        // not a full cover
        final Zone zone8 = new Zone(52, "44",
                null, null);
        r = new Select(MoveType.WORK, null, new Period(d(2005), d(2010)), PeriodGranularity.YEAR, zone8);
        primary = select.primarySelection(new GlobalSelection(r));
        Assert.assertEquals(primary.size(), 1);
        Assert.assertEquals(select.asDataSet(primary.iterator().next()), INSEE);
    }

    @Test
    public void testSelection() throws IllegalGranularityException {
        DataSetSelection select = new DataSetSelection();
        select.fill(Lists.newArrayList(LA_POSTE, INSEE, OTHER, INSEE_OLD));

        final Zone zone = new Zone(52,
                "44", null, null);
        Select r = new Select(MoveType.WORK, null, new Period(d(2005), d(2010)), PeriodGranularity.YEAR, zone);
        Strategy s = select.selectSource(r);
        Assert.assertFalse(s.getMissing().isEmpty());

        final Zone zone1 = new Zone(52,
                "44", null, null);
        r = new Select(MoveType.TOURISM, null, new Period(d(2011), d(2012)), PeriodGranularity.MONTH, zone1);
        s = select.selectSource(r);
        Assert.assertEquals(s.size(), 1);
        Map.Entry<GlobalSelection, ComplexSelection> content = s.entrySet().iterator().next();
        Assert.assertEquals(select.asDataSet(content.getKey()), OTHER);

        final Zone zone2 = new Zone(82, "07",
                null, null);
        r = new Select(MoveType.HOUSING, null, new Period(d(2006), d(2013)), PeriodGranularity.YEAR, zone2);
        s = select.selectSource(r);
        Assert.assertEquals(s.size(), 3);
        /*
         * content = s.entrySet().iterator().next(); // INSEE is considered the best because it has a higher granularity
         * Assert.assertEquals(select.asDataSet(content.getKey()), INSEE);
         */
    }

}