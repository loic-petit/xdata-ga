var jsp = require("uglify-js"),
    css = require("uglifycss"),
    fs = require("fs"),
    path = require("path");

var finalScript = "";
var DIR = "./src/main/webapp/";

var toLoad = require("."+DIR+"js/load.json");

function appendMinify(prefix, queue) {
    for(var i = 0 ; i < queue.length ; i++) {
        if(typeof queue[i] == 'object') {
            for(var dir in queue[i]) {
                if(dir != "_external")
                     appendMinify(prefix + dir + "/", queue[i][dir]);
            }
            continue;
        }
        var fileContent = fs.readFileSync(DIR+prefix+queue[i]+".js").toString();
        var idx = fileContent.indexOf("/*!");
        // add copyright
        if (idx >= 0) {
            var endIdx = fileContent.indexOf("*/", idx)+2;
            finalScript += fileContent.substring(idx,endIdx)+"\n";
        }
        finalScript += jsp.minify(fileContent, {fromString: true}).code+"\n";
    }
}

appendMinify("", toLoad);

fs.writeFileSync(process.argv[2], finalScript);

var listCss = fs.readdirSync("./src/main/webapp/css");
var cssToCompile = [];
for(var i = 0 ; i < listCss.length ; i++) {
    if(!listCss[i].match(/\.css$/)) continue;
    cssToCompile.push("./src/main/webapp/css/"+listCss[i]);
}
var cssContent = css.processFiles(cssToCompile);
fs.writeFileSync(process.argv[3], cssContent);
