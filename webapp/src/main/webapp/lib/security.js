
angular.module('angular-auth', ['http-auth-interceptor-buffer'])

  .factory('authService', ['$rootScope','$http','httpBuffer', function($rootScope,$http, httpBuffer) {
    return {
      loginConfirmed: function() {
        $rootScope.$broadcast('event:auth-loginConfirmed');
        httpBuffer.retryAll();
      },
      retrieveUserInfo:function(){
    	  $http.get("api/users/current/userInfo").success(function(data){
    		  $rootScope.user=data;
    	  });
      },
      isUserInRole:function(roles){
    	  if (angular.isUndefined($rootScope.user)|| $rootScope.user==null|| angular.isUndefined($rootScope.user.authorities) || roles==null) return false;
 		 var roleArray=roles.split(',');
    	  // check that the user has the authorities for this roles
    	  for (var e=0; e<roleArray.length;e++){
    		  var r=roleArray[e];
    		  for (var i=0; i<$rootScope.user.authorities.length; i++){
    			  if ($rootScope.user.authorities[i].authority==r) 
    				  return true;
    		  }
    	  }
    	  return false;
      }
    };
  }])
  
  /**
   * This directive implements the loginForm
   */
  .directive('authLoginForm', function() {
	  return {
		     restrict: 'E',
		     scope:{name:"@"},
		     controller: function ($scope, $http, authService) {
		    	 	$scope.error=false;
		    	    $scope.submit = function() {
		    	      $http({
		    	    		  method:'POST',
		    	    		  url:'login-check',
		    	              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		    	              data: $.param({j_username:$scope.username, j_password:$scope.password, _spring_security_remember_me:$scope.remember}),
		    	              ignoreAuthModule:true
		    	    	}).success(function() { 
		    	    		$scope.error=false;
		    	    		authService.loginConfirmed();
		    		    }).error(function(){
		    		    	  $scope.error=true;
		    		    });
		    	    }
		    	    authService.retrieveUserInfo();
		    	  },
		     template: "<div id='login-holder'><div id='loginbox'><div id='login-inner'><h2>{{name}}</h2>"+
		            "<form ng-submit='submit()' class='form-horizontal'>" +
		            "<div class='control-group' ng-class='{error:error}' ng-show='error'><label class='controls help-inline'>Enter valid credentials</label></div>"+
		     		"<div class='control-group' ng-class='{error:error}'><label class='control-label' for='inputEmail'>Email</label><div class='controls'><input type='text' ng-model='username' id='inputEmail' placeholder='Email'></div></div>"+
		            "<div class='control-group' ng-class='{error:error}'><label class='control-label' for='inputPassword'>Password</label><div class='controls'><input type='password' id='inputPassword' ng-model='password' placeholder='Password'></div></div>"+
		            "<div class='control-group'><div class='controls'><label class='checkbox'><input ng-model='remember' type='checkbox'> Remember me</label><button type='submit' class='btn btn-primary'>Sign in</button></div></div></form>"+
		            "</div><div class='clear'></div></div></div>"
		 };
		})
 /**
   * This directive checks that the role is ok
   */
  .directive('logout', function() {
	  return {
		     restrict: 'A',
		     controller: function ($scope,$rootScope, $http,authService) {
	  			$scope.logout = function() {
	  				$rootScope.user=undefined;
		    	    $http.get('logout').success(function() { // we expect a 302
		    	    	authService.retrieveUserInfo();
		    	    });
		    	}
		     }
	  }
  })
 /**
   * This directive logs the user out
   */
  .directive('roleCheck', function(authService) {
	  return {
		     restrict: 'A',
		     link: function(scope, elem, attrs) {
		    	 scope.$watch('user', function() {
		    		 if (authService.isUserInRole(attrs.roleCheck))
		    			 elem.show();
		    		 else 
		    			 elem.hide();
		    	 });
		     }
	  }
  })

  /**
   * $http interceptor.
   * On 401 response (without 'ignoreAuthModule' option) stores the request 
   * and broadcasts 'event:angular-auth-loginRequired'.
   */
  .config(['$httpProvider', function($httpProvider) {
    
    var interceptor = ['$rootScope', '$q', 'httpBuffer', function($rootScope, $q, httpBuffer) {
      function success(response) {
        return response;
      }
 
      function error(response) {
        if (response.status === 401 && !response.config.ignoreAuthModule) {
          var deferred = $q.defer();
          httpBuffer.append(response.config, deferred);
          $rootScope.$broadcast('event:auth-loginRequired');
          return deferred.promise;
        }
        // otherwise, default behaviour
        return $q.reject(response);
      }
 
      return function(promise) {
        return promise.then(success, error);
      };
 
    }];
    $httpProvider.responseInterceptors.push(interceptor);

  }])
  
  
  /**
   * This directive will find itself inside HTML as a class,
   * and will remove that class, so CSS will remove loading image and show app content.
   * It is also responsible for showing/hiding login form.
   */
  .directive('authLogin', function($http) {
    return {
      restrict: 'C',
      link: function(scope, elem, attrs) {
        //once Angular is started, remove class:
        elem.removeClass('waiting');
        
        var login = elem.find('#login-holder');
        var main = elem.find('#content');
        
        login.hide();
        
        scope.$on('event:auth-loginRequired', function() {
          login.slideDown('slow', function() {
            main.hide();
          });
        });
        scope.$on('event:auth-loginConfirmed', function() {
          main.show();
          login.slideUp();
        });
      }
    }
  });
  
  /**
   * Private module, an utility, required internally by 'http-auth-interceptor'.
   */
  angular.module('http-auth-interceptor-buffer', [])

  .factory('httpBuffer', ['$injector', function($injector) {
    /** Holds all the requests, so they can be re-requested in future. */
    var buffer = [];
    
    /** Service initialized later because of circular dependency problem. */
    var $http; 
    
    function retryHttpRequest(config, deferred) {
      $http = $http || $injector.get('$http');
      $http(config).then(function(response) {
        deferred.resolve(response);
      });
    }
    
    return {
      /**
       * Appends HTTP request configuration object with deferred response attached to buffer.
       */
      append: function(config, deferred) {
        buffer.push({
          config: config, 
          deferred: deferred
        });      
      },
              
      /**
       * Retries all the buffered requests clears the buffer.
       */
      retryAll: function() {
        for (var i = 0; i < buffer.length; ++i) {
          retryHttpRequest(buffer[i].config, buffer[i].deferred);
        }
        buffer = [];
      }
    };
  }]);
