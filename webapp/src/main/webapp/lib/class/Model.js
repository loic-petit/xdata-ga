/**
 * Model framework
 * Allows the creation of custom model classes that can be initialized with json
 * For instance:
 *      Something = Model.extend({
 *           doStuff: function() {
 *               return this.id;
 *           }
 *      });
 *      var smthg = new Something({id:1});
 *      smthg.doStuff() // returns 1
 *
 * This is based on the famous Class concept by John Resig (inspired by PrototypeJS)
 *      doc here : http://ejohn.org/blog/simple-javascript-inheritance/
 *      therefore overriding the init function will override the constructor (and this._super(json) calls the Model)
 * Moreover the model supports instanciation based on annotations on the prototype:
 *      Other = Model.extend(...);
 *      Something = Model.extend({
 *           _other: Other
 *           _arrayOfOther = [Other]
 *      });
 *      var smthg = new Something({
 *          other: {id:1}
 *          arrayOfOther: [{id:5}, {id:6}]
 *      });
 *      smthg.other instanceof Other // true!
 *      smthg.other.id // 1
 *      smthg.arrayOfOther.length // 2
 *      smthg.arrayOfOther[1].id // 6
 */
Model = Class.extend({
    init: function(json) {
        for(var name in json) {
            if(typeof json[name] === 'function') continue;
            var thisName = '_'+name;
            if(typeof this[thisName] === 'undefined') {
                this[name] = json[name];
            } else if(typeof this[thisName][0] === 'function' && this[thisName].length == 1) {
                var K = this[thisName][0];
                var arr = json[name];
                var result = [];
                for(var i = 0 ; i < arr.length ; i++) {
                    result[i] = new K(arr[i]);
                }
                this[name] = result;
            } else if(typeof this[thisName] === 'function') {
                var K = this[thisName];
                this[name] = new K(json[name]);
            }
        }
    },
    indexArray: function(property, indexProperty, into) {
        if(!into) { into = this[property] };
        if(!this[property] || !into) return;
        var l = this[property].length;
        for(var i = 0 ; i < l ; i++) {
            into[this[property][i][indexProperty]] = this[property][i];
        }
    }
});