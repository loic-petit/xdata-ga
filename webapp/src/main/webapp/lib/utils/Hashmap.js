Hashmap = {
    union: function(target, source, value) {
        if(value) {
            for(var k in source)
                target[k] = value;
        } else {
            for(var k in source)
                target[k] = source[k];
        }
        return target;
    },
    restrict: function(target, source) {
        for(var k in target)
            if (!source[k])
                delete target[k];
        return target;
    },
    remove: function(target, source) {
        for(var k in target)
            if (source[k])
                delete target[k];
        return target;
    },
    keys: function (bitmap, into) {
        var result;
        if(typeof into != "object") result = [];
        else result = into;

        for (var k in bitmap) {
            if(k[0] == '$' && k[1] == '$')
                continue;
            result.push(k);
        }
        return result;
    },
    each: function(bitmap, callback) {
        for (var k in bitmap) {
            if(k[0] == '$' && k[1] == '$')
                continue;
            callback(k, bitmap[k]);
        }
    },
    count: function (bitmap) {
        var result = 0;

        for (var k in bitmap) {
            if(k[0] == '$' && k[1] == '$')
                continue;
            ++result;
        }
        return result;
    },
    fromArray: function(array, into) {
        var result;
        if(typeof into != "object") result = {};
        else result = into;

        for(var i = 0, l = array.length ; i < l ; i++) {
            result[array[i]] = true;
        }
        return result;
    },
    isEmpty: function(bitmap) {
        for (var k in bitmap) {
            if(k[0] == '$' && k[1] == '$')
                continue;
            return false;
        }
        return true;
    },
    clone: function(bitmap, value) {
        return Bitmap.union({}, bitmap, value);
    },
    Indexed: {
        put: function(masterBitmap, masterIndex, index, value) {
            var bitmap = masterBitmap[masterIndex];
            if(!bitmap) {
                bitmap = {};
                masterBitmap[masterIndex] = bitmap;
            }
            bitmap[index] = value;
        },
        extractFromList: function(indexedBitmap, list) {
            var first = true;
            var result;
            for(var i = 0, l = list.length ; i < l ; i++) {
                var bitmap = indexedBitmap[list[i]];
                if (first) {
                    result = Bitmap.clone(bitmap);
                    first = false;
                } else {
                    Bitmap.restrict(result, bitmap);
                }
            }
            return result;
        },
        extractFromBitmap: function(indexedBitmap, bitmap) {
            var first = true;
            var result;
            for(var k in bitmap) {
                var bitmap = indexedBitmap[k];
                if (first) {
                    result = Bitmap.clone(bitmap);
                    first = false;
                } else {
                    Bitmap.restrict(result, bitmap);
                }
            }
            return result;
        }
    }
}