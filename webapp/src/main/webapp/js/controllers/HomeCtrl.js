xdata.controller("HomeCtrl", ["$scope", "$api", "$enum", "$routeParams", function($scope, $api, $enum, $routeParams) {
    var cogId = $routeParams.cogId;
    $scope.$enum = $enum;
    $api.datasets(cogId, function(data) {
        $scope.datasets = data.map(function(item) { return new DataSet(item); }).sort(function(a,b) {
            return a.timeFilter.from.unix() - b.timeFilter.from.unix();
        });;
    });

    $scope.mapSiblingsOption = $.extend(true, {}, $scope.optionsBase);
    $scope.mapSiblingsOption.title = {value : $enum.geoGranularity[$scope.zone.parent.granularity].label+" : "+$scope.zone.parent.lib+" ("+$scope.zone.parent.getCogString()+")"};
    $scope.mapChildrenOption = $.extend(true, {}, $scope.optionsBase);
    $scope.mapChildrenOption.title = {value : $enum.geoGranularity[$scope.zone.granularity].label+" : "+$scope.zone.lib+" ("+$scope.zone.getCogString()+")"};
}]);