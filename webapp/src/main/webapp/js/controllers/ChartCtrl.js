xdata.controller("ChartCtrl", ["$scope", "$timeout", function($scope, $timeout) {

    var data = {
        "cols": [{"type":'string', label:'Task', id: "task-id"}, {"type":'number', label:'Hours per Day', id: "hours-id"}],
        "rows": [{c:[{v:'Work'}, {v:11, p:{}}]},{c:[{v:'Eat'}, {v:2, p:{}}]},{c:[{v:'Commute'},{v:2, p:{}}]},{c:[{v:'Watch TV'}, {v:2, p:{}}]},{c:[{v:'Sleep'}, {v:7, f:'7.000', p:{}}]}]
    };
    $scope.chart = {
        "type": "ColumnChart",
        "displayed": true,
        "cssStyle": "height: 200px; width: 100%;",
        "data": data,
        "options": {
            "chartArea": {
            },
            "legend": {
                "position": "top"
            }
        },
        "formatters": {}
    };
    $scope.chart2 = $.extend({}, $scope.chart, true);
    $scope.chart2.type = "Table";
    console.log($scope.chart2);
}]);