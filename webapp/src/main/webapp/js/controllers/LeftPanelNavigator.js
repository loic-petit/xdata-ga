xdata.controller("LeftPanelNavigator", ["$scope", "$rootScope", function($scope, $rootScope) {
    $scope.options = {};
    $.extend(true, $scope.options, $scope.optionsBase);
    $scope.options.area.onclick = function(zone) {
        $scope.selectedZone.local = zone;
        $rootScope.$digest();
    };
}]);
