xdata.controller("GlobalChartCtrl", ["$scope", "$api", "$enum", "$timeout", function($scope, $api, $enum, $timeout) {
    $scope.availableDates = {
        year: ["2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014"],
        month: ["01","02","03","04","05","06","07","08","09","10","11","12"]
    };
    $scope.allowEstimated = false;
    var globalZone = $scope.zone;
    var initLocalZone = function() {
        if($scope.selection.level == "local") {
        $scope.zone = $.extend(true, {}, $scope.selectedZone.local);
        $scope.zone.parent = globalZone;
        $scope.zone.siblings = globalZone.children;
        $scope.zone.idx = globalZone.idx;
        }
    }
    $scope.$watch("selectedZone.local.id", function(newData, oldData) {
        if(newData == oldData)
            return;
        initLocalZone();
        $scope.update();
    }, true);
    initLocalZone();

    $scope.mapOptions = $.extend(true, {}, $scope.optionsBase);
    $scope.mapOptions.value.display = $scope.zone.siblings.length < 35;
    $scope.mapOptions.valueFormatter = function(value) {
        return Math.round(value) +" %";
    };
    $scope.mapOptions.area.label = $scope.zone.siblings.length < 15;
    $scope.mapOptions.area.txt.fill = "black";
    $scope.mapOptions.series = {useGradient:true, colorA: 'white', colorB: '#428bca', min: 0, max: 100};

    if(!$scope.selection.dimension)
        $scope.selectedDimension = "moveType";
    else if($scope.selection.dimension == "origin")
        $scope.selectedDimension = "origin.id";
    else if($scope.selection.dimension == "csp")
        $scope.selectedDimension = "category.category";

    $scope.availableChartTypes = Hashmap.keys($enum.chartType).sort(function(a,b) { return $enum.chartType[a].id - $enum.chartType[b].id; });
    if($scope.selection.dimension != "origin")
        $scope.availableChartTypes.splice($enum.chartType["Map"].id, 1);

    $scope.log = [];
    $scope.config = {
        level: "local",
        dateFrom: {year: "2006", month: "01"},
        dateEnd: {year: "2009", month: "01"},
        dateGranularity: "YEAR",
        moveType: "HOUSING",
        chartType: $scope.selectedDimension == "origin.id" ? "Map" : "Column",
        internalMoves: $scope.selectedDimension != 'origin.id',
        origin: $scope.zone.parent.id
    };

    $scope.availableDimensions = {};
    var addDimension = function(dim) {
        $scope.availableDimensions[dim.path] = dim;
    };

    var callApi = function(callbackData, configFunc) {
        var config = {
            level: "local",
            originCogId: $scope.config.origin,
            type: $scope.config.moveType,
            category: $scope.config.category,
            populationGranularity: $scope.config.category ? 'SELECTED' : 'ALL',
            from: $scope.config.dateFrom.year+"-"+$scope.config.dateFrom.month+"-01",
            end: $scope.config.dateEnd.year+"-"+$scope.config.dateEnd.month+"-01",
            granularity: $scope.config.dateGranularity,
            estimation: $scope.allowEstimated
        };
        configFunc(config);
        var level = config.level;
        delete config.level;
        $api.data($scope.zone.id, level, config, function(data) {
            $scope.qr = new QueryResult(data);
            delete $scope.qr.points;
            callbackData.addAll(data.points);
            if($scope.allowEstimated && $scope.qr.estimatedPoints) {
            for(var l = 0; l < $scope.qr.estimatedPoints.length ; l++) {
                callbackData.add($scope.qr.estimatedPoints[l].movement);
            }
            }
            $scope.plotData(callbackData);
        });
    };



    // MoveType handling declaration
    addDimension({
        path: "moveType",
        name: "Type de déplacement",
        label: function(v) { return $enum.moveType[v].label; },
        sort: function(a,b) { return $enum.moveType[a].id-$enum.moveType[b].id; },

        query: function(config) {
            config.level = "global";
            delete config.type;
        }
    });

    // Origin handling declaration
    addDimension({
        path: "origin.id",
        name: "Origine",
        label: function(v) { return $scope.zone.idx[v].lib; },
        query: function(config) {
            config.originCogId = $scope.zone.parent.id;
        }
    });

    // CSP handling declaration
    addDimension({
        path: "category.category",
        name: "Catégorie Socio-Professionnelle",
        label: function(v) { return $enum.category[v]; },
        query: function(config) {
            config.populationGranularity = 'SELECTED';
            delete config.category;
        }
    });

    var chartBase = {
        "displayed": true,
        "options": {
            "chartArea": {
            },
            "legend": {
                "position": "top"
            },
            pointSize: 5
        },
        "formatters": {}
    };



    var prepareTableData = function(slice, forChart) {
        var cols = [{"type": 'string', label: $scope.dimension.name, id: "dimension-id"},
                        {"type": 'number', label: "Somme", id: "sum-id"}];
        if(!forChart)
            cols.push({"type": 'number', label: "%", id: "percentage-id"});

        var rows = [];
        for(var i = 0, l = slice.categories.columns.length ; i < l ; i++) {
            var column = slice.categories.columns[i];
            var value = slice.aggregates.columns[column];
            var percent = value/slice.aggregates.total;
            var row = [
                {v: i, f: $scope.dimension.label(column), p:{}},
                {v: value, f: ""+Math.round(value), p:{}}
            ];
            if(!forChart)
                row.push({v: percent, f: Math.round(percent*10000)/100+" %", p:{}});
            rows.push({c: row});
        }
        var config = $.extend({}, chartBase, true);
        config.type = "Table";
        config.options.sortColumn = 1;
        config.options.sortAscending = false;
        config.data = {
            "cols": cols,
            "rows": rows
        };
        return config;
    };

    var prepareLineData = function(slice, estimatedResults) {
        var cols = [{"type": 'string', label: 'Période', id: "period-id"}];
        var series = [];
        for(var i = 0 ; i < slice.categories.columns.length ; i++) {
            var col = slice.categories.columns[i];
            cols.push({"type": "number", label: $scope.dimension.label(col), id: col+"-id"});
            series.push({});
        }
        cols.push({"type": "number", label: "Estimé", id: "estimated-id"});
        series.push({type: "steppedArea", targetAxisIndex:1});

        var estimatedDates = {};
        if(estimatedResults) {
            for(var i = 0 ; i < estimatedResults.length ; i++) {
                var lbl = moment(estimatedResults[i].movement.from).format("YYYYMMDD");
                estimatedDates[lbl] = true;
            }
        }

        var rows = [];
        for(var i = 0, l = slice.categories.rows.length ; i < l ; i++) {
            var date = slice.categories.rows[i];
            var row = [{v: moment(date).format("YYYYMMDD"), f: moment(date).format($enum.timeGranularity[$scope.config.dateGranularity].format), p:{}}];
            for(var j = 0 ; j < slice.categories.columns.length ; j++) {
                var col = slice.categories.columns[j];
                row.push({v: slice.points[date][col], f: ""+Math.round(slice.points[date][col]), p:{}});
            }
            var value = estimatedDates[moment(date).format("YYYYMMDD")] ? 1 : 0;
            row.push({v: value, f: value ? "Estimé" : "Réel", p:{}});
            
            rows.push({c: row});

        }

        var config = $.extend({}, chartBase, true);
        config.type = "ComboChart";
        config.options.vAxes = [
            {}, // Nothing specified for axis 0
            {textPosition: 'none', minValue: 0, maxValue:1} // Axis 1
        ];
        config.options.seriesType = $scope.config.chartType.toLowerCase();
        config.options.series = series;

        config.data = {
            "cols": cols,
            "rows": rows
        };
        return config;
    };


    var prepareHeatmapData = function(slice) {
        var rows = {};
        for(var i = 0, l = slice.categories.columns.length ; i < l ; i++) {
            var column = slice.categories.columns[i];
            var value = slice.aggregates.columns[column];
            var percent = value/slice.aggregates.total;
            rows[column] = percent*100;
        }
        return rows;
    };

    var initCharts = function(chartData, tableData) {
        if($scope.config.chartType != "Map") {
            if(chartData.type != "ComboChart")
                chartData.type = $scope.config.chartType + "Chart";
            $scope.chart = chartData;
        } else {
            $scope.mapData = chartData;
        }
        $scope.chartTable = tableData;
    };

    $scope.draw = function() {
        var slice = $scope.slice;
        slice.categories.columns.sort($scope.dimension.sort);
        slice.categories.rows.sort();
        var tableData = prepareTableData(slice);
        var plotData;

        if($scope.config.chartType == "Line" || $scope.config.chartType == "Area") {
            plotData = prepareLineData(slice, $scope.qr.estimatedPoints);
            //$scope.qr.estimatedPoints -- liste de Estimationresults -- $scope.qr.estimatedPoints.movement (un movement)
            //$scope.qr.estimatedPoints.movement
            //$scope.qr.estimatedPoints.error erreur associée
        } else if($scope.config.chartType == "Map") {
            plotData = prepareHeatmapData(slice);
        } else {
            plotData = prepareTableData(slice, true);
        }
        initCharts(plotData,tableData);
    };


    $scope.rangeHtml = function(r, dim) { return new Range(r, dim).format($enum); };
    $scope.plotData = function(callbackData) {
        $scope.data = callbackData;
        if(!$scope.config.internalMoves) {
            var res = new DataCube();
            for(var i = 0, l = callbackData.points.length ; i < l ; i++) {
                var pt = callbackData.points[i];
                if(pt.origin.id != pt.target.id)
                    res.add(pt);
            }
            callbackData = res;
        }

        $scope.slice = callbackData.slice("count", "from", $scope.dimension.path);
        $scope.draw();
    };


    $scope.update = function(noApi, recomputeSlice) {
        if(noApi && $scope.data) {
            if(recomputeSlice) {
                $scope.plotData($scope.data);
            } else
                $scope.draw();
            return;
        }

        $scope.dimension = $scope.availableDimensions[$scope.selectedDimension];

        /*if(o == $scope.origin) {
            $scope.chart.type =  $scope.chartType + "Chart";
            $scope.draw();
            return;
        }*/

        $scope.log.length = 0;
        callApi(new DataCube(), $scope.dimension.query);
    };

    $scope.alertMissing = function(label) {
        $scope.log.push({error: 1, label: label});
    };
    $scope.$watch("config", function(newData, oldData) {
        var onlyChartType = newData.chartType != oldData.chartType;
        var onlyInternalMoves = newData.internalMoves != oldData.internalMoves;
        $scope.update(onlyChartType || onlyInternalMoves, onlyInternalMoves);
    }, true);
    $scope.$watch("allowEstimated", function(newData, oldData) {
        $scope.update(false, false);
    });
}]);
