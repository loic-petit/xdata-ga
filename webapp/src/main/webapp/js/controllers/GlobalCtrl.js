xdata.controller("Global", ["$scope", "$routeParams", "$location", "$api", "$enum", function($scope, $routeParams, $location, $api, $enum) {
    $scope.selection = $routeParams;
    $scope.location = $location;
    $scope.$enum = $enum;
    $scope.selectedZone = {};
    function initOptionsBase() {
        $scope.optionsBase = {
            title: {value : ''},
            legend: {
                display: false
            },
            value: {
                display: false,
                tooltip: {
                    enabled: true,
                    formater: function(zoneName) {
                        return "<b>"+zoneName+"</b>";
                    },
                    font: "12pt Helvetica"
                },
                txt: {
                    fill: 'black'
                }
            },
            highlight: {
                color: "#428bca"
            },
            area: {
                onclick: function(z) {

                },
                label: false,
                backgroundColor: "white",
                lineColor: "#428bca",
                defaultColor: "#ccc",
                txt: {
                    fill: 'black'
                }
            },
            exporting: {
                enabled: false
            },
            series: { n: 2, colorA: "#ddd", colorB: "#999"}
        };
        $scope.emptyZonesData = {};
        for(var z in $scope.zone.idx) {
            $scope.emptyZonesData[z] = z == $scope.zone.id+"" ? 2 : 1;
        }
    }
    $scope.$watch("selection.cogId", function() {
        $scope.zone = null;
        if(!$routeParams.cogId)
            return;
        $api.zone($routeParams.cogId, function(data) {
            $scope.zone = new Zone(data.zone);
            var idx = {};
            idx[$scope.zone.id] = $scope.zone;
            $scope.zone.parent = new Zone(data.parent);
            idx[$scope.zone.parent.id] = $scope.zone.parent;

            var index = function(z) {
                z = new Zone(z);
                idx[z.id] = z;
                return z;
            }
            $scope.zone.siblings = data.siblings.map(index);
            $scope.zone.children = data.children.map(index);
            $scope.zone.idx = idx;
            $scope.selectedZone.global = $scope.zone;
            $scope.selectedZone.local = $scope.zone.children[0];

            initOptionsBase();
        });
    });

    $scope.$watch("user", function(data) {
        if(!data) return;
        data.restrictedZones.sort(function(a,b) {
            return a.id - b.id;
        });
    });
}]);