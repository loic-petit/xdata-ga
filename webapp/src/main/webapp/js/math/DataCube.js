DataSlice = Class.extend({
    // {};
    init: function() {
        $.extend(this, {
            points: {},
            categories: {
                rows: [],
                columns: []
            },
            aggregates: {
                rows: {},
                columns: {},
                total: 0
            },
            inner: {
                rows: {},
                columns: {}
            }
        }, true);
    },
    add: function(m, r, c) {
        if (!this.inner.rows[r]) {
            this.inner.rows[r] = 1;
            this.categories.rows.push(r);
            this.points[r] = {};
            this.points[r][c] = 0;
            this.aggregates.rows[r] = 0;
        }
        if (!this.inner.columns[c]) {
            this.inner.columns[c] = 1;
            this.categories.columns.push(c);
            this.points[r][c] = 0;
            this.aggregates.columns[c] = 0;
        }
        this.points[r][c] = (this.points[r][c] || 0) + m;
        this.aggregates.columns[c] += m;
        this.aggregates.rows[r] += m;
        this.aggregates.total += m;
    },
    finish: function() {
        delete this.inner;
        return this;
    }
});
DataCube = Class.extend({
    init: function() {
        this.points = [];
    },
    add: function(point) {
        this.points.push(point);
    },
    addAll: function(points) {
        for ( var i = 0, l = points.length; i < l; i++) {
            this.add(points[i]);
        }
    },
    slice: function(metric, row, column) {
        var result = new DataSlice();

        for ( var i = 0, l = this.points.length; i < l; i++) {
            var point = this.points[i];
            var m = DataCube.getPointValue(point, metric);
            var r = DataCube.getPointValue(point, row);
            var c = DataCube.getPointValue(point, column);

            result.add(m, r, c);
        }

        result.finish();
        return result;
    }
});
DataCube.getPointValue = function(point, path) {
    if (path.indexOf('.') == -1) { return point[path]; }
    var nodes = path.split('.');
    for ( var i = 0, l = nodes.length; i < l; i++) {
        point = point[nodes[i]];
    }
    return point;
};