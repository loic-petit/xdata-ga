xdata = angular.module('xdata', ['googlechart','angular-auth','ui.bootstrap']);


xdata.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/', {templateUrl: 'views/user.html'})
    .when('/:cogId', {
        templateUrl: 'views/home.html'
    }).when('/:cogId/:level', {templateUrl: 'views/chart-global.html',reloadOnSearch:false})
    .when('/:cogId/:level/:dimension', {templateUrl: 'views/chart-global.html',reloadOnSearch:false})
    .otherwise({redirectTo: '/'});
}]);