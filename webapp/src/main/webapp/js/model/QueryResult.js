QueryResult = Model.extend({
    _usedDataSets: [DataSet],
    init: function(json) {
        this._super(json);
        this.usedDataSets.sort(function(a,b) {
            return a.timeFilter.from.unix() - b.timeFilter.from.unix();
        });
    }
});