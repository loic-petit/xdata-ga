Zone = Model.extend({
    init: function(json) {
        this._super(json);
        this.name = this.lib;
    },
    getCogString: function() {
        if(!this.reg)
            return "FR";
        var cog = this.reg;
        if(this.dep)
            cog += "-"+this.dep;
        if(this.ar)
            cog += "-"+this.ar;
        if(this.com)
            cog += "-"+this.com;
        return cog;
    }
});