Range = Model.extend({
    dimension: null,
    init: function(json, dim) {
        this._super(json);
        this.dimension = dim;
    },
    format: function($enum) {
        if(this.from == null && this.to == null)
            return "Tout";
        if(this.from == this.to)
            return this.formatPoint(this.from, $enum);

        if(this.dimension == "MOVETYPE") {
            return this.listEnum($enum.moveType, this.from, this.fromExcluded, this.to, this.toExcluded);
        }
        var res = "<strong>";
        if(this.fromExcluded)
            res += "]";
        else
            res += "[";
        res += "</strong> ";
        if(this.from == null)
            res += "-&infin;"
        else
            res += this.formatPoint(this.from, $enum);
        res += ", ";
        if(this.to == null)
            res += "+&infin;"
        else
            res += this.formatPoint(this.to, $enum);
        res += " <strong>";
        if(this.toExcluded)
            res += "[";
        else
            res += "]";
        return res+"</strong>";
    },
    formatPoint: function(pt, $enum) {
        if("GEO" == this.dimension || "GEO_ORIGIN" == this.dimension) {
            return new Zone(pt).getCogString();
        } else if("TEMPORAL" == this.dimension) {
            return moment(pt).format("MM-YYYY");
        } else if("MOVETYPE" == this.dimension) {
            return $enum.moveType[pt].label;
        } else if("CSP" == this.dimension) {
            return $enum.category[pt];
        }
        return pt;
    },
    listEnum: function(moveType, from, fromExcluded, to, toExcluded) {
        var enumList = Hashmap.keys(moveType);
        enumList.sort(function(a,b) { return moveType[a].id-moveType[b].id; });
        from = moveType[from].id;
        if(fromExcluded)
            from++;
        if(to == null)
            to = enumList[enumList.length-1];
        to = moveType[to].id;
        if(!toExcluded)
            to++;
        return enumList.slice(from, to).map(function(a) {return moveType[a].label; }).join("<br/>");
    }
});