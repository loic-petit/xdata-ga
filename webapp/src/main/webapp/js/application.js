/**
 * Dev-Mode
 *
 * This mode loads the files declared on load.js, on production mode this file is replaced by a compiled version of
 * all those files.
 */
var applicationName = "application";

/**
 * Common loader
 *
 * This will load the application by doing the following tasks
 *  - Find the basePath of the application
 *  - Get the load json, this json is structured as an array of items, those can be of two types:
 *      - string: relative url of the javascript file (without '.js')
 *      - object: {dir: items}
 *          directory of items (dir is the name of the directory)
 *     Ex:
 *     [
 *      "init",
 *      {"lib": [
 *          "angular", "select2"
 *      ]},
 *      {"modules": [
 *          "core",
 *          {"stuff": [
 *              "A", "B"
 *          ]}
 *      ]}
 *     ]
 *     ... will load: "init.js", "lib/angular.js", "lib/select2", "modules/core", "modules/stuff/A", "modules/stuff/B"
 *
 *  - Forms the loadQueue from this json
 *  - Loads the queue with sync
 */
(function () {
    var basePath;
    /**
     * Common Hack (as found on scriptaculous and some other libs)
     * In order to get the absolute path to this script
     * List all the scripts element to find the one calling this one (js/visualis) and get the base
     */
    var searchLoader = applicationName+"\\.js(\\?.*)?$";
    $('head script[src]').each(function (i, s) {
        if (s.src.match(searchLoader)) {
            basePath = s.src.replace(new RegExp(searchLoader,'g'), '');
            return false;
        }
        return true;
    });

    var toLoad;
    $.ajax({
        url:basePath + "load.json",
        async:false,
        dataType:"json"
    })
        .done(function(json) { toLoad = json; })
        .fail(function (jqXHR, textStatus) {
        console.error("Can not get js/load.js to load "+applicationName+" (devmode)!");
    });

    var loadQueue = [];
    /**
     * Prepares the loading by injecting inside loadQueue the urls of the diverse elements found in toLoad
     * If an item is an object
     *      {dir: urls}
     *      Then recursiveLoad will be applied with the prefix: prefix+dir+"/" and arr: urls
     *
     * If it is a string
     *      Then the following relative url is loaded
     *          basePath + prefix + item + ".js"
     *
     * @param prefix Recursive URL prefix
     * @param arr Array of elements to load
     */
    var recursiveLoad = function(prefix, arr) {
        $.each(arr, function (i, item) {
            if (typeof item == 'object') {
                $.each(item, function (dir, array) {
                    recursiveLoad(prefix + dir + "/", array);
                });
                return;
            }
            loadQueue.push(basePath + prefix + item + ".js");
        });
    };

    // load all the scripts!
    recursiveLoad('../', toLoad);
    if (typeof jasmine !== 'undefined') // TODO : Get angular-mocks loading clean
        recursiveLoad('../../../test/webapp/lib/', ['angular-mocks']);

    // Sync loading
    $.each(loadQueue, function (i, url) {
        $.ajax({
            url:url,
            async:false,
            dataType:"text"
        }).done($.proxy(function (data) {
            try {
                eval(data+ "\n//@ sourceURL=" + url);
            } catch (err) {
                console.error("Could not load " + url + " / " + err);
            }
        }, window));
    });
})();
