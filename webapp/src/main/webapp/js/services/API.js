xdata.factory('$api', ["$http", function($http) {
    var api = {
        path: "/api/",
        errorHandler: function(data, status, headers, config) {
            if(console)
                console.log("Bad http request", data, status, headers, config);
        },
        datasets: function(cogId, callback) {
            $http.get(api.path+cogId+"/datasets").success(callback).error(api.errorHandler);
        },
        zone: function(cogId, callback) {
            $http.get(api.path+cogId+"/zone").success(callback).error(api.errorHandler);
        },
        data: function(cogId, granularity, params, callback) {
            $http.get(api.path+cogId+"/data/"+granularity, {params: params}).success(callback).error(api.errorHandler);
        }
    };
    return api;
}]);