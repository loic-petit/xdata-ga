xdata.factory('$enum', [function() {
    var w = {
        moveType: {
            "HOUSING": {
                id: 0,
                label: "Déménagement"
            },
            "TOURISM": {
                id: 1,
                label: "Tourisme"
            },
            "WORK": {
                id: 2,
                label: "Travail"
            },
            "SCHOLAR": {
                id: 3,
                label: "École"
            }
        },
        populationFilter: {},
        geoGranularity: {
            "COMMUNE": {id: 0, label: "Commune"},
            "ARRONDISSEMENT": {id: 1, label: "Arrondissement départemental"},
            "DEPARTEMENT": {id: 2, label: "Département"},
            "REGION": {id: 3, label: "Région"},
            "NATION": {id: 4, label: "France"}
        },
        timeGranularity: {
            "YEAR": {
                label: "Annuel",
                format: "YYYY"
            },
            "MONTH": {
                label: "Mensuel",
                format: "MM-YYYY"
            },
            "DAY": {
                label: "Journalier",
                format: "DD-MM-YYYY"
            }
        },
        category: {
            AGRICULTURE: ("Agriculteurs exploitants"),
            ARTISANT: ("Artisans, commerçants et chefs d'entreprise"),
            CADRES: ("Cadres et professions intellectuelles supérieures"),
            INTERMEDIAIRE: ("Professions Intermédiaires"),
            EMPLOYES: ("Employés"),
            OUVRIERS: ("Ouvriers"),
            RETRAITES: ("Retraités"),
            SANS_ACTIVITE: ("Autres personnes sans activité professionnelle"),
            ETUDIANT_INDEPENDANT: ("Etudiants indépendants")
        },
        chartType: {
            "Line": {id: 0, label: "Lignes"},
            "Column": {id: 2, label: "Colonnes"},
            "Area": {id: 1, label: "Aires"},
            "Map": {id: 5, label: "Carte"},
            "Bar": {id: 3, label:"Barres"},
            "Pie": {id: 4, label: "Camembert"}
        },
        dimensions: {
            TEMPORAL: {id: 0, label: "Période"},
            GEO: {id: 1, label: "Zone d'arrivée"},
            GEO_ORIGIN: {id: 2, label: "Zone de départ"},
            MOVETYPE: {id: 3, label: "Type de déplacement"},
            CSP: {id: 4, label: "CSP"}
        }
    };
    return w;
}]);