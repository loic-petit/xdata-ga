function data(url, load) {
    $.ajax({
        url: 'unit/data/'+url+'.json',
        async:false,
        dataType:"json"
    }).done(load);
}